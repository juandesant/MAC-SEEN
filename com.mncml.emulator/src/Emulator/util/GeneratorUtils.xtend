package Emulator.util

import mncModel.ControlNode
import mncModel.Alarm
import org.eclipse.emf.common.util.EList
import mncModel.AlarmBlock
import mncModel.EventBlock
import mncModel.Event
import mncModel.Command
import mncModel.CommandResponseBlock

class GeneratorUtils { 
	
	
	def getDataPointBlocks(ControlNode cn)
	{
		if(cn.dataPointBlocks!=null)
		{ 
			if(cn.dataPointBlocks.dataPointBlocks!=null)
			{
				return cn.dataPointBlocks.dataPointBlocks
			}
		}
	}
	
	def getEventBlocks(ControlNode cn)
	{
		if(cn.eventBlocks!=null)
		{
			if(cn.eventBlocks.eventBlocks!=null)
			{
				return cn.eventBlocks.eventBlocks
			}
		}
	}
	
	def getAlarmBlocks(ControlNode cn)
	{
		if(cn.alarmBlocks!=null)
		{
			if(cn.alarmBlocks.alarmBlocks!=null)
			{
				return  cn.alarmBlocks.alarmBlocks
			}
		}
	}
	
	def getCommandResponseBlocks(ControlNode cn)
	{
		if(cn.commandResponseBlocks!=null)
		{
			if(cn.commandResponseBlocks.commandResponseBlocks!=null)
			{
				return cn.commandResponseBlocks.commandResponseBlocks
			}
		}
	}
	

	

	
	def alarmExists(Alarm alarm, EList<AlarmBlock> alarmBlockList)
	{
		for(alarmBlock : alarmBlockList)
		{
			if(alarmBlock.alarm.equals(alarm))
			{
				return true;
			}
		}
		return false;
	}
	
	def eventExists(Event event,EList<EventBlock> eventBlockList)
	{
		for(eventBlock : eventBlockList)
		{
			if(eventBlock.event.equals(event))
			{
				return true; 
			}
		}
		return false;
	}
	
	def commandExists(Command command,EList<CommandResponseBlock> commandResponseBlockList)
	{
		for(comRespBlock : commandResponseBlockList)
		{
			if(comRespBlock.command.equals(command))
			{
				return true;
			}
		}
		return false;
	}
}