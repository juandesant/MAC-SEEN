package Emulator.util;

import com.google.common.base.Objects;
import mncModel.Alarm;
import mncModel.AlarmBlock;
import mncModel.Command;
import mncModel.CommandResponseBlock;
import mncModel.ControlNode;
import mncModel.DataPointBlock;
import mncModel.Event;
import mncModel.EventBlock;
import mncModel.utility.AlarmBlockUtility;
import mncModel.utility.CommandResponseBlockUtility;
import mncModel.utility.DataPointBlockUtility;
import mncModel.utility.EventBlockUtility;
import org.eclipse.emf.common.util.EList;

@SuppressWarnings("all")
public class GeneratorUtils {
  public EList<DataPointBlock> getDataPointBlocks(final ControlNode cn) {
    DataPointBlockUtility _dataPointBlocks = cn.getDataPointBlocks();
    boolean _notEquals = (!Objects.equal(_dataPointBlocks, null));
    if (_notEquals) {
      DataPointBlockUtility _dataPointBlocks_1 = cn.getDataPointBlocks();
      EList<DataPointBlock> _dataPointBlocks_2 = _dataPointBlocks_1.getDataPointBlocks();
      boolean _notEquals_1 = (!Objects.equal(_dataPointBlocks_2, null));
      if (_notEquals_1) {
        DataPointBlockUtility _dataPointBlocks_3 = cn.getDataPointBlocks();
        return _dataPointBlocks_3.getDataPointBlocks();
      }
    }
    return null;
  }
  
  public EList<EventBlock> getEventBlocks(final ControlNode cn) {
    EventBlockUtility _eventBlocks = cn.getEventBlocks();
    boolean _notEquals = (!Objects.equal(_eventBlocks, null));
    if (_notEquals) {
      EventBlockUtility _eventBlocks_1 = cn.getEventBlocks();
      EList<EventBlock> _eventBlocks_2 = _eventBlocks_1.getEventBlocks();
      boolean _notEquals_1 = (!Objects.equal(_eventBlocks_2, null));
      if (_notEquals_1) {
        EventBlockUtility _eventBlocks_3 = cn.getEventBlocks();
        return _eventBlocks_3.getEventBlocks();
      }
    }
    return null;
  }
  
  public EList<AlarmBlock> getAlarmBlocks(final ControlNode cn) {
    AlarmBlockUtility _alarmBlocks = cn.getAlarmBlocks();
    boolean _notEquals = (!Objects.equal(_alarmBlocks, null));
    if (_notEquals) {
      AlarmBlockUtility _alarmBlocks_1 = cn.getAlarmBlocks();
      EList<AlarmBlock> _alarmBlocks_2 = _alarmBlocks_1.getAlarmBlocks();
      boolean _notEquals_1 = (!Objects.equal(_alarmBlocks_2, null));
      if (_notEquals_1) {
        AlarmBlockUtility _alarmBlocks_3 = cn.getAlarmBlocks();
        return _alarmBlocks_3.getAlarmBlocks();
      }
    }
    return null;
  }
  
  public EList<CommandResponseBlock> getCommandResponseBlocks(final ControlNode cn) {
    CommandResponseBlockUtility _commandResponseBlocks = cn.getCommandResponseBlocks();
    boolean _notEquals = (!Objects.equal(_commandResponseBlocks, null));
    if (_notEquals) {
      CommandResponseBlockUtility _commandResponseBlocks_1 = cn.getCommandResponseBlocks();
      EList<CommandResponseBlock> _commandResponseBlocks_2 = _commandResponseBlocks_1.getCommandResponseBlocks();
      boolean _notEquals_1 = (!Objects.equal(_commandResponseBlocks_2, null));
      if (_notEquals_1) {
        CommandResponseBlockUtility _commandResponseBlocks_3 = cn.getCommandResponseBlocks();
        return _commandResponseBlocks_3.getCommandResponseBlocks();
      }
    }
    return null;
  }
  
  public boolean alarmExists(final Alarm alarm, final EList<AlarmBlock> alarmBlockList) {
    for (final AlarmBlock alarmBlock : alarmBlockList) {
      Alarm _alarm = alarmBlock.getAlarm();
      boolean _equals = _alarm.equals(alarm);
      if (_equals) {
        return true;
      }
    }
    return false;
  }
  
  public boolean eventExists(final Event event, final EList<EventBlock> eventBlockList) {
    for (final EventBlock eventBlock : eventBlockList) {
      Event _event = eventBlock.getEvent();
      boolean _equals = _event.equals(event);
      if (_equals) {
        return true;
      }
    }
    return false;
  }
  
  public boolean commandExists(final Command command, final EList<CommandResponseBlock> commandResponseBlockList) {
    for (final CommandResponseBlock comRespBlock : commandResponseBlockList) {
      Command _command = comRespBlock.getCommand();
      boolean _equals = _command.equals(command);
      if (_equals) {
        return true;
      }
    }
    return false;
  }
}
