package Emulator.generator;

import com.google.common.base.Objects;
import java.util.ArrayList;
import mncModel.DataPoint;
import mncModel.PrimitiveValue;
import mncModel.PrimitiveValueType;
import mncModel.impl.BoolValueImpl;
import mncModel.impl.FloatValueImpl;
import mncModel.impl.IntValueImpl;
import mncModel.impl.StringValueImpl;
import org.eclipse.emf.common.util.EList;

@SuppressWarnings("all")
public class ParameterValueFinder {
  public static String findValueType(final DataPoint dp) {
    boolean _notEquals = (!Objects.equal(dp, null));
    if (_notEquals) {
      PrimitiveValueType _type = dp.getType();
      boolean _equals = _type.equals(PrimitiveValueType.INT);
      if (_equals) {
        return "int";
      } else {
        PrimitiveValueType _type_1 = dp.getType();
        boolean _equals_1 = _type_1.equals(PrimitiveValueType.FLOAT);
        if (_equals_1) {
          return "float";
        } else {
          PrimitiveValueType _type_2 = dp.getType();
          boolean _equals_2 = _type_2.equals(PrimitiveValueType.STRING);
          if (_equals_2) {
            return "String";
          } else {
            PrimitiveValueType _type_3 = dp.getType();
            boolean _equals_3 = _type_3.equals(PrimitiveValueType.BOOLEAN);
            if (_equals_3) {
              return "boolean";
            }
          }
        }
      }
    }
    return null;
  }
  
  public static String findValue(final PrimitiveValue value) {
    boolean _or = false;
    boolean _notEquals = (!Objects.equal(value, null));
    if (_notEquals) {
      _or = true;
    } else {
      boolean _notEquals_1 = (!Objects.equal(value, ""));
      _or = _notEquals_1;
    }
    if (_or) {
      if ((value instanceof IntValueImpl)) {
        int _intValue = ((IntValueImpl)value).getIntValue();
        return Integer.valueOf(_intValue).toString();
      } else {
        if ((value instanceof FloatValueImpl)) {
          float _floatValue = ((FloatValueImpl)value).getFloatValue();
          return Float.valueOf(_floatValue).toString();
        } else {
          if ((value instanceof StringValueImpl)) {
            String _stringValue = ((StringValueImpl)value).getStringValue();
            String hh = _stringValue.toString();
            boolean _equals = hh.equals("");
            boolean _not = (!_equals);
            if (_not) {
              String _stringValue_1 = ((StringValueImpl)value).getStringValue();
              return _stringValue_1.toString();
            } else {
              return "0";
            }
          } else {
            if ((value instanceof BoolValueImpl)) {
              boolean _isBoolValue = ((BoolValueImpl)value).isBoolValue();
              return Boolean.valueOf(_isBoolValue).toString();
            }
          }
        }
      }
    } else {
      return "0";
    }
    return null;
  }
  
  public static String findValue(final EList<PrimitiveValue> value) {
    boolean _notEquals = (!Objects.equal(value, null));
    if (_notEquals) {
      boolean _and = false;
      int _size = value.size();
      boolean _greaterThan = (_size > 0);
      if (!_greaterThan) {
        _and = false;
      } else {
        PrimitiveValue _get = value.get(0);
        _and = (_get instanceof IntValueImpl);
      }
      if (_and) {
        String str = "";
        for (final PrimitiveValue valu : value) {
          {
            IntValueImpl v = ((IntValueImpl) valu);
            int _intValue = v.getIntValue();
            String _string = Integer.valueOf(_intValue).toString();
            String _plus = (str + _string);
            String _plus_1 = (_plus + ",");
            str = _plus_1;
          }
        }
        return str;
      } else {
        boolean _and_1 = false;
        int _size_1 = value.size();
        boolean _greaterThan_1 = (_size_1 > 0);
        if (!_greaterThan_1) {
          _and_1 = false;
        } else {
          PrimitiveValue _get_1 = value.get(0);
          _and_1 = (_get_1 instanceof FloatValueImpl);
        }
        if (_and_1) {
          String str_1 = "";
          for (final PrimitiveValue valu_1 : value) {
            {
              FloatValueImpl v = ((FloatValueImpl) valu_1);
              float _floatValue = v.getFloatValue();
              String _string = Float.valueOf(_floatValue).toString();
              String _plus = (str_1 + _string);
              String _plus_1 = (_plus + ",");
              str_1 = _plus_1;
            }
          }
          return str_1;
        } else {
          boolean _and_2 = false;
          int _size_2 = value.size();
          boolean _greaterThan_2 = (_size_2 > 0);
          if (!_greaterThan_2) {
            _and_2 = false;
          } else {
            PrimitiveValue _get_2 = value.get(0);
            _and_2 = (_get_2 instanceof StringValueImpl);
          }
          if (_and_2) {
            String str_2 = "";
            for (final PrimitiveValue valu_2 : value) {
              {
                StringValueImpl v = ((StringValueImpl) valu_2);
                String _stringValue = v.getStringValue();
                String _string = _stringValue.toString();
                String _plus = (str_2 + _string);
                String _plus_1 = (_plus + ",");
                str_2 = _plus_1;
              }
            }
            return str_2;
          } else {
            return "";
          }
        }
      }
    }
    return null;
  }
  
  public static ArrayList<Integer> convertIntoArrayList(final EList<PrimitiveValue> list) {
    ArrayList<Integer> arrayList = ((ArrayList<Integer>) null);
    boolean _and = false;
    boolean _notEquals = (!Objects.equal(list, null));
    if (!_notEquals) {
      _and = false;
    } else {
      int _size = list.size();
      boolean _greaterThan = (_size > 0);
      _and = _greaterThan;
    }
    if (_and) {
      PrimitiveValue _get = list.get(0);
      if ((_get instanceof IntValueImpl)) {
        ArrayList<Integer> _arrayList = new ArrayList<Integer>();
        arrayList = _arrayList;
        for (final PrimitiveValue value : list) {
          {
            IntValueImpl simpleVal = ((IntValueImpl) value);
            int _intValue = simpleVal.getIntValue();
            arrayList.add(Integer.valueOf(_intValue));
          }
        }
        return arrayList;
      }
      PrimitiveValue _get_1 = list.get(0);
      if ((_get_1 instanceof FloatValueImpl)) {
        ArrayList<Integer> _arrayList_1 = new ArrayList<Integer>();
        arrayList = _arrayList_1;
        for (final PrimitiveValue value_1 : list) {
          {
            FloatValueImpl simpleVal = ((FloatValueImpl) value_1);
            float _floatValue = simpleVal.getFloatValue();
            arrayList.add(Integer.valueOf(((int) _floatValue)));
          }
        }
        return arrayList;
      }
      PrimitiveValue _get_2 = list.get(0);
      if ((_get_2 instanceof StringValueImpl)) {
        ArrayList<Integer> _arrayList_2 = new ArrayList<Integer>();
        arrayList = _arrayList_2;
        for (final PrimitiveValue value_2 : list) {
          {
            StringValueImpl simpleVal = ((StringValueImpl) value_2);
            String _stringValue = simpleVal.getStringValue();
            int _parseInt = Integer.parseInt(_stringValue);
            arrayList.add(Integer.valueOf(_parseInt));
          }
        }
        return arrayList;
      }
    }
    return null;
  }
}
