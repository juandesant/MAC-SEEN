package Emulator.generator;

import Emulator.generator.ParameterValueFinder;
import Emulator.util.GeneratorUtils;
import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import java.util.List;
import mncModel.AlarmBlock;
import mncModel.Command;
import mncModel.CommandResponseBlock;
import mncModel.ControlNode;
import mncModel.DataPoint;
import mncModel.DataPointBlock;
import mncModel.DataPointHandling;
import mncModel.DataPointValidCondition;
import mncModel.EventBlock;
import mncModel.InterfaceDescription;
import mncModel.Model;
import mncModel.Parameter;
import mncModel.PrimitiveValue;
import mncModel.PrimitiveValueType;
import mncModel.SimpleType;
import mncModel.impl.BoolValueImpl;
import mncModel.impl.ControlNodeImpl;
import mncModel.impl.FloatValueImpl;
import mncModel.impl.IntValueImpl;
import mncModel.impl.InterfaceDescriptionImpl;
import mncModel.impl.StringValueImpl;
import mncModel.utility.CommandResponseBlockUtility;
import mncModel.utility.DataPointBlockUtility;
import mncModel.utility.DataPointUtility;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class EmulatorJavaGenerator implements IGenerator {
  @Inject
  private static EList<DataPointBlock> dataPointBlockList;
  
  @Inject
  private static EList<AlarmBlock> alarmBlockList;
  
  @Inject
  private static EList<EventBlock> eventBlockList;
  
  @Inject
  private static EList<CommandResponseBlock> commandResponseBlockList;
  
  private IFileSystemAccess fsa;
  
  private ControlNodeImpl controlNode = ((ControlNodeImpl) null);
  
  private boolean connect = false;
  
  public void setConnect(final boolean connect) {
    this.connect = connect;
  }
  
  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess fsa) {
    this.fsa = fsa;
    EList<EObject> _contents = resource.getContents();
    EObject _head = IterableExtensions.<EObject>head(_contents);
    final Model instance = ((Model) _head);
    boolean _and = false;
    boolean _and_1 = false;
    boolean _and_2 = false;
    boolean _and_3 = false;
    boolean _notEquals = (!Objects.equal(instance, null));
    if (!_notEquals) {
      _and_3 = false;
    } else {
      EList<EObject> _eContents = instance.eContents();
      Iterable<InterfaceDescriptionImpl> _filter = Iterables.<InterfaceDescriptionImpl>filter(_eContents, InterfaceDescriptionImpl.class);
      boolean _notEquals_1 = (!Objects.equal(_filter, null));
      _and_3 = _notEquals_1;
    }
    if (!_and_3) {
      _and_2 = false;
    } else {
      EList<EObject> _eContents_1 = instance.eContents();
      Iterable<InterfaceDescriptionImpl> _filter_1 = Iterables.<InterfaceDescriptionImpl>filter(_eContents_1, InterfaceDescriptionImpl.class);
      int _size = IterableExtensions.size(_filter_1);
      boolean _notEquals_2 = (_size != 0);
      _and_2 = _notEquals_2;
    }
    if (!_and_2) {
      _and_1 = false;
    } else {
      EList<EObject> _eContents_2 = instance.eContents();
      Iterable<ControlNode> _filter_2 = Iterables.<ControlNode>filter(_eContents_2, ControlNode.class);
      boolean _notEquals_3 = (!Objects.equal(_filter_2, null));
      _and_1 = _notEquals_3;
    }
    if (!_and_1) {
      _and = false;
    } else {
      EList<EObject> _eContents_3 = instance.eContents();
      Iterable<ControlNode> _filter_3 = Iterables.<ControlNode>filter(_eContents_3, ControlNode.class);
      int _size_1 = IterableExtensions.size(_filter_3);
      boolean _notEquals_4 = (_size_1 != 0);
      _and = _notEquals_4;
    }
    if (_and) {
      EList<EObject> _eContents_4 = instance.eContents();
      Iterable<InterfaceDescriptionImpl> _filter_4 = Iterables.<InterfaceDescriptionImpl>filter(_eContents_4, InterfaceDescriptionImpl.class);
      final InterfaceDescriptionImpl interface_ = ((InterfaceDescriptionImpl[])Conversions.unwrapArray(_filter_4, InterfaceDescriptionImpl.class))[0];
      EList<EObject> _eContents_5 = instance.eContents();
      Iterable<ControlNodeImpl> _filter_5 = Iterables.<ControlNodeImpl>filter(_eContents_5, ControlNodeImpl.class);
      ControlNodeImpl _get = ((ControlNodeImpl[])Conversions.unwrapArray(_filter_5, ControlNodeImpl.class))[0];
      this.controlNode = _get;
      GeneratorUtils _generatorUtils = new GeneratorUtils();
      EList<DataPointBlock> _dataPointBlocks = _generatorUtils.getDataPointBlocks(this.controlNode);
      EmulatorJavaGenerator.dataPointBlockList = _dataPointBlocks;
      GeneratorUtils _generatorUtils_1 = new GeneratorUtils();
      EList<EventBlock> _eventBlocks = _generatorUtils_1.getEventBlocks(this.controlNode);
      EmulatorJavaGenerator.eventBlockList = _eventBlocks;
      GeneratorUtils _generatorUtils_2 = new GeneratorUtils();
      EList<AlarmBlock> _alarmBlocks = _generatorUtils_2.getAlarmBlocks(this.controlNode);
      EmulatorJavaGenerator.alarmBlockList = _alarmBlocks;
      GeneratorUtils _generatorUtils_3 = new GeneratorUtils();
      EList<CommandResponseBlock> _commandResponseBlocks = _generatorUtils_3.getCommandResponseBlocks(this.controlNode);
      EmulatorJavaGenerator.commandResponseBlockList = _commandResponseBlocks;
      final String className = this.controlNode.getName();
      boolean _notEquals_5 = (!Objects.equal(instance, null));
      if (_notEquals_5) {
        CharSequence _javaCode = this.toJavaCode(((InterfaceDescriptionImpl) interface_), ((ControlNodeImpl) this.controlNode), fsa);
        fsa.generateFile((className + ".java"), _javaCode);
      }
    } else {
      IWorkbench _workbench = PlatformUI.getWorkbench();
      Display _display = _workbench.getDisplay();
      _display.asyncExec(
        new Runnable() {
          @Override
          public void run() {
            IWorkbench _workbench = PlatformUI.getWorkbench();
            IWorkbenchWindow _activeWorkbenchWindow = _workbench.getActiveWorkbenchWindow();
            Shell shell = _activeWorkbenchWindow.getShell();
            MessageDialog.openError(shell, "Error", 
              "Please create the interface description and the control node");
          }
        });
    }
  }
  
  public CharSequence toJavaCode(final InterfaceDescriptionImpl ides, final ControlNodeImpl controlNode, final IFileSystemAccess fsa) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.newLine();
    _builder.append("/*----- PROTECTED REGION END -----*///\t");
    String _name = controlNode.getName();
    String _firstUpper = StringExtensions.toFirstUpper(_name);
    _builder.append(_firstUpper, "");
    _builder.append(".java");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.newLine();
    _builder.append("package com.tango.nodes.java;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("/*----- PROTECTED REGION ENABLED START -----*/");
    _builder.newLine();
    _builder.newLine();
    _builder.append("import utils.tango.Alarm;");
    _builder.newLine();
    _builder.append("import fr.esrf.Tango.DevFailed;");
    _builder.newLine();
    _builder.append("import fr.esrf.Tango.DevState;");
    _builder.newLine();
    _builder.append("import fr.esrf.Tango.DispLevel;");
    _builder.newLine();
    _builder.append("import fr.esrf.TangoApi.DevicePipe;");
    _builder.newLine();
    _builder.append("import fr.esrf.TangoApi.PipeBlob;");
    _builder.newLine();
    _builder.append("import fr.esrf.TangoApi.PipeDataElement;");
    _builder.newLine();
    _builder.append("import org.slf4j.Logger;");
    _builder.newLine();
    _builder.append("import org.slf4j.LoggerFactory;");
    _builder.newLine();
    _builder.append("import org.slf4j.ext.XLogger;");
    _builder.newLine();
    _builder.append("import org.slf4j.ext.XLoggerFactory;");
    _builder.newLine();
    _builder.append("import org.tango.DeviceState;");
    _builder.newLine();
    _builder.append("import org.tango.server.InvocationContext;");
    _builder.newLine();
    _builder.append("import org.tango.server.ServerManager;");
    _builder.newLine();
    _builder.append("import org.tango.server.annotation.*;");
    _builder.newLine();
    _builder.append("import org.tango.server.attribute.AttributeValue;");
    _builder.newLine();
    _builder.append("import org.tango.server.device.DeviceManager;");
    _builder.newLine();
    _builder.append("import org.tango.server.dynamic.DynamicManager;");
    _builder.newLine();
    _builder.append("import org.tango.server.events.EventType;");
    _builder.newLine();
    _builder.append("import org.tango.server.pipe.PipeValue;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("import java.util.HashMap;");
    _builder.newLine();
    _builder.append("import java.util.Map;");
    _builder.newLine();
    _builder.append("import java.util.Random;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("import static java.lang.Thread.sleep;");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    _builder.append("//\tImport Tango IDL types");
    _builder.newLine();
    _builder.append("// additional imported packages");
    _builder.newLine();
    _builder.newLine();
    _builder.append("/*----- PROTECTED REGION END -----*///\t");
    String _name_1 = controlNode.getName();
    String _firstUpper_1 = StringExtensions.toFirstUpper(_name_1);
    _builder.append(_firstUpper_1, "");
    _builder.append(".imports");
    _builder.newLineIfNotEmpty();
    _builder.append("  ");
    _builder.newLine();
    _builder.append("@Device");
    _builder.newLine();
    _builder.append("public class ");
    String _name_2 = controlNode.getName();
    String _firstUpper_2 = StringExtensions.toFirstUpper(_name_2);
    _builder.append(_firstUpper_2, "");
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public static final Logger logger = LoggerFactory.getLogger(");
    String _name_3 = controlNode.getName();
    String _firstUpper_3 = StringExtensions.toFirstUpper(_name_3);
    _builder.append(_firstUpper_3, "\t");
    _builder.append(".class);");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("private static final XLogger xlogger = XLoggerFactory");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append(".getXLogger(");
    String _name_4 = controlNode.getName();
    String _firstUpper_4 = StringExtensions.toFirstUpper(_name_4);
    _builder.append(_firstUpper_4, "\t\t\t");
    _builder.append(".class);");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("// private static String instance;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// ========================================================");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// Programmer\'s data members");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// ========================================================");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/*----- PROTECTED REGION interfaceDescription(");
    String _name_5 = controlNode.getName();
    String _firstUpper_5 = StringExtensions.toFirstUpper(_name_5);
    _builder.append(_firstUpper_5, "\t");
    _builder.append(".variables) ENABLED START -----*/");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// Put static variables here");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/*----- PROTECTED REGION END -----*/// ");
    String _name_6 = controlNode.getName();
    String _firstUpper_6 = StringExtensions.toFirstUpper(_name_6);
    _builder.append(_firstUpper_6, "\t");
    _builder.append(".variables");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("/*----- PROTECTED REGION interfaceDescription(");
    String _name_7 = controlNode.getName();
    String _firstUpper_7 = StringExtensions.toFirstUpper(_name_7);
    _builder.append(_firstUpper_7, "\t");
    _builder.append(".private) ENABLED START -----*/");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// Put private variables here");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private Map<Integer, Alarm> alarmMap;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private PipeBlob responsePipeBlob;");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("private PipeBlob alarmPipeBlob;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private static String deviceName = \"nodes/");
    String _name_8 = controlNode.getName();
    String _firstUpper_8 = StringExtensions.toFirstUpper(_name_8);
    _builder.append(_firstUpper_8, "\t");
    _builder.append("/test\";");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//========================================================");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//\tProperty data members and related methods");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//========================================================");
    _builder.newLine();
    _builder.append("\t");
    CharSequence _declareDevicePropertiesAndDynamicAttributes = this.declareDevicePropertiesAndDynamicAttributes(ides, fsa);
    _builder.append(_declareDevicePropertiesAndDynamicAttributes, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//========================================================");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//\tMiscellaneous methods");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//========================================================");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Initialize the device.");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @throws DevFailed if something fails during the device initialization.");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@Init(lazyLoading = false)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public void initDevice() throws DevFailed {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("xlogger.entry();");
    _builder.newLine();
    _builder.append("\t \t");
    _builder.append("logger.debug(\"init device \" + deviceManager.getName());");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("/*----- PROTECTED REGION interfaceDescription(LeafNode.initDevice) ENABLED START -----*/");
    _builder.newLine();
    _builder.newLine();
    _builder.append("        ");
    _builder.append("//\tPut your device initialization code here");
    _builder.newLine();
    _builder.append("        ");
    _builder.append("/*----- PROTECTED REGION END -----*/\t//\tLeafNode.initDevice");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("xlogger.exit();");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("        ");
    _builder.append("//\tPut your code here");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("/*----- PROTECTED REGION END -----*/\t//\tLeafNode.setDynamicManager");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Device management. Will be injected by the framework.");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@DeviceManagement");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("DeviceManager deviceManager;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public void setDeviceManager(DeviceManager deviceManager){");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("this.deviceManager= deviceManager ;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}      ");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// ========================================================");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// Attribute data members and related methods");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// ========================================================");
    _builder.newLine();
    _builder.append("\t");
    CharSequence _declareAttributes = this.declareAttributes(controlNode);
    _builder.append(_declareAttributes, "\t");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//========================================================");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//\tPipe data members and related methods");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//========================================================");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Pipe Response");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* description:");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*     ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@Pipe(description=\"\", displayLevel=DispLevel._OPERATOR, label=\"Response\")");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private PipeValue response;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Read Pipe Response");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @return attribute value");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @throws DevFailed if read pipe failed.");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public PipeValue getResponse() throws DevFailed {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("xlogger.entry();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("/*----- PROTECTED REGION interfaceDescription(LeafNode.getResponse) ENABLED START -----*/");
    _builder.newLine();
    _builder.newLine();
    _builder.append("        ");
    _builder.append("//\tPut read attribute code here");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("/*----- PROTECTED REGION END -----*/\t//\tLeafNode.getResponse");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("xlogger.exit();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("return response;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Pipe Alarm");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* description:");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*     ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@Pipe(description=\"\", displayLevel=DispLevel._OPERATOR, label=\"Alarm\")");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private PipeValue alarm;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Read Pipe Alarm");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @return attribute value");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @throws DevFailed if read pipe failed.");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public PipeValue getAlarm() throws DevFailed {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("xlogger.entry();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("/*----- PROTECTED REGION interfaceDescription(LeafNode.getAlarm) ENABLED START -----*/");
    _builder.newLine();
    _builder.append("        ");
    _builder.append("//\tPut read attribute code here");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("/*----- PROTECTED REGION END -----*/\t//\tLeafNode.getAlarm");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("xlogger.exit();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("return alarm;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//========================================================");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//\tCommand data members and related methods");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//========================================================");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* The state of the device");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@State");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private DevState state = DevState.UNKNOWN;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Execute command \"State\".");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* description: This command gets the device state (stored in its \'state\' data member) and returns it to the caller.");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @return Device state");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @throws DevFailed if command execution failed.");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public final DevState getState() throws DevFailed {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("/*----- PROTECTED REGION interfaceDescription(LeafNode.getState) ENABLED START -----*/");
    _builder.newLine();
    _builder.newLine();
    _builder.append("        ");
    _builder.append("//\tPut state code here");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("/*----- PROTECTED REGION END -----*/\t//\tLeafNode.getState");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("return state;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Set the device state");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @param state the new device state");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public void setState(final DevState state) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("this.state = state;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* The status of the device");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@Status");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private String status = \"Server is starting. The device state is unknown\";");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Execute command \"Status\".");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* description: This command gets the device status (stored in its \'status\' data member) and returns it to the caller.");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @return Device status");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @throws DevFailed if command execution failed.");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public final String getStatus() throws DevFailed {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("/*----- PROTECTED REGION interfaceDescription(LeafNode.getStatus) ENABLED START -----*/");
    _builder.newLine();
    _builder.newLine();
    _builder.append("        ");
    _builder.append("//\tPut status code here");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("/*----- PROTECTED REGION END -----*/\t//\tLeafNode.getStatus");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("return status;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Set the device status");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @param status the new device status");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public void setStatus(final String status) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("this.status = status;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    CharSequence _declareCommand = this.declareCommand(controlNode);
    _builder.append(_declareCommand, "\t");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/*----- PROTECTED REGION END -----*/// ");
    String _name_9 = controlNode.getName();
    String _firstUpper_9 = StringExtensions.toFirstUpper(_name_9);
    _builder.append(_firstUpper_9, "\t");
    _builder.append(".methods");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Starts the server.");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* ");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @param args");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*            program arguments (instance_name [-v[trace level]] [-nodb");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*            [-dlist <device name list>] [-file=fileName]])");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public static void main(final String[] args) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("//DataBaseHandler.addDevice(deviceName); // Uncomment this to enter the device into the database");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("ServerManager.getInstance().start(args, ");
    String _name_10 = controlNode.getName();
    String _firstUpper_10 = StringExtensions.toFirstUpper(_name_10);
    _builder.append(_firstUpper_10, "\t\t");
    _builder.append(".class);");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("logger.info(\"------- Started -------------\");");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence declareDevicePropertiesAndDynamicAttributes(final InterfaceDescription ides, final IFileSystemAccess fsa) {
    CharSequence _xblockexpression = null;
    {
      List<String> devicePropertiesAsPerTango = CollectionLiterals.<String>newArrayList();
      devicePropertiesAsPerTango.add("description");
      devicePropertiesAsPerTango.add("default");
      devicePropertiesAsPerTango.add("isMandatory");
      StringConcatenation _builder = new StringConcatenation();
      {
        boolean _notEquals = (!Objects.equal(ides, null));
        if (_notEquals) {
          DataPointUtility dataPointBlock = ides.getDataPoints();
          _builder.newLineIfNotEmpty();
          {
            boolean _notEquals_1 = (!Objects.equal(dataPointBlock, null));
            if (_notEquals_1) {
              EList<DataPoint> dataPointList = dataPointBlock.getDataPoints();
              _builder.newLineIfNotEmpty();
              {
                boolean _notEquals_2 = (!Objects.equal(dataPointList, null));
                if (_notEquals_2) {
                  {
                    for(final DataPoint dataPoint : dataPointList) {
                      {
                        String _name = dataPoint.getName();
                        boolean _contains = _name.contains("deviceProperty");
                        if (_contains) {
                          _builder.append("/** ");
                          _builder.newLine();
                          _builder.append(" ");
                          _builder.append("* Device Property ");
                          String _name_1 = dataPoint.getName();
                          String _replace = _name_1.replace("deviceProperty_", "");
                          String _firstUpper = StringExtensions.toFirstUpper(_replace);
                          _builder.append(_firstUpper, " ");
                          _builder.newLineIfNotEmpty();
                          _builder.append(" ");
                          _builder.append("* Name of the LMC to which leaf node should connect");
                          _builder.newLine();
                          _builder.append(" ");
                          _builder.append("*/");
                          _builder.newLine();
                          _builder.append(" \t");
                          _builder.append("@DeviceProperty(name=\"");
                          String _name_2 = dataPoint.getName();
                          String _firstUpper_1 = StringExtensions.toFirstUpper(_name_2);
                          _builder.append(_firstUpper_1, " \t");
                          _builder.append("\"");
                          _builder.newLineIfNotEmpty();
                          {
                            EList<Parameter> _parameters = dataPoint.getParameters();
                            boolean _notEquals_3 = (!Objects.equal(_parameters, null));
                            if (_notEquals_3) {
                              EList<Parameter> dataPointAllParams = dataPoint.getParameters();
                              _builder.newLineIfNotEmpty();
                              {
                                for(final Parameter param : dataPointAllParams) {
                                  SimpleType simpleType = ((SimpleType) param);
                                  _builder.newLineIfNotEmpty();
                                  {
                                    String _name_3 = simpleType.getName();
                                    boolean _contains_1 = devicePropertiesAsPerTango.contains(_name_3);
                                    if (_contains_1) {
                                      {
                                        Parameter _last = IterableExtensions.<Parameter>last(dataPointAllParams);
                                        boolean _equals = param.equals(_last);
                                        boolean _not = (!_equals);
                                        if (_not) {
                                          _builder.append(",");
                                        }
                                      }
                                      _builder.append(" ");
                                      _builder.newLineIfNotEmpty();
                                      String _name_4 = simpleType.getName();
                                      _builder.append(_name_4, "");
                                      _builder.append("=");
                                      {
                                        PrimitiveValueType _type = simpleType.getType();
                                        boolean _equals_1 = _type.equals(PrimitiveValueType.STRING);
                                        if (_equals_1) {
                                          _builder.append("\"");
                                          PrimitiveValue _value = simpleType.getValue();
                                          String _findValue = ParameterValueFinder.findValue(_value);
                                          _builder.append(_findValue, "");
                                          _builder.append("\"");
                                          _builder.newLineIfNotEmpty();
                                        } else {
                                          PrimitiveValue _value_1 = simpleType.getValue();
                                          String _findValue_1 = ParameterValueFinder.findValue(_value_1);
                                          _builder.append(_findValue_1, "");
                                        }
                                      }
                                      _builder.newLineIfNotEmpty();
                                      {
                                        Parameter _last_1 = IterableExtensions.<Parameter>last(dataPointAllParams);
                                        boolean _equals_2 = param.equals(_last_1);
                                        boolean _not_1 = (!_equals_2);
                                        if (_not_1) {
                                          _builder.append(",");
                                        }
                                      }
                                      _builder.append(" ");
                                      _builder.newLineIfNotEmpty();
                                    }
                                  }
                                }
                              }
                              _builder.append(")");
                              _builder.newLine();
                            }
                          }
                          _builder.append("private ");
                          String _findValueType = ParameterValueFinder.findValueType(dataPoint);
                          _builder.append(_findValueType, "");
                          _builder.append(" ");
                          String _name_5 = dataPoint.getName();
                          String _firstLower = StringExtensions.toFirstLower(_name_5);
                          _builder.append(_firstLower, "");
                          _builder.append(";");
                          _builder.newLineIfNotEmpty();
                          _builder.append("/**");
                          _builder.newLine();
                          _builder.append(" ");
                          _builder.append("* set property ");
                          String _name_6 = dataPoint.getName();
                          String _firstUpper_2 = StringExtensions.toFirstUpper(_name_6);
                          _builder.append(_firstUpper_2, " ");
                          _builder.newLineIfNotEmpty();
                          _builder.append(" ");
                          _builder.append("* @param  ");
                          String _name_7 = dataPoint.getName();
                          String _firstLower_1 = StringExtensions.toFirstLower(_name_7);
                          _builder.append(_firstLower_1, " ");
                          _builder.append("  see description above.");
                          _builder.newLineIfNotEmpty();
                          _builder.append(" ");
                          _builder.append("*/");
                          _builder.newLine();
                          _builder.append("public void set");
                          String _name_8 = dataPoint.getName();
                          String _firstUpper_3 = StringExtensions.toFirstUpper(_name_8);
                          _builder.append(_firstUpper_3, "");
                          _builder.append("(String ");
                          String _name_9 = dataPoint.getName();
                          String _firstLower_2 = StringExtensions.toFirstLower(_name_9);
                          _builder.append(_firstLower_2, "");
                          _builder.append(") {");
                          _builder.newLineIfNotEmpty();
                          _builder.append("   ");
                          _builder.append("this.");
                          String _name_10 = dataPoint.getName();
                          String _firstLower_3 = StringExtensions.toFirstLower(_name_10);
                          _builder.append(_firstLower_3, "   ");
                          _builder.append(" = ");
                          String _name_11 = dataPoint.getName();
                          String _firstLower_4 = StringExtensions.toFirstLower(_name_11);
                          _builder.append(_firstLower_4, "   ");
                          _builder.append(";");
                          _builder.newLineIfNotEmpty();
                          _builder.append("/*----- PROTECTED REGION interfaceDescription(LeafNode.set");
                          String _name_12 = dataPoint.getName();
                          String _firstUpper_4 = StringExtensions.toFirstUpper(_name_12);
                          _builder.append(_firstUpper_4, "");
                          _builder.append(") ENABLED START -----*/");
                          _builder.newLineIfNotEmpty();
                          _builder.newLine();
                          _builder.append("//\tCheck property value here");
                          _builder.newLine();
                          _builder.append("\t");
                          _builder.newLine();
                          _builder.append("\t");
                          _builder.append("/*----- PROTECTED REGION END -----*/\t//\tLeafNode.setAssignedLMC");
                          _builder.newLine();
                          _builder.append("}");
                          _builder.newLine();
                        } else {
                          String _name_13 = dataPoint.getName();
                          boolean _contains_2 = _name_13.contains("dynamicAttribute");
                          if (_contains_2) {
                            String _name_14 = dataPoint.getName();
                            String _firstUpper_5 = StringExtensions.toFirstUpper(_name_14);
                            String _plus = (_firstUpper_5 + ".java");
                            CharSequence _createDynamicAttributeFile = this.createDynamicAttributeFile(dataPoint, this.controlNode);
                            fsa.generateFile(_plus, _createDynamicAttributeFile);
                            _builder.newLineIfNotEmpty();
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  public CharSequence createDynamicAttributeFile(final DataPoint dataPoint, final ControlNode controlNode) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/*----- PROTECTED REGION END -----*/\t//\tLeafNode.DynamicBooleanAttribute.java");
    _builder.newLine();
    _builder.newLine();
    _builder.append("package org.tango.nodes;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("import org.tango.DeviceState;");
    _builder.newLine();
    _builder.append("import org.tango.server.StateMachineBehavior;");
    _builder.newLine();
    _builder.append("import org.tango.server.attribute.IAttributeBehavior;");
    _builder.newLine();
    _builder.append("import org.tango.server.attribute.AttributeValue;");
    _builder.newLine();
    _builder.append("import org.tango.server.attribute.AttributeConfiguration;");
    _builder.newLine();
    _builder.append("import org.tango.server.attribute.AttributePropertiesImpl;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("//\tImport Tango IDL types");
    _builder.newLine();
    _builder.append("import fr.esrf.Tango.*;");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    _builder.append("/*----- PROTECTED REGION interfaceDescription(LeafNode.DynamicBooleanAttribute.addImports) ENABLED START -----*/");
    _builder.newLine();
    _builder.newLine();
    _builder.append("/*----- PROTECTED REGION END -----*/\t//\t");
    String _name = controlNode.getName();
    String _firstUpper = StringExtensions.toFirstUpper(_name);
    _builder.append(_firstUpper, "");
    _builder.append(".");
    String _name_1 = dataPoint.getName();
    String _firstUpper_1 = StringExtensions.toFirstUpper(_name_1);
    _builder.append(_firstUpper_1, "");
    _builder.append(".addImports");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.newLine();
    _builder.append("/**");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("* Dynamic attribute ");
    String _name_2 = dataPoint.getName();
    String _firstUpper_2 = StringExtensions.toFirstUpper(_name_2);
    _builder.append(_firstUpper_2, " ");
    _builder.append(", ");
    String _findValueType = ParameterValueFinder.findValueType(dataPoint);
    _builder.append(_findValueType, " ");
    _builder.append(", Scalar, READ");
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("* description: ");
    {
      EList<Parameter> _parameters = dataPoint.getParameters();
      boolean _notEquals = (!Objects.equal(_parameters, null));
      if (_notEquals) {
        {
          EList<Parameter> _parameters_1 = dataPoint.getParameters();
          for(final Parameter param : _parameters_1) {
            _builder.newLineIfNotEmpty();
            _builder.append(" ");
            SimpleType simpleType = ((SimpleType) dataPoint);
            _builder.newLineIfNotEmpty();
            _builder.append(" ");
            {
              boolean _and = false;
              String _name_3 = simpleType.getName();
              boolean _contains = _name_3.contains("description");
              if (!_contains) {
                _and = false;
              } else {
                PrimitiveValue _value = simpleType.getValue();
                boolean _notEquals_1 = (!Objects.equal(_value, null));
                _and = _notEquals_1;
              }
              if (_and) {
                PrimitiveValue _value_1 = simpleType.getValue();
                String _findValue = ParameterValueFinder.findValue(_value_1);
                _builder.append(_findValue, " ");
              }
            }
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.append(" ");
    _builder.append("*     ");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("public class ");
    String _name_4 = dataPoint.getName();
    String _firstUpper_3 = StringExtensions.toFirstUpper(_name_4);
    _builder.append(_firstUpper_3, "");
    _builder.append(" implements IAttributeBehavior {");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**\tThe attribute name */");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private String  attributeName;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/*----- PROTECTED REGION interfaceDescription(");
    String _name_5 = controlNode.getName();
    String _firstUpper_4 = StringExtensions.toFirstUpper(_name_5);
    _builder.append(_firstUpper_4, "\t");
    _builder.append(".");
    String _name_6 = dataPoint.getName();
    String _firstUpper_5 = StringExtensions.toFirstUpper(_name_6);
    _builder.append(_firstUpper_5, "\t");
    _builder.append(".dataMembers) ENABLED START -----*/");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//\tPut your data member declarations");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private boolean attrValue;");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/*----- PROTECTED REGION END -----*/\t//\t");
    String _name_7 = controlNode.getName();
    String _firstUpper_6 = StringExtensions.toFirstUpper(_name_7);
    _builder.append(_firstUpper_6, "\t");
    _builder.append(".");
    String _name_8 = dataPoint.getName();
    String _firstUpper_7 = StringExtensions.toFirstUpper(_name_8);
    _builder.append(_firstUpper_7, "\t");
    _builder.append(".dataMembers");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Dynamic attribute ");
    String _name_9 = dataPoint.getName();
    String _firstUpper_8 = StringExtensions.toFirstUpper(_name_9);
    _builder.append(_firstUpper_8, "\t ");
    _builder.append(" constructor");
    _builder.newLineIfNotEmpty();
    _builder.append("\t ");
    _builder.append("* @param attributeName The dynamic attribute name");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public ");
    String _name_10 = dataPoint.getName();
    String _firstUpper_9 = StringExtensions.toFirstUpper(_name_10);
    _builder.append(_firstUpper_9, "\t");
    _builder.append("(String attributeName) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("this.attributeName = attributeName;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Build and return the configuration for dynamic attribute ");
    String _name_11 = dataPoint.getName();
    String _firstUpper_10 = StringExtensions.toFirstUpper(_name_11);
    _builder.append(_firstUpper_10, "\t ");
    _builder.append(".");
    _builder.newLineIfNotEmpty();
    _builder.append("\t ");
    _builder.append("* @return the configuration for dynamic attribute ");
    String _name_12 = dataPoint.getName();
    String _firstUpper_11 = StringExtensions.toFirstUpper(_name_12);
    _builder.append(_firstUpper_11, "\t ");
    _builder.append(".");
    _builder.newLineIfNotEmpty();
    _builder.append("\t ");
    _builder.append("* @throws DevFailed in case of configuration error.");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@Override");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public AttributeConfiguration getConfiguration() throws DevFailed {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("AttributeConfiguration  config = new AttributeConfiguration();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("config.setName(attributeName);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("config.setType(");
    String _findValueType_1 = ParameterValueFinder.findValueType(dataPoint);
    _builder.append(_findValueType_1, "\t\t");
    _builder.append(".class);");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("config.setFormat(AttrDataFormat.SCALAR);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("config.setWritable(AttrWriteType.READ);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("config.setDispLevel(DispLevel.OPERATOR);");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("//\tSet attribute properties");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("AttributePropertiesImpl\tproperties = new AttributePropertiesImpl();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("config.setAttributeProperties(properties);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("return config;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Get dynamic attribute state machine");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @return the attribute state machine");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @throws DevFailed if the state machine computation failed.");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@Override");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public StateMachineBehavior getStateMachine() throws DevFailed {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("StateMachineBehavior stateMachine = new StateMachineBehavior();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("/*----- PROTECTED REGION interfaceDescription(");
    String _name_13 = controlNode.getName();
    String _firstUpper_12 = StringExtensions.toFirstUpper(_name_13);
    _builder.append(_firstUpper_12, "\t\t");
    _builder.append(".");
    String _name_14 = dataPoint.getName();
    String _firstUpper_13 = StringExtensions.toFirstUpper(_name_14);
    _builder.append(_firstUpper_13, "\t\t");
    _builder.append(") ENABLED START -----*/");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("/*----- PROTECTED REGION END -----*/\t//\t");
    String _name_15 = controlNode.getName();
    String _firstUpper_14 = StringExtensions.toFirstUpper(_name_15);
    _builder.append(_firstUpper_14, "\t\t");
    _builder.append(".");
    String _name_16 = dataPoint.getName();
    String _firstUpper_15 = StringExtensions.toFirstUpper(_name_16);
    _builder.append(_firstUpper_15, "\t\t");
    _builder.append(".getStateMachine");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("return stateMachine;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Get dynamic attribute value");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @return the attribute value");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @throws DevFailed if the read value failed.");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@Override");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public AttributeValue getValue() throws DevFailed {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("boolean\treadValue;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("/*----- PROTECTED REGION interfaceDescription(");
    String _name_17 = controlNode.getName();
    String _firstUpper_16 = StringExtensions.toFirstUpper(_name_17);
    _builder.append(_firstUpper_16, "\t\t");
    _builder.append(".");
    String _name_18 = dataPoint.getName();
    String _firstUpper_17 = StringExtensions.toFirstUpper(_name_18);
    _builder.append(_firstUpper_17, "\t\t");
    _builder.append(".getValue) ENABLED START -----*/");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("readValue = attrValue;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("/*----- PROTECTED REGION END -----*/\t//\tLeafNode.DynamicBooleanAttribute.getValue");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("return new AttributeValue(readValue);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/**");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* Set dynamic attribute value");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @param writeValue the attribute value");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("* @throws DevFailed if the set value failed.");
    _builder.newLine();
    _builder.append("\t ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@Override");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public void setValue(AttributeValue writeValue) throws DevFailed {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("/*----- PROTECTED REGION interfaceDescription(");
    String _name_19 = controlNode.getName();
    String _firstUpper_18 = StringExtensions.toFirstUpper(_name_19);
    _builder.append(_firstUpper_18, "\t\t");
    _builder.append(".");
    String _name_20 = dataPoint.getName();
    String _firstUpper_19 = StringExtensions.toFirstUpper(_name_20);
    _builder.append(_firstUpper_19, "\t\t");
    _builder.append(".setValue) ENABLED START -----*/");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("attrValue = (Boolean) writeValue.getValue();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("/*----- PROTECTED REGION END -----*/\t//\t");
    String _name_21 = controlNode.getName();
    String _firstUpper_20 = StringExtensions.toFirstUpper(_name_21);
    _builder.append(_firstUpper_20, "\t\t");
    _builder.append(".");
    String _name_22 = dataPoint.getName();
    String _firstUpper_21 = StringExtensions.toFirstUpper(_name_22);
    _builder.append(_firstUpper_21, "\t\t");
    _builder.append(".setValue");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/*----- PROTECTED REGION interfaceDescription(");
    String _name_23 = controlNode.getName();
    String _firstUpper_22 = StringExtensions.toFirstUpper(_name_23);
    _builder.append(_firstUpper_22, "\t");
    _builder.append(".");
    String _name_24 = dataPoint.getName();
    String _firstUpper_23 = StringExtensions.toFirstUpper(_name_24);
    _builder.append(_firstUpper_23, "\t");
    _builder.append(".methods) ENABLED START -----*/");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("//\tPut your own methods");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("/*----- PROTECTED REGION END -----*/\t//\tLeafNode.DynamicBooleanAttribute.methods");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence declareCommand(final ControlNode controlNode) {
    CharSequence _xblockexpression = null;
    {
      List<String> commandPropertiesAsPerTango = CollectionLiterals.<String>newArrayList();
      commandPropertiesAsPerTango.add("inTypeDesc");
      commandPropertiesAsPerTango.add("outTypeDesc");
      StringConcatenation _builder = new StringConcatenation();
      {
        boolean _notEquals = (!Objects.equal(controlNode, null));
        if (_notEquals) {
          CommandResponseBlockUtility commandBlockUtil = controlNode.getCommandResponseBlocks();
          _builder.newLineIfNotEmpty();
          {
            boolean _notEquals_1 = (!Objects.equal(commandBlockUtil, null));
            if (_notEquals_1) {
              CommandResponseBlockUtility _commandResponseBlocks = controlNode.getCommandResponseBlocks();
              EList<CommandResponseBlock> commandBlockList = _commandResponseBlocks.getCommandResponseBlocks();
              _builder.newLineIfNotEmpty();
              {
                boolean _notEquals_2 = (!Objects.equal(commandBlockList, null));
                if (_notEquals_2) {
                  {
                    for(final CommandResponseBlock commandBlock : commandBlockList) {
                      _builder.append(" ");
                      Command command = commandBlock.getCommand();
                      _builder.newLineIfNotEmpty();
                      _builder.append(" ");
                      EList<Parameter> commandParameters = command.getParameters();
                      _builder.newLineIfNotEmpty();
                      _builder.append("@Command(name=\"");
                      String _name = command.getName();
                      _builder.append(_name, "");
                      _builder.append("\" ");
                      _builder.newLineIfNotEmpty();
                      {
                        boolean _notEquals_3 = (!Objects.equal(commandParameters, null));
                        if (_notEquals_3) {
                          {
                            for(final Parameter param : commandParameters) {
                              SimpleType simpleType = ((SimpleType) param);
                              _builder.newLineIfNotEmpty();
                              {
                                String _name_1 = simpleType.getName();
                                boolean _contains = commandPropertiesAsPerTango.contains(_name_1);
                                if (_contains) {
                                  {
                                    Parameter _last = IterableExtensions.<Parameter>last(commandParameters);
                                    boolean _equals = param.equals(_last);
                                    boolean _not = (!_equals);
                                    if (_not) {
                                      _builder.append(",");
                                    }
                                  }
                                  _builder.newLineIfNotEmpty();
                                  String _name_2 = simpleType.getName();
                                  _builder.append(_name_2, "");
                                  _builder.append("=");
                                  {
                                    PrimitiveValueType _type = simpleType.getType();
                                    boolean _equals_1 = _type.equals(PrimitiveValueType.STRING);
                                    if (_equals_1) {
                                      _builder.append("\"");
                                      PrimitiveValue _value = simpleType.getValue();
                                      String _findValue = ParameterValueFinder.findValue(_value);
                                      _builder.append(_findValue, "");
                                      _builder.append("\"");
                                      _builder.newLineIfNotEmpty();
                                    } else {
                                      PrimitiveValue _value_1 = simpleType.getValue();
                                      String _findValue_1 = ParameterValueFinder.findValue(_value_1);
                                      _builder.append(_findValue_1, "");
                                    }
                                  }
                                  _builder.newLineIfNotEmpty();
                                  {
                                    Parameter _last_1 = IterableExtensions.<Parameter>last(commandParameters);
                                    boolean _equals_2 = param.equals(_last_1);
                                    boolean _not_1 = (!_equals_2);
                                    if (_not_1) {
                                      _builder.append(",");
                                    }
                                  }
                                  _builder.newLineIfNotEmpty();
                                }
                              }
                            }
                          }
                        }
                      }
                      _builder.append(" ");
                      _builder.append(")");
                      _builder.newLine();
                      _builder.append(" ");
                      _builder.append("public String ");
                      String _name_3 = command.getName();
                      _builder.append(_name_3, " ");
                      _builder.append("(String ");
                      String _name_4 = command.getName();
                      _builder.append(_name_4, " ");
                      _builder.append("Params) throws DevFailed {");
                      _builder.newLineIfNotEmpty();
                      _builder.append("xlogger.entry();");
                      _builder.newLine();
                      _builder.append("\t\t");
                      _builder.append("String ");
                      String _name_5 = command.getName();
                      _builder.append(_name_5, "\t\t");
                      _builder.append("Out = null;");
                      _builder.newLineIfNotEmpty();
                      _builder.append("\t\t");
                      _builder.append("/*----- PROTECTED REGION interfaceDescription(");
                      String _name_6 = controlNode.getName();
                      String _firstUpper = StringExtensions.toFirstUpper(_name_6);
                      _builder.append(_firstUpper, "\t\t");
                      _builder.append(".");
                      String _name_7 = command.getName();
                      _builder.append(_name_7, "\t\t");
                      _builder.append(") ENABLED START -----*/");
                      _builder.newLineIfNotEmpty();
                      _builder.append("        ");
                      _builder.append("//\tPut command code here");
                      _builder.newLine();
                      _builder.append("\t\t");
                      _builder.append("xlogger.exit();");
                      _builder.newLine();
                      {
                        if (this.connect) {
                          _builder.append("\t\t");
                          String _name_8 = command.getName();
                          _builder.append(_name_8, "\t\t");
                          _builder.append("Out = new ");
                          String _name_9 = controlNode.getName();
                          String _firstUpper_1 = StringExtensions.toFirstUpper(_name_9);
                          _builder.append(_firstUpper_1, "\t\t");
                          _builder.append("Simulator().simulateResponse(\"");
                          String _name_10 = command.getName();
                          _builder.append(_name_10, "\t\t");
                          _builder.append("\",");
                          String _name_11 = command.getName();
                          _builder.append(_name_11, "\t\t");
                          _builder.append("Params);");
                          _builder.newLineIfNotEmpty();
                        }
                      }
                      _builder.append("\t\t");
                      _builder.append("return ");
                      String _name_12 = command.getName();
                      _builder.append(_name_12, "\t\t");
                      _builder.append("Out;");
                      _builder.newLineIfNotEmpty();
                      _builder.append("\t");
                      _builder.append("} ");
                      _builder.newLine();
                    }
                  }
                }
              }
            }
          }
        }
      }
      _builder.newLine();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  public CharSequence declareAttributes(final ControlNode controlNode) {
    StringConcatenation _builder = new StringConcatenation();
    {
      boolean _notEquals = (!Objects.equal(controlNode, null));
      if (_notEquals) {
        {
          DataPointBlockUtility _dataPointBlocks = controlNode.getDataPointBlocks();
          boolean _notEquals_1 = (!Objects.equal(_dataPointBlocks, null));
          if (_notEquals_1) {
            {
              DataPointBlockUtility _dataPointBlocks_1 = controlNode.getDataPointBlocks();
              EList<DataPointBlock> _dataPointBlocks_2 = _dataPointBlocks_1.getDataPointBlocks();
              boolean _notEquals_2 = (!Objects.equal(_dataPointBlocks_2, null));
              if (_notEquals_2) {
                DataPointBlockUtility _dataPointBlocks_3 = controlNode.getDataPointBlocks();
                EList<DataPointBlock> dataPointList = _dataPointBlocks_3.getDataPointBlocks();
                _builder.append(" ");
                _builder.newLineIfNotEmpty();
                {
                  boolean _notEquals_3 = (!Objects.equal(dataPointList, null));
                  if (_notEquals_3) {
                    {
                      for(final DataPointBlock dataPoint : dataPointList) {
                        _builder.append("@Attribute(name=\"");
                        DataPoint _dataPoint = dataPoint.getDataPoint();
                        String _name = _dataPoint.getName();
                        String _firstUpper = StringExtensions.toFirstUpper(_name);
                        _builder.append(_firstUpper, "");
                        _builder.append("\")");
                        _builder.newLineIfNotEmpty();
                        {
                          DataPointHandling _dataPointHandling = dataPoint.getDataPointHandling();
                          DataPointValidCondition _checkDataPoint = _dataPointHandling.getCheckDataPoint();
                          boolean _notEquals_4 = (!Objects.equal(_checkDataPoint, null));
                          if (_notEquals_4) {
                            _builder.append("@AttributeProperties(minAlarm=\"");
                            DataPointHandling _dataPointHandling_1 = dataPoint.getDataPointHandling();
                            DataPointValidCondition _checkDataPoint_1 = _dataPointHandling_1.getCheckDataPoint();
                            PrimitiveValue _checkMinValue = _checkDataPoint_1.getCheckMinValue();
                            Object _dataPointValue = this.getDataPointValue(_checkMinValue);
                            _builder.append(_dataPointValue, "");
                            _builder.append("\",maxAlarm=\"");
                            DataPointHandling _dataPointHandling_2 = dataPoint.getDataPointHandling();
                            DataPointValidCondition _checkDataPoint_2 = _dataPointHandling_2.getCheckDataPoint();
                            PrimitiveValue _checkMaxValue = _checkDataPoint_2.getCheckMaxValue();
                            Object _dataPointValue_1 = this.getDataPointValue(_checkMaxValue);
                            _builder.append(_dataPointValue_1, "");
                            _builder.append("\")");
                            _builder.newLineIfNotEmpty();
                          }
                        }
                        _builder.append("private ");
                        DataPoint _dataPoint_1 = dataPoint.getDataPoint();
                        PrimitiveValueType _type = _dataPoint_1.getType();
                        _builder.append(_type, "");
                        _builder.append(" ");
                        DataPoint _dataPoint_2 = dataPoint.getDataPoint();
                        String _name_1 = _dataPoint_2.getName();
                        _builder.append(_name_1, "");
                        _builder.append(" ");
                        {
                          DataPoint _dataPoint_3 = dataPoint.getDataPoint();
                          PrimitiveValue _value = _dataPoint_3.getValue();
                          boolean _notEquals_5 = (!Objects.equal(_value, null));
                          if (_notEquals_5) {
                            _builder.append("= ");
                            DataPoint _dataPoint_4 = dataPoint.getDataPoint();
                            PrimitiveValue _value_1 = _dataPoint_4.getValue();
                            Object _dataPointValue_2 = this.getDataPointValue(_value_1);
                            _builder.append(_dataPointValue_2, "");
                          }
                        }
                        _builder.append(";");
                        _builder.newLineIfNotEmpty();
                        _builder.append("public synchronized ");
                        DataPoint _dataPoint_5 = dataPoint.getDataPoint();
                        PrimitiveValueType _type_1 = _dataPoint_5.getType();
                        _builder.append(_type_1, "");
                        _builder.append(" get");
                        DataPoint _dataPoint_6 = dataPoint.getDataPoint();
                        String _name_2 = _dataPoint_6.getName();
                        _builder.append(_name_2, "");
                        _builder.append("(){");
                        _builder.newLineIfNotEmpty();
                        _builder.append("\t");
                        _builder.append("return this.");
                        DataPoint _dataPoint_7 = dataPoint.getDataPoint();
                        String _name_3 = _dataPoint_7.getName();
                        _builder.append(_name_3, "\t");
                        _builder.append(";");
                        _builder.newLineIfNotEmpty();
                        _builder.append("}");
                        _builder.newLine();
                        _builder.append("public synchronized void set");
                        DataPoint _dataPoint_8 = dataPoint.getDataPoint();
                        String _name_4 = _dataPoint_8.getName();
                        _builder.append(_name_4, "");
                        _builder.append("(");
                        DataPoint _dataPoint_9 = dataPoint.getDataPoint();
                        PrimitiveValueType _type_2 = _dataPoint_9.getType();
                        _builder.append(_type_2, "");
                        _builder.append(" ");
                        DataPoint _dataPoint_10 = dataPoint.getDataPoint();
                        String _name_5 = _dataPoint_10.getName();
                        _builder.append(_name_5, "");
                        _builder.append(") throws DevFailed{");
                        _builder.newLineIfNotEmpty();
                        _builder.append("\t");
                        _builder.append("this.");
                        DataPoint _dataPoint_11 = dataPoint.getDataPoint();
                        String _name_6 = _dataPoint_11.getName();
                        _builder.append(_name_6, "\t");
                        _builder.append("  = ");
                        DataPoint _dataPoint_12 = dataPoint.getDataPoint();
                        String _name_7 = _dataPoint_12.getName();
                        _builder.append(_name_7, "\t");
                        _builder.append(";");
                        _builder.newLineIfNotEmpty();
                        _builder.append("} ");
                        _builder.newLine();
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return _builder;
  }
  
  public Object getDataPointValue(final PrimitiveValue value) {
    boolean _notEquals = (!Objects.equal(value, null));
    if (_notEquals) {
      if ((value instanceof IntValueImpl)) {
        return Integer.valueOf(((IntValueImpl)value).getIntValue());
      }
      if ((value instanceof FloatValueImpl)) {
        return Float.valueOf(((FloatValueImpl)value).getFloatValue());
      }
      if ((value instanceof StringValueImpl)) {
        return ((StringValueImpl)value).getStringValue();
      }
      if ((value instanceof BoolValueImpl)) {
        return Boolean.valueOf(((BoolValueImpl)value).isBoolValue());
      }
    } else {
      return null;
    }
    return null;
  }
}
