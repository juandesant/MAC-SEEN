package Emulator.generator;

import Emulator.generator.EmulatorJavaGenerator;
import com.google.common.base.Objects;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess;
import org.eclipse.xtext.generator.OutputConfiguration;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.osgi.framework.Bundle;

@SuppressWarnings("all")
public class EmulatorGenerator extends AbstractHandler {
  @Override
  public Object execute(final ExecutionEvent event) throws ExecutionException {
    try {
      IPackageFragment tangoSrcFolder = ((IPackageFragment) null);
      BufferedInputStream is = ((BufferedInputStream) null);
      Shell _activeShell = HandlerUtil.getActiveShell(event);
      final boolean generate = MessageDialog.openConfirm(_activeShell, "Generate JAVA", 
        "Do you want to generate JAVA code?");
      if ((!generate)) {
        return null;
      } else {
        IEditorPart activeEditor = HandlerUtil.getActiveEditor(event);
        IEditorInput _editorInput = activeEditor.getEditorInput();
        IFile _adapter = _editorInput.<IFile>getAdapter(IFile.class);
        IFile file = ((IFile) _adapter);
        boolean _notEquals = (!Objects.equal(file, null));
        if (_notEquals) {
          IProject project = file.getProject();
          IJavaProject javaProject = JavaCore.create(project);
          IFolder srcGenFolder = project.getFolder("src-gen");
          try {
            boolean _exists = srcGenFolder.exists();
            boolean _not = (!_exists);
            if (_not) {
              NullProgressMonitor _nullProgressMonitor = new NullProgressMonitor();
              srcGenFolder.create(true, true, _nullProgressMonitor);
            }
            IPackageFragmentRoot srcRoot = javaProject.getPackageFragmentRoot(srcGenFolder);
            IClasspathEntry[] oldEntries = javaProject.getRawClasspath();
            List<IClasspathEntry> newEntries = CollectionLiterals.<IClasspathEntry>newArrayList(oldEntries);
            IPath _path = srcRoot.getPath();
            IClasspathEntry newEntry = JavaCore.newSourceEntry(_path);
            newEntries.add(newEntry);
            for (final IClasspathEntry ce : oldEntries) {
              boolean _equals = ce.equals(newEntry);
              if (_equals) {
                newEntries.remove(newEntry);
              }
            }
            IPackageFragment _createPackageFragment = srcRoot.createPackageFragment("com.tango.nodes.java", true, null);
            tangoSrcFolder = _createPackageFragment;
            IFolder jarFolder = project.getFolder("jars");
            boolean _exists_1 = jarFolder.exists();
            boolean _not_1 = (!_exists_1);
            if (_not_1) {
              jarFolder.create(true, true, null);
            }
            Bundle _bundle = Platform.getBundle("com.mncml.dsl.libs");
            URL fileURL = _bundle.getEntry("libs");
            URL resolvedFileLibsURL = FileLocator.toFileURL(fileURL);
            String _protocol = resolvedFileLibsURL.getProtocol();
            String _path_1 = resolvedFileLibsURL.getPath();
            URI _uRI = new URI(_protocol, _path_1, null);
            File fileLibs = new File(_uRI);
            File[] _listFiles = fileLibs.listFiles();
            List<File> jars = IterableExtensions.<File>toList(((Iterable<File>)Conversions.doWrapArray(_listFiles)));
            for (final File f : jars) {
              boolean _or = false;
              boolean _or_1 = false;
              boolean _or_2 = false;
              boolean _or_3 = false;
              boolean _or_4 = false;
              String _name = f.getName();
              boolean _contains = _name.contains("JTango");
              if (_contains) {
                _or_4 = true;
              } else {
                String _name_1 = f.getName();
                boolean _contains_1 = _name_1.contains("SKA_Dependencies");
                _or_4 = _contains_1;
              }
              if (_or_4) {
                _or_3 = true;
              } else {
                String _name_2 = f.getName();
                boolean _contains_2 = _name_2.contains("json");
                _or_3 = _contains_2;
              }
              if (_or_3) {
                _or_2 = true;
              } else {
                String _name_3 = f.getName();
                boolean _contains_3 = _name_3.contains("testng");
                _or_2 = _contains_3;
              }
              if (_or_2) {
                _or_1 = true;
              } else {
                String _name_4 = f.getName();
                boolean _contains_4 = _name_4.contains("jcommander");
                _or_1 = _contains_4;
              }
              if (_or_1) {
                _or = true;
              } else {
                String _name_5 = f.getName();
                boolean _contains_5 = _name_5.contains("tango-utilities");
                _or = _contains_5;
              }
              if (_or) {
                FileInputStream _fileInputStream = new FileInputStream(f);
                BufferedInputStream _bufferedInputStream = new BufferedInputStream(_fileInputStream);
                is = _bufferedInputStream;
                String _name_6 = f.getName();
                String _plus = ("jars/" + _name_6);
                IFile jarFile = project.getFile(_plus);
                boolean _exists_2 = jarFile.exists();
                boolean _not_2 = (!_exists_2);
                if (_not_2) {
                  jarFile.create(is, true, null);
                }
                IPath jarPath = jarFile.getFullPath();
                IClasspathEntry _newLibraryEntry = JavaCore.newLibraryEntry(jarPath, null, null);
                newEntries.add(_newLibraryEntry);
                is.close();
              }
            }
            final List<IClasspathEntry> _converted_newEntries = (List<IClasspathEntry>)newEntries;
            Set<IClasspathEntry> setOfEntries = CollectionLiterals.<IClasspathEntry>newHashSet(((IClasspathEntry[])Conversions.unwrapArray(_converted_newEntries, IClasspathEntry.class)));
            final Set<IClasspathEntry> _converted_setOfEntries = (Set<IClasspathEntry>)setOfEntries;
            javaProject.setRawClasspath(((IClasspathEntry[])Conversions.unwrapArray(_converted_setOfEntries, IClasspathEntry.class)), null);
          } catch (final Throwable _t) {
            if (_t instanceof CoreException) {
              final CoreException e = (CoreException)_t;
              e.printStackTrace();
              return null;
            } else {
              throw Exceptions.sneakyThrow(_t);
            }
          } finally {
          }
          final EclipseResourceFileSystemAccess fsa = new EclipseResourceFileSystemAccess();
          IPath _path_2 = tangoSrcFolder.getPath();
          String _string = _path_2.toString();
          fsa.setOutputPath(_string);
          Map<String, OutputConfiguration> teste = fsa.getOutputConfigurations();
          IWorkspace _workspace = project.getWorkspace();
          IWorkspaceRoot _root = _workspace.getRoot();
          fsa.setRoot(_root);
          Set<Map.Entry<String, OutputConfiguration>> _entrySet = teste.entrySet();
          Iterator<Map.Entry<String, OutputConfiguration>> it = _entrySet.iterator();
          while (it.hasNext()) {
            {
              Map.Entry<String, OutputConfiguration> next = it.next();
              OutputConfiguration out = next.getValue();
              out.isOverrideExistingResources();
              out.setCreateOutputDirectory(true);
            }
          }
          if ((activeEditor instanceof XtextEditor)) {
            IXtextDocument _document = ((XtextEditor) activeEditor).getDocument();
            _document.<Boolean>readOnly(new IUnitOfWork<Boolean, XtextResource>() {
              @Override
              public Boolean exec(final XtextResource state) throws Exception {
                EmulatorJavaGenerator emuj = new EmulatorJavaGenerator();
                emuj.setConnect(false);
                emuj.doGenerate(state, fsa);
                return Boolean.TRUE;
              }
            });
          }
        }
        return null;
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
