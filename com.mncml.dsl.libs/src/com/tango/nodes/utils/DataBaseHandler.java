package com.tango.nodes.utils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.Device;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.CommunicationFailed;
import fr.esrf.TangoApi.ConnectionFailed;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DatabaseDAODefaultImpl;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DbDevImportInfo;
import fr.esrf.TangoApi.DbDevInfo;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.NonDbDevice;
import fr.esrf.TangoApi.NonSupportedFeature;
import fr.esrf.TangoApi.WrongData;
import fr.esrf.TangoApi.WrongNameSyntax;
import fr.esrf.TangoDs.Except;

public class DataBaseHandler {

	public static void addDevice(String deviceName) throws DevFailed {
		try {
			// Database Management.
			// ----------------------
			String split[] = deviceName.split("/");
			String serverName = split[1]+"/"+split[2];
			String className = split[1];
			// Create a Database object or retrieve an existing connection
			Database dbase = ApiUtil.get_db_obj();

			// Get and display database info.
			System.out.println(dbase.get_info());

			// Build a DbDevInfo object (name, class, server)
			// to add a device into the database .
			String devname = deviceName;
			DbDevInfo devinfo = new DbDevInfo(devname, className,
					serverName);
			dbase.add_device(devinfo);
			//dbase.delete_device( "tango/admin/corvus");
		}

		catch (NonSupportedFeature e) {
			System.out.println(e.getStack());
		} catch (NonDbDevice e) {
			System.out.println(e.getStack());
		} catch (WrongData e) {
			System.out.println(e.getStack());
		} catch (WrongNameSyntax e) {
			System.out.println(e.getStack());
		} catch (ConnectionFailed e) {
			System.out.println(e.getStack());
		} catch (CommunicationFailed e) {
			System.out.println(e.getStack());
		} catch (DevFailed e) {
			Except.print_exception(e);
		}
	}
	
	public static void main(String deviceName[]) throws DevFailed {
		try{
		
			String devname = "nodes/Controller/test";
			new Database().delete_device(devname);
			/*if(new Database(devname).is_taco())
			{
				System.out.println("Yes its in database ");
				
			}
			else{
				System.out.println("No  its not");
			}*/
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
