package com.tango.nodes.utils;

/**
 * Created by admin on 15-10-2015.
 */
public enum CapabilityId {
    VHF_BAND_RECEPTION,
    UHF_BAND_RECEPTION,
    L_BAND_RECEPTION,
    RECEPTOR_INPUT,
    PULSAR_SEARCH,
    PULSAR_TIMING,
}
