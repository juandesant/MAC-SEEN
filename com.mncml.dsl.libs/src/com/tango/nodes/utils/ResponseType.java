package com.tango.nodes.utils;

/**
 * Created by admin on 18/12/15.
 */
public enum ResponseType {
    ACK_RESPONSE,
    FINAL_RESPONSE,
    INITIALIZED;

    public static ResponseType fromInt(int x) {
        switch(x) {
            case 0:
                return ACK_RESPONSE;
            case 1:
                return FINAL_RESPONSE;
            case 2:
                return INITIALIZED;
            default:
                return null;
        }
    }
}
