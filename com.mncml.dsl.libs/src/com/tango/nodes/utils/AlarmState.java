package com.tango.nodes.utils;

/**
 * Created by admin on 03-10-2015.
 */
public enum AlarmState {
    RAISE,
    CLEAR,
    TRANSIENT,
    INITIALISED;

    public AlarmState fromInt(int x)
    {
        switch(x)
        {
            case 0:
                return RAISE;
            case 1:
                return CLEAR;
            case 2:
                return TRANSIENT;
            case 3:
                return INITIALISED;
            default:
                return null;
        }
    }
}
