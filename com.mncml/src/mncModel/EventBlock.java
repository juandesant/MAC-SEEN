/**
 */
package mncModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.EventBlock#getEvent <em>Event</em>}</li>
 *   <li>{@link mncModel.EventBlock#getEventCondition <em>Event Condition</em>}</li>
 *   <li>{@link mncModel.EventBlock#getEventHandling <em>Event Handling</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getEventBlock()
 * @model
 * @generated
 */
public interface EventBlock extends EObject {
	/**
	 * Returns the value of the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' reference.
	 * @see #setEvent(Event)
	 * @see mncModel.MncModelPackage#getEventBlock_Event()
	 * @model
	 * @generated
	 */
	Event getEvent();

	/**
	 * Sets the value of the '{@link mncModel.EventBlock#getEvent <em>Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' reference.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(Event value);

	/**
	 * Returns the value of the '<em><b>Event Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Condition</em>' containment reference.
	 * @see #setEventCondition(EventTriggerCondition)
	 * @see mncModel.MncModelPackage#getEventBlock_EventCondition()
	 * @model containment="true"
	 * @generated
	 */
	EventTriggerCondition getEventCondition();

	/**
	 * Sets the value of the '{@link mncModel.EventBlock#getEventCondition <em>Event Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event Condition</em>' containment reference.
	 * @see #getEventCondition()
	 * @generated
	 */
	void setEventCondition(EventTriggerCondition value);

	/**
	 * Returns the value of the '<em><b>Event Handling</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Handling</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Handling</em>' containment reference.
	 * @see #setEventHandling(EventHandling)
	 * @see mncModel.MncModelPackage#getEventBlock_EventHandling()
	 * @model containment="true"
	 * @generated
	 */
	EventHandling getEventHandling();

	/**
	 * Sets the value of the '{@link mncModel.EventBlock#getEventHandling <em>Event Handling</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event Handling</em>' containment reference.
	 * @see #getEventHandling()
	 * @generated
	 */
	void setEventHandling(EventHandling value);

} // EventBlock
