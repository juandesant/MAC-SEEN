/**
 */
package mncModel;

import mncModel.utility.CommandDistributionUtility;
import mncModel.utility.ResponseBlockUtility;
import mncModel.utility.TransitionUtility;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Command Response Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.CommandResponseBlock#getCommand <em>Command</em>}</li>
 *   <li>{@link mncModel.CommandResponseBlock#getCommandValidation <em>Command Validation</em>}</li>
 *   <li>{@link mncModel.CommandResponseBlock#getTransition <em>Transition</em>}</li>
 *   <li>{@link mncModel.CommandResponseBlock#getCommandTriggerCondition <em>Command Trigger Condition</em>}</li>
 *   <li>{@link mncModel.CommandResponseBlock#getCommandTranslation <em>Command Translation</em>}</li>
 *   <li>{@link mncModel.CommandResponseBlock#getCommandDistributions <em>Command Distributions</em>}</li>
 *   <li>{@link mncModel.CommandResponseBlock#getResponseBlock <em>Response Block</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getCommandResponseBlock()
 * @model
 * @generated
 */
public interface CommandResponseBlock extends EObject {
	/**
	 * Returns the value of the '<em><b>Command</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Command</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command</em>' reference.
	 * @see #setCommand(Command)
	 * @see mncModel.MncModelPackage#getCommandResponseBlock_Command()
	 * @model
	 * @generated
	 */
	Command getCommand();

	/**
	 * Sets the value of the '{@link mncModel.CommandResponseBlock#getCommand <em>Command</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Command</em>' reference.
	 * @see #getCommand()
	 * @generated
	 */
	void setCommand(Command value);

	/**
	 * Returns the value of the '<em><b>Command Validation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Command Validation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command Validation</em>' containment reference.
	 * @see #setCommandValidation(CommandValidation)
	 * @see mncModel.MncModelPackage#getCommandResponseBlock_CommandValidation()
	 * @model containment="true"
	 * @generated
	 */
	CommandValidation getCommandValidation();

	/**
	 * Sets the value of the '{@link mncModel.CommandResponseBlock#getCommandValidation <em>Command Validation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Command Validation</em>' containment reference.
	 * @see #getCommandValidation()
	 * @generated
	 */
	void setCommandValidation(CommandValidation value);

	/**
	 * Returns the value of the '<em><b>Transition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition</em>' containment reference.
	 * @see #setTransition(TransitionUtility)
	 * @see mncModel.MncModelPackage#getCommandResponseBlock_Transition()
	 * @model containment="true"
	 * @generated
	 */
	TransitionUtility getTransition();

	/**
	 * Sets the value of the '{@link mncModel.CommandResponseBlock#getTransition <em>Transition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transition</em>' containment reference.
	 * @see #getTransition()
	 * @generated
	 */
	void setTransition(TransitionUtility value);

	/**
	 * Returns the value of the '<em><b>Command Trigger Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Command Trigger Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command Trigger Condition</em>' containment reference.
	 * @see #setCommandTriggerCondition(CommandTriggerCondition)
	 * @see mncModel.MncModelPackage#getCommandResponseBlock_CommandTriggerCondition()
	 * @model containment="true"
	 * @generated
	 */
	CommandTriggerCondition getCommandTriggerCondition();

	/**
	 * Sets the value of the '{@link mncModel.CommandResponseBlock#getCommandTriggerCondition <em>Command Trigger Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Command Trigger Condition</em>' containment reference.
	 * @see #getCommandTriggerCondition()
	 * @generated
	 */
	void setCommandTriggerCondition(CommandTriggerCondition value);

	/**
	 * Returns the value of the '<em><b>Command Translation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Command Translation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command Translation</em>' containment reference.
	 * @see #setCommandTranslation(CommandTranslation)
	 * @see mncModel.MncModelPackage#getCommandResponseBlock_CommandTranslation()
	 * @model containment="true"
	 * @generated
	 */
	CommandTranslation getCommandTranslation();

	/**
	 * Sets the value of the '{@link mncModel.CommandResponseBlock#getCommandTranslation <em>Command Translation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Command Translation</em>' containment reference.
	 * @see #getCommandTranslation()
	 * @generated
	 */
	void setCommandTranslation(CommandTranslation value);

	/**
	 * Returns the value of the '<em><b>Command Distributions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Command Distributions</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command Distributions</em>' containment reference.
	 * @see #setCommandDistributions(CommandDistributionUtility)
	 * @see mncModel.MncModelPackage#getCommandResponseBlock_CommandDistributions()
	 * @model containment="true"
	 * @generated
	 */
	CommandDistributionUtility getCommandDistributions();

	/**
	 * Sets the value of the '{@link mncModel.CommandResponseBlock#getCommandDistributions <em>Command Distributions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Command Distributions</em>' containment reference.
	 * @see #getCommandDistributions()
	 * @generated
	 */
	void setCommandDistributions(CommandDistributionUtility value);

	/**
	 * Returns the value of the '<em><b>Response Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Response Block</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Response Block</em>' containment reference.
	 * @see #setResponseBlock(ResponseBlockUtility)
	 * @see mncModel.MncModelPackage#getCommandResponseBlock_ResponseBlock()
	 * @model containment="true"
	 * @generated
	 */
	ResponseBlockUtility getResponseBlock();

	/**
	 * Sets the value of the '{@link mncModel.CommandResponseBlock#getResponseBlock <em>Response Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Response Block</em>' containment reference.
	 * @see #getResponseBlock()
	 * @generated
	 */
	void setResponseBlock(ResponseBlockUtility value);

} // CommandResponseBlock
