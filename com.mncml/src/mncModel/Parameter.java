/**
 */
package mncModel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mncModel.MncModelPackage#getParameter()
 * @model
 * @generated
 */
public interface Parameter extends AbstractInterfaceItems {
} // Parameter
