/**
 */
package mncModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Interface Items</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mncModel.MncModelPackage#getAbstractInterfaceItems()
 * @model
 * @generated
 */
public interface AbstractInterfaceItems extends EObject {
} // AbstractInterfaceItems
