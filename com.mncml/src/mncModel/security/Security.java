/**
 */
package mncModel.security;

import mncModel.utility.AuthorisationUtility;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Security</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.security.Security#getAuthorisations <em>Authorisations</em>}</li>
 *   <li>{@link mncModel.security.Security#getSecuredLinks <em>Secured Links</em>}</li>
 * </ul>
 *
 * @see mncModel.security.SecurityPackage#getSecurity()
 * @model
 * @generated
 */
public interface Security extends EObject {
	/**
	 * Returns the value of the '<em><b>Authorisations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Authorisations</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Authorisations</em>' containment reference.
	 * @see #setAuthorisations(AuthorisationUtility)
	 * @see mncModel.security.SecurityPackage#getSecurity_Authorisations()
	 * @model containment="true"
	 * @generated
	 */
	AuthorisationUtility getAuthorisations();

	/**
	 * Sets the value of the '{@link mncModel.security.Security#getAuthorisations <em>Authorisations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Authorisations</em>' containment reference.
	 * @see #getAuthorisations()
	 * @generated
	 */
	void setAuthorisations(AuthorisationUtility value);

	/**
	 * Returns the value of the '<em><b>Secured Links</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Secured Links</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Secured Links</em>' containment reference.
	 * @see #setSecuredLinks(SecuredLink)
	 * @see mncModel.security.SecurityPackage#getSecurity_SecuredLinks()
	 * @model containment="true"
	 * @generated
	 */
	SecuredLink getSecuredLinks();

	/**
	 * Sets the value of the '{@link mncModel.security.Security#getSecuredLinks <em>Secured Links</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Secured Links</em>' containment reference.
	 * @see #getSecuredLinks()
	 * @generated
	 */
	void setSecuredLinks(SecuredLink value);

} // Security
