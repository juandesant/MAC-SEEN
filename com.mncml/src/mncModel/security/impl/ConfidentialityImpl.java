/**
 */
package mncModel.security.impl;

import mncModel.security.Confidentiality;
import mncModel.security.SecurityPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Confidentiality</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ConfidentialityImpl extends MinimalEObjectImpl.Container implements Confidentiality {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConfidentialityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.CONFIDENTIALITY;
	}

} //ConfidentialityImpl
