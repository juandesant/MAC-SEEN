/**
 */
package mncModel.security.impl;

import java.util.Collection;

import mncModel.security.Authorisation;
import mncModel.security.SecurityPackage;

import mncModel.stakeholder.Role;
import mncModel.stakeholder.SystemControl;
import mncModel.stakeholder.UserOperation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Authorisation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.security.impl.AuthorisationImpl#getRole <em>Role</em>}</li>
 *   <li>{@link mncModel.security.impl.AuthorisationImpl#getSystemControl <em>System Control</em>}</li>
 *   <li>{@link mncModel.security.impl.AuthorisationImpl#getUserOperations <em>User Operations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AuthorisationImpl extends MinimalEObjectImpl.Container implements Authorisation {
	/**
	 * The cached value of the '{@link #getRole() <em>Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole()
	 * @generated
	 * @ordered
	 */
	protected Role role;

	/**
	 * The cached value of the '{@link #getSystemControl() <em>System Control</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemControl()
	 * @generated
	 * @ordered
	 */
	protected SystemControl systemControl;

	/**
	 * The cached value of the '{@link #getUserOperations() <em>User Operations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserOperations()
	 * @generated
	 * @ordered
	 */
	protected EList<UserOperation> userOperations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AuthorisationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.AUTHORISATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role getRole() {
		if (role != null && role.eIsProxy()) {
			InternalEObject oldRole = (InternalEObject)role;
			role = (Role)eResolveProxy(oldRole);
			if (role != oldRole) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SecurityPackage.AUTHORISATION__ROLE, oldRole, role));
			}
		}
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role basicGetRole() {
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRole(Role newRole) {
		Role oldRole = role;
		role = newRole;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SecurityPackage.AUTHORISATION__ROLE, oldRole, role));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemControl getSystemControl() {
		return systemControl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSystemControl(SystemControl newSystemControl, NotificationChain msgs) {
		SystemControl oldSystemControl = systemControl;
		systemControl = newSystemControl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SecurityPackage.AUTHORISATION__SYSTEM_CONTROL, oldSystemControl, newSystemControl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystemControl(SystemControl newSystemControl) {
		if (newSystemControl != systemControl) {
			NotificationChain msgs = null;
			if (systemControl != null)
				msgs = ((InternalEObject)systemControl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SecurityPackage.AUTHORISATION__SYSTEM_CONTROL, null, msgs);
			if (newSystemControl != null)
				msgs = ((InternalEObject)newSystemControl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SecurityPackage.AUTHORISATION__SYSTEM_CONTROL, null, msgs);
			msgs = basicSetSystemControl(newSystemControl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SecurityPackage.AUTHORISATION__SYSTEM_CONTROL, newSystemControl, newSystemControl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UserOperation> getUserOperations() {
		if (userOperations == null) {
			userOperations = new EObjectContainmentEList<UserOperation>(UserOperation.class, this, SecurityPackage.AUTHORISATION__USER_OPERATIONS);
		}
		return userOperations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SecurityPackage.AUTHORISATION__SYSTEM_CONTROL:
				return basicSetSystemControl(null, msgs);
			case SecurityPackage.AUTHORISATION__USER_OPERATIONS:
				return ((InternalEList<?>)getUserOperations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SecurityPackage.AUTHORISATION__ROLE:
				if (resolve) return getRole();
				return basicGetRole();
			case SecurityPackage.AUTHORISATION__SYSTEM_CONTROL:
				return getSystemControl();
			case SecurityPackage.AUTHORISATION__USER_OPERATIONS:
				return getUserOperations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SecurityPackage.AUTHORISATION__ROLE:
				setRole((Role)newValue);
				return;
			case SecurityPackage.AUTHORISATION__SYSTEM_CONTROL:
				setSystemControl((SystemControl)newValue);
				return;
			case SecurityPackage.AUTHORISATION__USER_OPERATIONS:
				getUserOperations().clear();
				getUserOperations().addAll((Collection<? extends UserOperation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SecurityPackage.AUTHORISATION__ROLE:
				setRole((Role)null);
				return;
			case SecurityPackage.AUTHORISATION__SYSTEM_CONTROL:
				setSystemControl((SystemControl)null);
				return;
			case SecurityPackage.AUTHORISATION__USER_OPERATIONS:
				getUserOperations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SecurityPackage.AUTHORISATION__ROLE:
				return role != null;
			case SecurityPackage.AUTHORISATION__SYSTEM_CONTROL:
				return systemControl != null;
			case SecurityPackage.AUTHORISATION__USER_OPERATIONS:
				return userOperations != null && !userOperations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AuthorisationImpl
