/**
 */
package mncModel.security;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Secured Link</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mncModel.security.SecurityPackage#getSecuredLink()
 * @model
 * @generated
 */
public interface SecuredLink extends EObject {
} // SecuredLink
