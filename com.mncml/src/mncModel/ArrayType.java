/**
 */
package mncModel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.ArrayType#getType <em>Type</em>}</li>
 *   <li>{@link mncModel.ArrayType#getName <em>Name</em>}</li>
 *   <li>{@link mncModel.ArrayType#getValues <em>Values</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getArrayType()
 * @model
 * @generated
 */
public interface ArrayType extends Parameter {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link mncModel.PrimitiveValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see mncModel.PrimitiveValueType
	 * @see #setType(PrimitiveValueType)
	 * @see mncModel.MncModelPackage#getArrayType_Type()
	 * @model
	 * @generated
	 */
	PrimitiveValueType getType();

	/**
	 * Sets the value of the '{@link mncModel.ArrayType#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see mncModel.PrimitiveValueType
	 * @see #getType()
	 * @generated
	 */
	void setType(PrimitiveValueType value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see mncModel.MncModelPackage#getArrayType_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link mncModel.ArrayType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Values</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.PrimitiveValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Values</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Values</em>' containment reference list.
	 * @see mncModel.MncModelPackage#getArrayType_Values()
	 * @model containment="true"
	 * @generated
	 */
	EList<PrimitiveValue> getValues();

} // ArrayType
