/**
 */
package mncModel.impl;

import java.util.Collection;

import mncModel.AlarmTranslation;
import mncModel.AlarmTranslationRule;
import mncModel.MncModelPackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alarm Translation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.AlarmTranslationImpl#getAlarmTranslationRules <em>Alarm Translation Rules</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AlarmTranslationImpl extends MinimalEObjectImpl.Container implements AlarmTranslation {
	/**
	 * The cached value of the '{@link #getAlarmTranslationRules() <em>Alarm Translation Rules</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlarmTranslationRules()
	 * @generated
	 * @ordered
	 */
	protected EList<AlarmTranslationRule> alarmTranslationRules;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AlarmTranslationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.ALARM_TRANSLATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AlarmTranslationRule> getAlarmTranslationRules() {
		if (alarmTranslationRules == null) {
			alarmTranslationRules = new EObjectContainmentEList<AlarmTranslationRule>(AlarmTranslationRule.class, this, MncModelPackage.ALARM_TRANSLATION__ALARM_TRANSLATION_RULES);
		}
		return alarmTranslationRules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.ALARM_TRANSLATION__ALARM_TRANSLATION_RULES:
				return ((InternalEList<?>)getAlarmTranslationRules()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.ALARM_TRANSLATION__ALARM_TRANSLATION_RULES:
				return getAlarmTranslationRules();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.ALARM_TRANSLATION__ALARM_TRANSLATION_RULES:
				getAlarmTranslationRules().clear();
				getAlarmTranslationRules().addAll((Collection<? extends AlarmTranslationRule>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.ALARM_TRANSLATION__ALARM_TRANSLATION_RULES:
				getAlarmTranslationRules().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.ALARM_TRANSLATION__ALARM_TRANSLATION_RULES:
				return alarmTranslationRules != null && !alarmTranslationRules.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AlarmTranslationImpl
