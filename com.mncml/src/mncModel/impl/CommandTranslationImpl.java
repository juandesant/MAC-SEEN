/**
 */
package mncModel.impl;

import java.util.Collection;

import mncModel.AbstractOperationableItems;
import mncModel.Command;
import mncModel.CommandTranslation;
import mncModel.MncModelPackage;
import mncModel.ParameterTranslation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Command Translation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.CommandTranslationImpl#getInputCommand <em>Input Command</em>}</li>
 *   <li>{@link mncModel.impl.CommandTranslationImpl#getTranslatedCommands <em>Translated Commands</em>}</li>
 *   <li>{@link mncModel.impl.CommandTranslationImpl#getParameterTranslations <em>Parameter Translations</em>}</li>
 *   <li>{@link mncModel.impl.CommandTranslationImpl#getOperation <em>Operation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommandTranslationImpl extends MinimalEObjectImpl.Container implements CommandTranslation {
	/**
	 * The cached value of the '{@link #getInputCommand() <em>Input Command</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputCommand()
	 * @generated
	 * @ordered
	 */
	protected Command inputCommand;

	/**
	 * The cached value of the '{@link #getTranslatedCommands() <em>Translated Commands</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTranslatedCommands()
	 * @generated
	 * @ordered
	 */
	protected EList<Command> translatedCommands;

	/**
	 * The cached value of the '{@link #getParameterTranslations() <em>Parameter Translations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterTranslations()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterTranslation> parameterTranslations;

	/**
	 * The cached value of the '{@link #getOperation() <em>Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected AbstractOperationableItems operation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommandTranslationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.COMMAND_TRANSLATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Command getInputCommand() {
		if (inputCommand != null && inputCommand.eIsProxy()) {
			InternalEObject oldInputCommand = (InternalEObject)inputCommand;
			inputCommand = (Command)eResolveProxy(oldInputCommand);
			if (inputCommand != oldInputCommand) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MncModelPackage.COMMAND_TRANSLATION__INPUT_COMMAND, oldInputCommand, inputCommand));
			}
		}
		return inputCommand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Command basicGetInputCommand() {
		return inputCommand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInputCommand(Command newInputCommand) {
		Command oldInputCommand = inputCommand;
		inputCommand = newInputCommand;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_TRANSLATION__INPUT_COMMAND, oldInputCommand, inputCommand));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Command> getTranslatedCommands() {
		if (translatedCommands == null) {
			translatedCommands = new EObjectResolvingEList<Command>(Command.class, this, MncModelPackage.COMMAND_TRANSLATION__TRANSLATED_COMMANDS);
		}
		return translatedCommands;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterTranslation> getParameterTranslations() {
		if (parameterTranslations == null) {
			parameterTranslations = new EObjectContainmentEList<ParameterTranslation>(ParameterTranslation.class, this, MncModelPackage.COMMAND_TRANSLATION__PARAMETER_TRANSLATIONS);
		}
		return parameterTranslations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractOperationableItems getOperation() {
		if (operation != null && operation.eIsProxy()) {
			InternalEObject oldOperation = (InternalEObject)operation;
			operation = (AbstractOperationableItems)eResolveProxy(oldOperation);
			if (operation != oldOperation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MncModelPackage.COMMAND_TRANSLATION__OPERATION, oldOperation, operation));
			}
		}
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractOperationableItems basicGetOperation() {
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperation(AbstractOperationableItems newOperation) {
		AbstractOperationableItems oldOperation = operation;
		operation = newOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_TRANSLATION__OPERATION, oldOperation, operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.COMMAND_TRANSLATION__PARAMETER_TRANSLATIONS:
				return ((InternalEList<?>)getParameterTranslations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.COMMAND_TRANSLATION__INPUT_COMMAND:
				if (resolve) return getInputCommand();
				return basicGetInputCommand();
			case MncModelPackage.COMMAND_TRANSLATION__TRANSLATED_COMMANDS:
				return getTranslatedCommands();
			case MncModelPackage.COMMAND_TRANSLATION__PARAMETER_TRANSLATIONS:
				return getParameterTranslations();
			case MncModelPackage.COMMAND_TRANSLATION__OPERATION:
				if (resolve) return getOperation();
				return basicGetOperation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.COMMAND_TRANSLATION__INPUT_COMMAND:
				setInputCommand((Command)newValue);
				return;
			case MncModelPackage.COMMAND_TRANSLATION__TRANSLATED_COMMANDS:
				getTranslatedCommands().clear();
				getTranslatedCommands().addAll((Collection<? extends Command>)newValue);
				return;
			case MncModelPackage.COMMAND_TRANSLATION__PARAMETER_TRANSLATIONS:
				getParameterTranslations().clear();
				getParameterTranslations().addAll((Collection<? extends ParameterTranslation>)newValue);
				return;
			case MncModelPackage.COMMAND_TRANSLATION__OPERATION:
				setOperation((AbstractOperationableItems)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.COMMAND_TRANSLATION__INPUT_COMMAND:
				setInputCommand((Command)null);
				return;
			case MncModelPackage.COMMAND_TRANSLATION__TRANSLATED_COMMANDS:
				getTranslatedCommands().clear();
				return;
			case MncModelPackage.COMMAND_TRANSLATION__PARAMETER_TRANSLATIONS:
				getParameterTranslations().clear();
				return;
			case MncModelPackage.COMMAND_TRANSLATION__OPERATION:
				setOperation((AbstractOperationableItems)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.COMMAND_TRANSLATION__INPUT_COMMAND:
				return inputCommand != null;
			case MncModelPackage.COMMAND_TRANSLATION__TRANSLATED_COMMANDS:
				return translatedCommands != null && !translatedCommands.isEmpty();
			case MncModelPackage.COMMAND_TRANSLATION__PARAMETER_TRANSLATIONS:
				return parameterTranslations != null && !parameterTranslations.isEmpty();
			case MncModelPackage.COMMAND_TRANSLATION__OPERATION:
				return operation != null;
		}
		return super.eIsSet(featureID);
	}

} //CommandTranslationImpl
