/**
 */
package mncModel.impl;

import mncModel.AbstractInterfaceItems;
import mncModel.MncModelPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Interface Items</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AbstractInterfaceItemsImpl extends MinimalEObjectImpl.Container implements AbstractInterfaceItems {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractInterfaceItemsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.ABSTRACT_INTERFACE_ITEMS;
	}

} //AbstractInterfaceItemsImpl
