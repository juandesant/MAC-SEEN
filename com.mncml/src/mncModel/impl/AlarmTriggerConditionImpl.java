/**
 */
package mncModel.impl;

import mncModel.AbstractOperationableItems;
import mncModel.AlarmTranslation;
import mncModel.AlarmTriggerCondition;
import mncModel.MncModelPackage;
import mncModel.ResponsibleItemList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alarm Trigger Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.AlarmTriggerConditionImpl#getAlarmTranslation <em>Alarm Translation</em>}</li>
 *   <li>{@link mncModel.impl.AlarmTriggerConditionImpl#getOperation <em>Operation</em>}</li>
 *   <li>{@link mncModel.impl.AlarmTriggerConditionImpl#getResponsibleItems <em>Responsible Items</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AlarmTriggerConditionImpl extends MinimalEObjectImpl.Container implements AlarmTriggerCondition {
	/**
	 * The cached value of the '{@link #getAlarmTranslation() <em>Alarm Translation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlarmTranslation()
	 * @generated
	 * @ordered
	 */
	protected AlarmTranslation alarmTranslation;

	/**
	 * The cached value of the '{@link #getOperation() <em>Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected AbstractOperationableItems operation;

	/**
	 * The cached value of the '{@link #getResponsibleItems() <em>Responsible Items</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponsibleItems()
	 * @generated
	 * @ordered
	 */
	protected ResponsibleItemList responsibleItems;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AlarmTriggerConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.ALARM_TRIGGER_CONDITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlarmTranslation getAlarmTranslation() {
		return alarmTranslation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAlarmTranslation(AlarmTranslation newAlarmTranslation, NotificationChain msgs) {
		AlarmTranslation oldAlarmTranslation = alarmTranslation;
		alarmTranslation = newAlarmTranslation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.ALARM_TRIGGER_CONDITION__ALARM_TRANSLATION, oldAlarmTranslation, newAlarmTranslation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlarmTranslation(AlarmTranslation newAlarmTranslation) {
		if (newAlarmTranslation != alarmTranslation) {
			NotificationChain msgs = null;
			if (alarmTranslation != null)
				msgs = ((InternalEObject)alarmTranslation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.ALARM_TRIGGER_CONDITION__ALARM_TRANSLATION, null, msgs);
			if (newAlarmTranslation != null)
				msgs = ((InternalEObject)newAlarmTranslation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.ALARM_TRIGGER_CONDITION__ALARM_TRANSLATION, null, msgs);
			msgs = basicSetAlarmTranslation(newAlarmTranslation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.ALARM_TRIGGER_CONDITION__ALARM_TRANSLATION, newAlarmTranslation, newAlarmTranslation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractOperationableItems getOperation() {
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperation(AbstractOperationableItems newOperation, NotificationChain msgs) {
		AbstractOperationableItems oldOperation = operation;
		operation = newOperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.ALARM_TRIGGER_CONDITION__OPERATION, oldOperation, newOperation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperation(AbstractOperationableItems newOperation) {
		if (newOperation != operation) {
			NotificationChain msgs = null;
			if (operation != null)
				msgs = ((InternalEObject)operation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.ALARM_TRIGGER_CONDITION__OPERATION, null, msgs);
			if (newOperation != null)
				msgs = ((InternalEObject)newOperation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.ALARM_TRIGGER_CONDITION__OPERATION, null, msgs);
			msgs = basicSetOperation(newOperation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.ALARM_TRIGGER_CONDITION__OPERATION, newOperation, newOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponsibleItemList getResponsibleItems() {
		return responsibleItems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResponsibleItems(ResponsibleItemList newResponsibleItems, NotificationChain msgs) {
		ResponsibleItemList oldResponsibleItems = responsibleItems;
		responsibleItems = newResponsibleItems;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.ALARM_TRIGGER_CONDITION__RESPONSIBLE_ITEMS, oldResponsibleItems, newResponsibleItems);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResponsibleItems(ResponsibleItemList newResponsibleItems) {
		if (newResponsibleItems != responsibleItems) {
			NotificationChain msgs = null;
			if (responsibleItems != null)
				msgs = ((InternalEObject)responsibleItems).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.ALARM_TRIGGER_CONDITION__RESPONSIBLE_ITEMS, null, msgs);
			if (newResponsibleItems != null)
				msgs = ((InternalEObject)newResponsibleItems).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.ALARM_TRIGGER_CONDITION__RESPONSIBLE_ITEMS, null, msgs);
			msgs = basicSetResponsibleItems(newResponsibleItems, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.ALARM_TRIGGER_CONDITION__RESPONSIBLE_ITEMS, newResponsibleItems, newResponsibleItems));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.ALARM_TRIGGER_CONDITION__ALARM_TRANSLATION:
				return basicSetAlarmTranslation(null, msgs);
			case MncModelPackage.ALARM_TRIGGER_CONDITION__OPERATION:
				return basicSetOperation(null, msgs);
			case MncModelPackage.ALARM_TRIGGER_CONDITION__RESPONSIBLE_ITEMS:
				return basicSetResponsibleItems(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.ALARM_TRIGGER_CONDITION__ALARM_TRANSLATION:
				return getAlarmTranslation();
			case MncModelPackage.ALARM_TRIGGER_CONDITION__OPERATION:
				return getOperation();
			case MncModelPackage.ALARM_TRIGGER_CONDITION__RESPONSIBLE_ITEMS:
				return getResponsibleItems();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.ALARM_TRIGGER_CONDITION__ALARM_TRANSLATION:
				setAlarmTranslation((AlarmTranslation)newValue);
				return;
			case MncModelPackage.ALARM_TRIGGER_CONDITION__OPERATION:
				setOperation((AbstractOperationableItems)newValue);
				return;
			case MncModelPackage.ALARM_TRIGGER_CONDITION__RESPONSIBLE_ITEMS:
				setResponsibleItems((ResponsibleItemList)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.ALARM_TRIGGER_CONDITION__ALARM_TRANSLATION:
				setAlarmTranslation((AlarmTranslation)null);
				return;
			case MncModelPackage.ALARM_TRIGGER_CONDITION__OPERATION:
				setOperation((AbstractOperationableItems)null);
				return;
			case MncModelPackage.ALARM_TRIGGER_CONDITION__RESPONSIBLE_ITEMS:
				setResponsibleItems((ResponsibleItemList)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.ALARM_TRIGGER_CONDITION__ALARM_TRANSLATION:
				return alarmTranslation != null;
			case MncModelPackage.ALARM_TRIGGER_CONDITION__OPERATION:
				return operation != null;
			case MncModelPackage.ALARM_TRIGGER_CONDITION__RESPONSIBLE_ITEMS:
				return responsibleItems != null;
		}
		return super.eIsSet(featureID);
	}

} //AlarmTriggerConditionImpl
