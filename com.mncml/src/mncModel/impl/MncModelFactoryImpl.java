/**
 */
package mncModel.impl;

import mncModel.AbstractInterfaceItems;
import mncModel.AbstractOperationableItems;
import mncModel.Action;
import mncModel.Address;
import mncModel.Alarm;
import mncModel.AlarmBlock;
import mncModel.AlarmHandling;
import mncModel.AlarmTranslation;
import mncModel.AlarmTranslationRule;
import mncModel.AlarmTriggerCondition;
import mncModel.ArrayType;
import mncModel.BoolValue;
import mncModel.CheckParameterCondition;
import mncModel.Command;
import mncModel.CommandDistribution;
import mncModel.CommandResponseBlock;
import mncModel.CommandTranslation;
import mncModel.CommandTriggerCondition;
import mncModel.CommandValidation;
import mncModel.ControlNode;
import mncModel.DataPoint;
import mncModel.DataPointBlock;
import mncModel.DataPointHandling;
import mncModel.DataPointTranslation;
import mncModel.DataPointTranslationRule;
import mncModel.DataPointTriggerCondition;
import mncModel.DataPointValidCondition;
import mncModel.Event;
import mncModel.EventBlock;
import mncModel.EventHandling;
import mncModel.EventTranslation;
import mncModel.EventTranslationRule;
import mncModel.EventTriggerCondition;
import mncModel.FloatValue;
import mncModel.Import;
import mncModel.IntValue;
import mncModel.InterfaceDescription;
import mncModel.MncModelFactory;
import mncModel.MncModelPackage;
import mncModel.Model;
import mncModel.OperatingState;
import mncModel.Operation;
import mncModel.Parameter;
import mncModel.ParameterTranslation;
import mncModel.Port;
import mncModel.PrimitiveValue;
import mncModel.PrimitiveValueType;
import mncModel.Publish;
import mncModel.Response;
import mncModel.ResponseBlock;
import mncModel.ResponseDistribution;
import mncModel.ResponseTranslation;
import mncModel.ResponseTranslationRule;
import mncModel.ResponseValidation;
import mncModel.ResponsibleItemList;
import mncModel.SimpleType;
import mncModel.StringValue;
import mncModel.SubscribableItemList;
import mncModel.SubscribableItems;
import mncModel.Transition;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MncModelFactoryImpl extends EFactoryImpl implements MncModelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MncModelFactory init() {
		try {
			MncModelFactory theMncModelFactory = (MncModelFactory)EPackage.Registry.INSTANCE.getEFactory(MncModelPackage.eNS_URI);
			if (theMncModelFactory != null) {
				return theMncModelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MncModelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MncModelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case MncModelPackage.MODEL: return createModel();
			case MncModelPackage.IMPORT: return createImport();
			case MncModelPackage.SYSTEM: return createSystem();
			case MncModelPackage.CONTROL_NODE: return createControlNode();
			case MncModelPackage.INTERFACE_DESCRIPTION: return createInterfaceDescription();
			case MncModelPackage.ABSTRACT_INTERFACE_ITEMS: return createAbstractInterfaceItems();
			case MncModelPackage.ABSTRACT_OPERATIONABLE_ITEMS: return createAbstractOperationableItems();
			case MncModelPackage.OPERATING_STATE: return createOperatingState();
			case MncModelPackage.COMMAND_RESPONSE_BLOCK: return createCommandResponseBlock();
			case MncModelPackage.COMMAND: return createCommand();
			case MncModelPackage.COMMAND_VALIDATION: return createCommandValidation();
			case MncModelPackage.COMMAND_DISTRIBUTION: return createCommandDistribution();
			case MncModelPackage.COMMAND_TRANSLATION: return createCommandTranslation();
			case MncModelPackage.COMMAND_TRIGGER_CONDITION: return createCommandTriggerCondition();
			case MncModelPackage.PARAMETER_TRANSLATION: return createParameterTranslation();
			case MncModelPackage.TRANSITION: return createTransition();
			case MncModelPackage.RESPONSE: return createResponse();
			case MncModelPackage.RESPONSE_BLOCK: return createResponseBlock();
			case MncModelPackage.RESPONSE_VALIDATION: return createResponseValidation();
			case MncModelPackage.RESPONSE_DISTRIBUTION: return createResponseDistribution();
			case MncModelPackage.RESPONSE_TRANSLATION: return createResponseTranslation();
			case MncModelPackage.RESPONSE_TRANSLATION_RULE: return createResponseTranslationRule();
			case MncModelPackage.PORT: return createPort();
			case MncModelPackage.EVENT_BLOCK: return createEventBlock();
			case MncModelPackage.EVENT: return createEvent();
			case MncModelPackage.EVENT_TRIGGER_CONDITION: return createEventTriggerCondition();
			case MncModelPackage.EVENT_TRANSLATION: return createEventTranslation();
			case MncModelPackage.EVENT_TRANSLATION_RULE: return createEventTranslationRule();
			case MncModelPackage.EVENT_HANDLING: return createEventHandling();
			case MncModelPackage.ALARM_BLOCK: return createAlarmBlock();
			case MncModelPackage.ALARM: return createAlarm();
			case MncModelPackage.ALARM_TRIGGER_CONDITION: return createAlarmTriggerCondition();
			case MncModelPackage.ALARM_TRANSLATION: return createAlarmTranslation();
			case MncModelPackage.ALARM_TRANSLATION_RULE: return createAlarmTranslationRule();
			case MncModelPackage.ALARM_HANDLING: return createAlarmHandling();
			case MncModelPackage.DATA_POINT_BLOCK: return createDataPointBlock();
			case MncModelPackage.DATA_POINT: return createDataPoint();
			case MncModelPackage.DATA_POINT_TRIGGER_CONDITION: return createDataPointTriggerCondition();
			case MncModelPackage.DATA_POINT_TRANSLATION: return createDataPointTranslation();
			case MncModelPackage.DATA_POINT_TRANSLATION_RULE: return createDataPointTranslationRule();
			case MncModelPackage.DATA_POINT_HANDLING: return createDataPointHandling();
			case MncModelPackage.DATA_POINT_VALID_CONDITION: return createDataPointValidCondition();
			case MncModelPackage.PARAMETER: return createParameter();
			case MncModelPackage.CHECK_PARAMETER_CONDITION: return createCheckParameterCondition();
			case MncModelPackage.ARRAY_TYPE: return createArrayType();
			case MncModelPackage.SIMPLE_TYPE: return createSimpleType();
			case MncModelPackage.PRIMITIVE_VALUE: return createPrimitiveValue();
			case MncModelPackage.INT_VALUE: return createIntValue();
			case MncModelPackage.BOOL_VALUE: return createBoolValue();
			case MncModelPackage.FLOAT_VALUE: return createFloatValue();
			case MncModelPackage.STRING_VALUE: return createStringValue();
			case MncModelPackage.OPERATION: return createOperation();
			case MncModelPackage.ADDRESS: return createAddress();
			case MncModelPackage.ACTION: return createAction();
			case MncModelPackage.PUBLISH: return createPublish();
			case MncModelPackage.SUBSCRIBABLE_ITEMS: return createSubscribableItems();
			case MncModelPackage.SUBSCRIBABLE_ITEM_LIST: return createSubscribableItemList();
			case MncModelPackage.RESPONSIBLE_ITEM_LIST: return createResponsibleItemList();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case MncModelPackage.PRIMITIVE_VALUE_TYPE:
				return createPrimitiveValueTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case MncModelPackage.PRIMITIVE_VALUE_TYPE:
				return convertPrimitiveValueTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model createModel() {
		ModelImpl model = new ModelImpl();
		return model;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Import createImport() {
		ImportImpl import_ = new ImportImpl();
		return import_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public mncModel.System createSystem() {
		SystemImpl system = new SystemImpl();
		return system;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlNode createControlNode() {
		ControlNodeImpl controlNode = new ControlNodeImpl();
		return controlNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceDescription createInterfaceDescription() {
		InterfaceDescriptionImpl interfaceDescription = new InterfaceDescriptionImpl();
		return interfaceDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractInterfaceItems createAbstractInterfaceItems() {
		AbstractInterfaceItemsImpl abstractInterfaceItems = new AbstractInterfaceItemsImpl();
		return abstractInterfaceItems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractOperationableItems createAbstractOperationableItems() {
		AbstractOperationableItemsImpl abstractOperationableItems = new AbstractOperationableItemsImpl();
		return abstractOperationableItems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatingState createOperatingState() {
		OperatingStateImpl operatingState = new OperatingStateImpl();
		return operatingState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommandResponseBlock createCommandResponseBlock() {
		CommandResponseBlockImpl commandResponseBlock = new CommandResponseBlockImpl();
		return commandResponseBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Command createCommand() {
		CommandImpl command = new CommandImpl();
		return command;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommandValidation createCommandValidation() {
		CommandValidationImpl commandValidation = new CommandValidationImpl();
		return commandValidation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommandDistribution createCommandDistribution() {
		CommandDistributionImpl commandDistribution = new CommandDistributionImpl();
		return commandDistribution;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommandTranslation createCommandTranslation() {
		CommandTranslationImpl commandTranslation = new CommandTranslationImpl();
		return commandTranslation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommandTriggerCondition createCommandTriggerCondition() {
		CommandTriggerConditionImpl commandTriggerCondition = new CommandTriggerConditionImpl();
		return commandTriggerCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterTranslation createParameterTranslation() {
		ParameterTranslationImpl parameterTranslation = new ParameterTranslationImpl();
		return parameterTranslation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition createTransition() {
		TransitionImpl transition = new TransitionImpl();
		return transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Response createResponse() {
		ResponseImpl response = new ResponseImpl();
		return response;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseBlock createResponseBlock() {
		ResponseBlockImpl responseBlock = new ResponseBlockImpl();
		return responseBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseValidation createResponseValidation() {
		ResponseValidationImpl responseValidation = new ResponseValidationImpl();
		return responseValidation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseDistribution createResponseDistribution() {
		ResponseDistributionImpl responseDistribution = new ResponseDistributionImpl();
		return responseDistribution;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseTranslation createResponseTranslation() {
		ResponseTranslationImpl responseTranslation = new ResponseTranslationImpl();
		return responseTranslation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseTranslationRule createResponseTranslationRule() {
		ResponseTranslationRuleImpl responseTranslationRule = new ResponseTranslationRuleImpl();
		return responseTranslationRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port createPort() {
		PortImpl port = new PortImpl();
		return port;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventBlock createEventBlock() {
		EventBlockImpl eventBlock = new EventBlockImpl();
		return eventBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event createEvent() {
		EventImpl event = new EventImpl();
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventTriggerCondition createEventTriggerCondition() {
		EventTriggerConditionImpl eventTriggerCondition = new EventTriggerConditionImpl();
		return eventTriggerCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventTranslation createEventTranslation() {
		EventTranslationImpl eventTranslation = new EventTranslationImpl();
		return eventTranslation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventTranslationRule createEventTranslationRule() {
		EventTranslationRuleImpl eventTranslationRule = new EventTranslationRuleImpl();
		return eventTranslationRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventHandling createEventHandling() {
		EventHandlingImpl eventHandling = new EventHandlingImpl();
		return eventHandling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlarmBlock createAlarmBlock() {
		AlarmBlockImpl alarmBlock = new AlarmBlockImpl();
		return alarmBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Alarm createAlarm() {
		AlarmImpl alarm = new AlarmImpl();
		return alarm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlarmTriggerCondition createAlarmTriggerCondition() {
		AlarmTriggerConditionImpl alarmTriggerCondition = new AlarmTriggerConditionImpl();
		return alarmTriggerCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlarmTranslation createAlarmTranslation() {
		AlarmTranslationImpl alarmTranslation = new AlarmTranslationImpl();
		return alarmTranslation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlarmTranslationRule createAlarmTranslationRule() {
		AlarmTranslationRuleImpl alarmTranslationRule = new AlarmTranslationRuleImpl();
		return alarmTranslationRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlarmHandling createAlarmHandling() {
		AlarmHandlingImpl alarmHandling = new AlarmHandlingImpl();
		return alarmHandling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPointBlock createDataPointBlock() {
		DataPointBlockImpl dataPointBlock = new DataPointBlockImpl();
		return dataPointBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPoint createDataPoint() {
		DataPointImpl dataPoint = new DataPointImpl();
		return dataPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPointTriggerCondition createDataPointTriggerCondition() {
		DataPointTriggerConditionImpl dataPointTriggerCondition = new DataPointTriggerConditionImpl();
		return dataPointTriggerCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPointTranslation createDataPointTranslation() {
		DataPointTranslationImpl dataPointTranslation = new DataPointTranslationImpl();
		return dataPointTranslation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPointTranslationRule createDataPointTranslationRule() {
		DataPointTranslationRuleImpl dataPointTranslationRule = new DataPointTranslationRuleImpl();
		return dataPointTranslationRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPointHandling createDataPointHandling() {
		DataPointHandlingImpl dataPointHandling = new DataPointHandlingImpl();
		return dataPointHandling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPointValidCondition createDataPointValidCondition() {
		DataPointValidConditionImpl dataPointValidCondition = new DataPointValidConditionImpl();
		return dataPointValidCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Parameter createParameter() {
		ParameterImpl parameter = new ParameterImpl();
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckParameterCondition createCheckParameterCondition() {
		CheckParameterConditionImpl checkParameterCondition = new CheckParameterConditionImpl();
		return checkParameterCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrayType createArrayType() {
		ArrayTypeImpl arrayType = new ArrayTypeImpl();
		return arrayType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType createSimpleType() {
		SimpleTypeImpl simpleType = new SimpleTypeImpl();
		return simpleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitiveValue createPrimitiveValue() {
		PrimitiveValueImpl primitiveValue = new PrimitiveValueImpl();
		return primitiveValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntValue createIntValue() {
		IntValueImpl intValue = new IntValueImpl();
		return intValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BoolValue createBoolValue() {
		BoolValueImpl boolValue = new BoolValueImpl();
		return boolValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatValue createFloatValue() {
		FloatValueImpl floatValue = new FloatValueImpl();
		return floatValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringValue createStringValue() {
		StringValueImpl stringValue = new StringValueImpl();
		return stringValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation createOperation() {
		OperationImpl operation = new OperationImpl();
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Address createAddress() {
		AddressImpl address = new AddressImpl();
		return address;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action createAction() {
		ActionImpl action = new ActionImpl();
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Publish createPublish() {
		PublishImpl publish = new PublishImpl();
		return publish;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubscribableItems createSubscribableItems() {
		SubscribableItemsImpl subscribableItems = new SubscribableItemsImpl();
		return subscribableItems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubscribableItemList createSubscribableItemList() {
		SubscribableItemListImpl subscribableItemList = new SubscribableItemListImpl();
		return subscribableItemList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponsibleItemList createResponsibleItemList() {
		ResponsibleItemListImpl responsibleItemList = new ResponsibleItemListImpl();
		return responsibleItemList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitiveValueType createPrimitiveValueTypeFromString(EDataType eDataType, String initialValue) {
		PrimitiveValueType result = PrimitiveValueType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPrimitiveValueTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MncModelPackage getMncModelPackage() {
		return (MncModelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MncModelPackage getPackage() {
		return MncModelPackage.eINSTANCE;
	}

} //MncModelFactoryImpl
