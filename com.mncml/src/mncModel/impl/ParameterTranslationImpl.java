/**
 */
package mncModel.impl;

import java.util.Collection;

import mncModel.MncModelPackage;
import mncModel.Parameter;
import mncModel.ParameterTranslation;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter Translation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.ParameterTranslationImpl#getInputParameters <em>Input Parameters</em>}</li>
 *   <li>{@link mncModel.impl.ParameterTranslationImpl#getTranslatedParameters <em>Translated Parameters</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParameterTranslationImpl extends MinimalEObjectImpl.Container implements ParameterTranslation {
	/**
	 * The cached value of the '{@link #getInputParameters() <em>Input Parameters</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<Parameter> inputParameters;

	/**
	 * The cached value of the '{@link #getTranslatedParameters() <em>Translated Parameters</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTranslatedParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<Parameter> translatedParameters;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterTranslationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.PARAMETER_TRANSLATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Parameter> getInputParameters() {
		if (inputParameters == null) {
			inputParameters = new EObjectResolvingEList<Parameter>(Parameter.class, this, MncModelPackage.PARAMETER_TRANSLATION__INPUT_PARAMETERS);
		}
		return inputParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Parameter> getTranslatedParameters() {
		if (translatedParameters == null) {
			translatedParameters = new EObjectResolvingEList<Parameter>(Parameter.class, this, MncModelPackage.PARAMETER_TRANSLATION__TRANSLATED_PARAMETERS);
		}
		return translatedParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.PARAMETER_TRANSLATION__INPUT_PARAMETERS:
				return getInputParameters();
			case MncModelPackage.PARAMETER_TRANSLATION__TRANSLATED_PARAMETERS:
				return getTranslatedParameters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.PARAMETER_TRANSLATION__INPUT_PARAMETERS:
				getInputParameters().clear();
				getInputParameters().addAll((Collection<? extends Parameter>)newValue);
				return;
			case MncModelPackage.PARAMETER_TRANSLATION__TRANSLATED_PARAMETERS:
				getTranslatedParameters().clear();
				getTranslatedParameters().addAll((Collection<? extends Parameter>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.PARAMETER_TRANSLATION__INPUT_PARAMETERS:
				getInputParameters().clear();
				return;
			case MncModelPackage.PARAMETER_TRANSLATION__TRANSLATED_PARAMETERS:
				getTranslatedParameters().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.PARAMETER_TRANSLATION__INPUT_PARAMETERS:
				return inputParameters != null && !inputParameters.isEmpty();
			case MncModelPackage.PARAMETER_TRANSLATION__TRANSLATED_PARAMETERS:
				return translatedParameters != null && !translatedParameters.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ParameterTranslationImpl
