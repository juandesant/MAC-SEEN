/**
 */
package mncModel.impl;

import mncModel.MncModelPackage;
import mncModel.SubscribableItems;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Subscribable Items</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SubscribableItemsImpl extends MinimalEObjectImpl.Container implements SubscribableItems {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubscribableItemsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.SUBSCRIBABLE_ITEMS;
	}

} //SubscribableItemsImpl
