/**
 */
package mncModel.utility;

import mncModel.security.Authorisation;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Authorisation Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.AuthorisationUtility#getAuthorisations <em>Authorisations</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getAuthorisationUtility()
 * @model
 * @generated
 */
public interface AuthorisationUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Authorisations</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.security.Authorisation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Authorisations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Authorisations</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getAuthorisationUtility_Authorisations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Authorisation> getAuthorisations();

} // AuthorisationUtility
