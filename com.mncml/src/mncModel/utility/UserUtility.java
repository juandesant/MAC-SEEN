/**
 */
package mncModel.utility;

import mncModel.stakeholder.User;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.UserUtility#getUsers <em>Users</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getUserUtility()
 * @model
 * @generated
 */
public interface UserUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Users</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.stakeholder.User}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Users</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Users</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getUserUtility_Users()
	 * @model containment="true"
	 * @generated
	 */
	EList<User> getUsers();

} // UserUtility
