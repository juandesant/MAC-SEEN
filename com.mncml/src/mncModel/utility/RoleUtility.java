/**
 */
package mncModel.utility;

import mncModel.stakeholder.Role;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.RoleUtility#getRoles <em>Roles</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getRoleUtility()
 * @model
 * @generated
 */
public interface RoleUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Roles</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.stakeholder.Role}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roles</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roles</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getRoleUtility_Roles()
	 * @model containment="true"
	 * @generated
	 */
	EList<Role> getRoles();

} // RoleUtility
