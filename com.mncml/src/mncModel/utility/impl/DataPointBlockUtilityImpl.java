/**
 */
package mncModel.utility.impl;

import java.util.Collection;

import mncModel.DataPointBlock;

import mncModel.utility.DataPointBlockUtility;
import mncModel.utility.UtilityPackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Point Block Utility</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.impl.DataPointBlockUtilityImpl#getDataPointBlocks <em>Data Point Blocks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataPointBlockUtilityImpl extends MinimalEObjectImpl.Container implements DataPointBlockUtility {
	/**
	 * The cached value of the '{@link #getDataPointBlocks() <em>Data Point Blocks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataPointBlocks()
	 * @generated
	 * @ordered
	 */
	protected EList<DataPointBlock> dataPointBlocks;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataPointBlockUtilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UtilityPackage.Literals.DATA_POINT_BLOCK_UTILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataPointBlock> getDataPointBlocks() {
		if (dataPointBlocks == null) {
			dataPointBlocks = new EObjectContainmentEList<DataPointBlock>(DataPointBlock.class, this, UtilityPackage.DATA_POINT_BLOCK_UTILITY__DATA_POINT_BLOCKS);
		}
		return dataPointBlocks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UtilityPackage.DATA_POINT_BLOCK_UTILITY__DATA_POINT_BLOCKS:
				return ((InternalEList<?>)getDataPointBlocks()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UtilityPackage.DATA_POINT_BLOCK_UTILITY__DATA_POINT_BLOCKS:
				return getDataPointBlocks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UtilityPackage.DATA_POINT_BLOCK_UTILITY__DATA_POINT_BLOCKS:
				getDataPointBlocks().clear();
				getDataPointBlocks().addAll((Collection<? extends DataPointBlock>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UtilityPackage.DATA_POINT_BLOCK_UTILITY__DATA_POINT_BLOCKS:
				getDataPointBlocks().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UtilityPackage.DATA_POINT_BLOCK_UTILITY__DATA_POINT_BLOCKS:
				return dataPointBlocks != null && !dataPointBlocks.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataPointBlockUtilityImpl
