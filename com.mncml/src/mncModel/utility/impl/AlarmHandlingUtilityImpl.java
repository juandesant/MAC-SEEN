/**
 */
package mncModel.utility.impl;

import java.util.Collection;

import mncModel.AlarmHandling;

import mncModel.utility.AlarmHandlingUtility;
import mncModel.utility.UtilityPackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alarm Handling Utility</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.impl.AlarmHandlingUtilityImpl#getAlarmHandling <em>Alarm Handling</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AlarmHandlingUtilityImpl extends MinimalEObjectImpl.Container implements AlarmHandlingUtility {
	/**
	 * The cached value of the '{@link #getAlarmHandling() <em>Alarm Handling</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlarmHandling()
	 * @generated
	 * @ordered
	 */
	protected EList<AlarmHandling> alarmHandling;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AlarmHandlingUtilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UtilityPackage.Literals.ALARM_HANDLING_UTILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AlarmHandling> getAlarmHandling() {
		if (alarmHandling == null) {
			alarmHandling = new EObjectContainmentEList<AlarmHandling>(AlarmHandling.class, this, UtilityPackage.ALARM_HANDLING_UTILITY__ALARM_HANDLING);
		}
		return alarmHandling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UtilityPackage.ALARM_HANDLING_UTILITY__ALARM_HANDLING:
				return ((InternalEList<?>)getAlarmHandling()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UtilityPackage.ALARM_HANDLING_UTILITY__ALARM_HANDLING:
				return getAlarmHandling();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UtilityPackage.ALARM_HANDLING_UTILITY__ALARM_HANDLING:
				getAlarmHandling().clear();
				getAlarmHandling().addAll((Collection<? extends AlarmHandling>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UtilityPackage.ALARM_HANDLING_UTILITY__ALARM_HANDLING:
				getAlarmHandling().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UtilityPackage.ALARM_HANDLING_UTILITY__ALARM_HANDLING:
				return alarmHandling != null && !alarmHandling.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AlarmHandlingUtilityImpl
