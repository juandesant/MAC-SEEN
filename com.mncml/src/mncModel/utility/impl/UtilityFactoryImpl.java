/**
 */
package mncModel.utility.impl;

import mncModel.utility.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UtilityFactoryImpl extends EFactoryImpl implements UtilityFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UtilityFactory init() {
		try {
			UtilityFactory theUtilityFactory = (UtilityFactory)EPackage.Registry.INSTANCE.getEFactory(UtilityPackage.eNS_URI);
			if (theUtilityFactory != null) {
				return theUtilityFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new UtilityFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UtilityFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case UtilityPackage.COMMAND_VALIDATION_UTILITY: return createCommandValidationUtility();
			case UtilityPackage.ALARM_HANDLING_UTILITY: return createAlarmHandlingUtility();
			case UtilityPackage.DATA_POINT_HANDLING_UTILITY: return createDataPointHandlingUtility();
			case UtilityPackage.OPERATION_UTILITY: return createOperationUtility();
			case UtilityPackage.COMMAND_UTILITY: return createCommandUtility();
			case UtilityPackage.RESPONSE_VALIDATION_UTILITY: return createResponseValidationUtility();
			case UtilityPackage.ALARM_CONDITION_UTILITY: return createAlarmConditionUtility();
			case UtilityPackage.DATA_POINT_UTILITY: return createDataPointUtility();
			case UtilityPackage.ALARM_UTILITY: return createAlarmUtility();
			case UtilityPackage.EVENT_UTILITY: return createEventUtility();
			case UtilityPackage.RESPONSE_UTILITY: return createResponseUtility();
			case UtilityPackage.STATE_UTILITY: return createStateUtility();
			case UtilityPackage.AUTHORISATION_UTILITY: return createAuthorisationUtility();
			case UtilityPackage.ROLE_UTILITY: return createRoleUtility();
			case UtilityPackage.USER_UTILITY: return createUserUtility();
			case UtilityPackage.TRANSITION_UTILITY: return createTransitionUtility();
			case UtilityPackage.COMMAND_RESPONSE_BLOCK_UTILITY: return createCommandResponseBlockUtility();
			case UtilityPackage.RESPONSE_BLOCK_UTILITY: return createResponseBlockUtility();
			case UtilityPackage.EVENT_BLOCK_UTILITY: return createEventBlockUtility();
			case UtilityPackage.ALARM_BLOCK_UTILITY: return createAlarmBlockUtility();
			case UtilityPackage.DATA_POINT_BLOCK_UTILITY: return createDataPointBlockUtility();
			case UtilityPackage.COMMAND_DISTRIBUTION_UTILITY: return createCommandDistributionUtility();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommandValidationUtility createCommandValidationUtility() {
		CommandValidationUtilityImpl commandValidationUtility = new CommandValidationUtilityImpl();
		return commandValidationUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlarmHandlingUtility createAlarmHandlingUtility() {
		AlarmHandlingUtilityImpl alarmHandlingUtility = new AlarmHandlingUtilityImpl();
		return alarmHandlingUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPointHandlingUtility createDataPointHandlingUtility() {
		DataPointHandlingUtilityImpl dataPointHandlingUtility = new DataPointHandlingUtilityImpl();
		return dataPointHandlingUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationUtility createOperationUtility() {
		OperationUtilityImpl operationUtility = new OperationUtilityImpl();
		return operationUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommandUtility createCommandUtility() {
		CommandUtilityImpl commandUtility = new CommandUtilityImpl();
		return commandUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseValidationUtility createResponseValidationUtility() {
		ResponseValidationUtilityImpl responseValidationUtility = new ResponseValidationUtilityImpl();
		return responseValidationUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlarmConditionUtility createAlarmConditionUtility() {
		AlarmConditionUtilityImpl alarmConditionUtility = new AlarmConditionUtilityImpl();
		return alarmConditionUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPointUtility createDataPointUtility() {
		DataPointUtilityImpl dataPointUtility = new DataPointUtilityImpl();
		return dataPointUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlarmUtility createAlarmUtility() {
		AlarmUtilityImpl alarmUtility = new AlarmUtilityImpl();
		return alarmUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventUtility createEventUtility() {
		EventUtilityImpl eventUtility = new EventUtilityImpl();
		return eventUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseUtility createResponseUtility() {
		ResponseUtilityImpl responseUtility = new ResponseUtilityImpl();
		return responseUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateUtility createStateUtility() {
		StateUtilityImpl stateUtility = new StateUtilityImpl();
		return stateUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AuthorisationUtility createAuthorisationUtility() {
		AuthorisationUtilityImpl authorisationUtility = new AuthorisationUtilityImpl();
		return authorisationUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleUtility createRoleUtility() {
		RoleUtilityImpl roleUtility = new RoleUtilityImpl();
		return roleUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserUtility createUserUtility() {
		UserUtilityImpl userUtility = new UserUtilityImpl();
		return userUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransitionUtility createTransitionUtility() {
		TransitionUtilityImpl transitionUtility = new TransitionUtilityImpl();
		return transitionUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommandResponseBlockUtility createCommandResponseBlockUtility() {
		CommandResponseBlockUtilityImpl commandResponseBlockUtility = new CommandResponseBlockUtilityImpl();
		return commandResponseBlockUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseBlockUtility createResponseBlockUtility() {
		ResponseBlockUtilityImpl responseBlockUtility = new ResponseBlockUtilityImpl();
		return responseBlockUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventBlockUtility createEventBlockUtility() {
		EventBlockUtilityImpl eventBlockUtility = new EventBlockUtilityImpl();
		return eventBlockUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlarmBlockUtility createAlarmBlockUtility() {
		AlarmBlockUtilityImpl alarmBlockUtility = new AlarmBlockUtilityImpl();
		return alarmBlockUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPointBlockUtility createDataPointBlockUtility() {
		DataPointBlockUtilityImpl dataPointBlockUtility = new DataPointBlockUtilityImpl();
		return dataPointBlockUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommandDistributionUtility createCommandDistributionUtility() {
		CommandDistributionUtilityImpl commandDistributionUtility = new CommandDistributionUtilityImpl();
		return commandDistributionUtility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UtilityPackage getUtilityPackage() {
		return (UtilityPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static UtilityPackage getPackage() {
		return UtilityPackage.eINSTANCE;
	}

} //UtilityFactoryImpl
