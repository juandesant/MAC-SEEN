/**
 */
package mncModel.utility.impl;

import java.util.Collection;

import mncModel.DataPointHandling;

import mncModel.utility.DataPointHandlingUtility;
import mncModel.utility.UtilityPackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Point Handling Utility</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.impl.DataPointHandlingUtilityImpl#getDataPointHandling <em>Data Point Handling</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataPointHandlingUtilityImpl extends MinimalEObjectImpl.Container implements DataPointHandlingUtility {
	/**
	 * The cached value of the '{@link #getDataPointHandling() <em>Data Point Handling</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataPointHandling()
	 * @generated
	 * @ordered
	 */
	protected EList<DataPointHandling> dataPointHandling;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataPointHandlingUtilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UtilityPackage.Literals.DATA_POINT_HANDLING_UTILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataPointHandling> getDataPointHandling() {
		if (dataPointHandling == null) {
			dataPointHandling = new EObjectContainmentEList<DataPointHandling>(DataPointHandling.class, this, UtilityPackage.DATA_POINT_HANDLING_UTILITY__DATA_POINT_HANDLING);
		}
		return dataPointHandling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UtilityPackage.DATA_POINT_HANDLING_UTILITY__DATA_POINT_HANDLING:
				return ((InternalEList<?>)getDataPointHandling()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UtilityPackage.DATA_POINT_HANDLING_UTILITY__DATA_POINT_HANDLING:
				return getDataPointHandling();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UtilityPackage.DATA_POINT_HANDLING_UTILITY__DATA_POINT_HANDLING:
				getDataPointHandling().clear();
				getDataPointHandling().addAll((Collection<? extends DataPointHandling>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UtilityPackage.DATA_POINT_HANDLING_UTILITY__DATA_POINT_HANDLING:
				getDataPointHandling().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UtilityPackage.DATA_POINT_HANDLING_UTILITY__DATA_POINT_HANDLING:
				return dataPointHandling != null && !dataPointHandling.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataPointHandlingUtilityImpl
