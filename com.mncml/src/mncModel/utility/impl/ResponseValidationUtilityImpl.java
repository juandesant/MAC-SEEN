/**
 */
package mncModel.utility.impl;

import java.util.Collection;

import mncModel.ResponseValidation;

import mncModel.utility.ResponseValidationUtility;
import mncModel.utility.UtilityPackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Response Validation Utility</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.impl.ResponseValidationUtilityImpl#getResponseValidations <em>Response Validations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResponseValidationUtilityImpl extends MinimalEObjectImpl.Container implements ResponseValidationUtility {
	/**
	 * The cached value of the '{@link #getResponseValidations() <em>Response Validations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponseValidations()
	 * @generated
	 * @ordered
	 */
	protected EList<ResponseValidation> responseValidations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResponseValidationUtilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UtilityPackage.Literals.RESPONSE_VALIDATION_UTILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ResponseValidation> getResponseValidations() {
		if (responseValidations == null) {
			responseValidations = new EObjectContainmentEList<ResponseValidation>(ResponseValidation.class, this, UtilityPackage.RESPONSE_VALIDATION_UTILITY__RESPONSE_VALIDATIONS);
		}
		return responseValidations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UtilityPackage.RESPONSE_VALIDATION_UTILITY__RESPONSE_VALIDATIONS:
				return ((InternalEList<?>)getResponseValidations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UtilityPackage.RESPONSE_VALIDATION_UTILITY__RESPONSE_VALIDATIONS:
				return getResponseValidations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UtilityPackage.RESPONSE_VALIDATION_UTILITY__RESPONSE_VALIDATIONS:
				getResponseValidations().clear();
				getResponseValidations().addAll((Collection<? extends ResponseValidation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UtilityPackage.RESPONSE_VALIDATION_UTILITY__RESPONSE_VALIDATIONS:
				getResponseValidations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UtilityPackage.RESPONSE_VALIDATION_UTILITY__RESPONSE_VALIDATIONS:
				return responseValidations != null && !responseValidations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ResponseValidationUtilityImpl
