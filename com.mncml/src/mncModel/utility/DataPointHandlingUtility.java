/**
 */
package mncModel.utility;

import mncModel.DataPointHandling;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Point Handling Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.DataPointHandlingUtility#getDataPointHandling <em>Data Point Handling</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getDataPointHandlingUtility()
 * @model
 * @generated
 */
public interface DataPointHandlingUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Data Point Handling</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.DataPointHandling}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Point Handling</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Point Handling</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getDataPointHandlingUtility_DataPointHandling()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataPointHandling> getDataPointHandling();

} // DataPointHandlingUtility
