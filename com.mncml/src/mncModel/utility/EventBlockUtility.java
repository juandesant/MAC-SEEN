/**
 */
package mncModel.utility;

import mncModel.EventBlock;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Block Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.EventBlockUtility#getEventBlocks <em>Event Blocks</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getEventBlockUtility()
 * @model
 * @generated
 */
public interface EventBlockUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Event Blocks</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.EventBlock}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Blocks</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Blocks</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getEventBlockUtility_EventBlocks()
	 * @model containment="true"
	 * @generated
	 */
	EList<EventBlock> getEventBlocks();

} // EventBlockUtility
