/**
 */
package mncModel.utility;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see mncModel.utility.UtilityFactory
 * @model kind="package"
 * @generated
 */
public interface UtilityPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "utility";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://mncModel/utility/2.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "utility";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UtilityPackage eINSTANCE = mncModel.utility.impl.UtilityPackageImpl.init();

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.CommandValidationUtilityImpl <em>Command Validation Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.CommandValidationUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getCommandValidationUtility()
	 * @generated
	 */
	int COMMAND_VALIDATION_UTILITY = 0;

	/**
	 * The feature id for the '<em><b>Command Validations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_VALIDATION_UTILITY__COMMAND_VALIDATIONS = 0;

	/**
	 * The number of structural features of the '<em>Command Validation Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_VALIDATION_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.AlarmHandlingUtilityImpl <em>Alarm Handling Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.AlarmHandlingUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getAlarmHandlingUtility()
	 * @generated
	 */
	int ALARM_HANDLING_UTILITY = 1;

	/**
	 * The feature id for the '<em><b>Alarm Handling</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_HANDLING_UTILITY__ALARM_HANDLING = 0;

	/**
	 * The number of structural features of the '<em>Alarm Handling Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_HANDLING_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.DataPointHandlingUtilityImpl <em>Data Point Handling Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.DataPointHandlingUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getDataPointHandlingUtility()
	 * @generated
	 */
	int DATA_POINT_HANDLING_UTILITY = 2;

	/**
	 * The feature id for the '<em><b>Data Point Handling</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_HANDLING_UTILITY__DATA_POINT_HANDLING = 0;

	/**
	 * The number of structural features of the '<em>Data Point Handling Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_HANDLING_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.OperationUtilityImpl <em>Operation Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.OperationUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getOperationUtility()
	 * @generated
	 */
	int OPERATION_UTILITY = 3;

	/**
	 * The feature id for the '<em><b>Operations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_UTILITY__OPERATIONS = 0;

	/**
	 * The number of structural features of the '<em>Operation Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.CommandUtilityImpl <em>Command Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.CommandUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getCommandUtility()
	 * @generated
	 */
	int COMMAND_UTILITY = 4;

	/**
	 * The feature id for the '<em><b>Commands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_UTILITY__COMMANDS = 0;

	/**
	 * The number of structural features of the '<em>Command Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.ResponseValidationUtilityImpl <em>Response Validation Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.ResponseValidationUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getResponseValidationUtility()
	 * @generated
	 */
	int RESPONSE_VALIDATION_UTILITY = 5;

	/**
	 * The feature id for the '<em><b>Response Validations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_VALIDATION_UTILITY__RESPONSE_VALIDATIONS = 0;

	/**
	 * The number of structural features of the '<em>Response Validation Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_VALIDATION_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.AlarmConditionUtilityImpl <em>Alarm Condition Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.AlarmConditionUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getAlarmConditionUtility()
	 * @generated
	 */
	int ALARM_CONDITION_UTILITY = 6;

	/**
	 * The feature id for the '<em><b>Alarm Conditions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_CONDITION_UTILITY__ALARM_CONDITIONS = 0;

	/**
	 * The number of structural features of the '<em>Alarm Condition Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_CONDITION_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.DataPointUtilityImpl <em>Data Point Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.DataPointUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getDataPointUtility()
	 * @generated
	 */
	int DATA_POINT_UTILITY = 7;

	/**
	 * The feature id for the '<em><b>Data Points</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_UTILITY__DATA_POINTS = 0;

	/**
	 * The number of structural features of the '<em>Data Point Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.AlarmUtilityImpl <em>Alarm Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.AlarmUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getAlarmUtility()
	 * @generated
	 */
	int ALARM_UTILITY = 8;

	/**
	 * The feature id for the '<em><b>Alarms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_UTILITY__ALARMS = 0;

	/**
	 * The number of structural features of the '<em>Alarm Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.EventUtilityImpl <em>Event Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.EventUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getEventUtility()
	 * @generated
	 */
	int EVENT_UTILITY = 9;

	/**
	 * The feature id for the '<em><b>Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_UTILITY__EVENTS = 0;

	/**
	 * The number of structural features of the '<em>Event Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.ResponseUtilityImpl <em>Response Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.ResponseUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getResponseUtility()
	 * @generated
	 */
	int RESPONSE_UTILITY = 10;

	/**
	 * The feature id for the '<em><b>Responses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_UTILITY__RESPONSES = 0;

	/**
	 * The number of structural features of the '<em>Response Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.StateUtilityImpl <em>State Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.StateUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getStateUtility()
	 * @generated
	 */
	int STATE_UTILITY = 11;

	/**
	 * The feature id for the '<em><b>Operating States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_UTILITY__OPERATING_STATES = 0;

	/**
	 * The feature id for the '<em><b>Start State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_UTILITY__START_STATE = 1;

	/**
	 * The feature id for the '<em><b>End State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_UTILITY__END_STATE = 2;

	/**
	 * The number of structural features of the '<em>State Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_UTILITY_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.AuthorisationUtilityImpl <em>Authorisation Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.AuthorisationUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getAuthorisationUtility()
	 * @generated
	 */
	int AUTHORISATION_UTILITY = 12;

	/**
	 * The feature id for the '<em><b>Authorisations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTHORISATION_UTILITY__AUTHORISATIONS = 0;

	/**
	 * The number of structural features of the '<em>Authorisation Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTHORISATION_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.RoleUtilityImpl <em>Role Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.RoleUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getRoleUtility()
	 * @generated
	 */
	int ROLE_UTILITY = 13;

	/**
	 * The feature id for the '<em><b>Roles</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_UTILITY__ROLES = 0;

	/**
	 * The number of structural features of the '<em>Role Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.UserUtilityImpl <em>User Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.UserUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getUserUtility()
	 * @generated
	 */
	int USER_UTILITY = 14;

	/**
	 * The feature id for the '<em><b>Users</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_UTILITY__USERS = 0;

	/**
	 * The number of structural features of the '<em>User Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.TransitionUtilityImpl <em>Transition Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.TransitionUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getTransitionUtility()
	 * @generated
	 */
	int TRANSITION_UTILITY = 15;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_UTILITY__TRANSITIONS = 0;

	/**
	 * The number of structural features of the '<em>Transition Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.CommandResponseBlockUtilityImpl <em>Command Response Block Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.CommandResponseBlockUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getCommandResponseBlockUtility()
	 * @generated
	 */
	int COMMAND_RESPONSE_BLOCK_UTILITY = 16;

	/**
	 * The feature id for the '<em><b>Command Response Blocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_RESPONSE_BLOCK_UTILITY__COMMAND_RESPONSE_BLOCKS = 0;

	/**
	 * The number of structural features of the '<em>Command Response Block Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_RESPONSE_BLOCK_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.ResponseBlockUtilityImpl <em>Response Block Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.ResponseBlockUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getResponseBlockUtility()
	 * @generated
	 */
	int RESPONSE_BLOCK_UTILITY = 17;

	/**
	 * The feature id for the '<em><b>Response Blocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_BLOCK_UTILITY__RESPONSE_BLOCKS = 0;

	/**
	 * The number of structural features of the '<em>Response Block Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_BLOCK_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.EventBlockUtilityImpl <em>Event Block Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.EventBlockUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getEventBlockUtility()
	 * @generated
	 */
	int EVENT_BLOCK_UTILITY = 18;

	/**
	 * The feature id for the '<em><b>Event Blocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_BLOCK_UTILITY__EVENT_BLOCKS = 0;

	/**
	 * The number of structural features of the '<em>Event Block Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_BLOCK_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.AlarmBlockUtilityImpl <em>Alarm Block Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.AlarmBlockUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getAlarmBlockUtility()
	 * @generated
	 */
	int ALARM_BLOCK_UTILITY = 19;

	/**
	 * The feature id for the '<em><b>Alarm Blocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_BLOCK_UTILITY__ALARM_BLOCKS = 0;

	/**
	 * The number of structural features of the '<em>Alarm Block Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_BLOCK_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.DataPointBlockUtilityImpl <em>Data Point Block Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.DataPointBlockUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getDataPointBlockUtility()
	 * @generated
	 */
	int DATA_POINT_BLOCK_UTILITY = 20;

	/**
	 * The feature id for the '<em><b>Data Point Blocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_BLOCK_UTILITY__DATA_POINT_BLOCKS = 0;

	/**
	 * The number of structural features of the '<em>Data Point Block Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_BLOCK_UTILITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.utility.impl.CommandDistributionUtilityImpl <em>Command Distribution Utility</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.utility.impl.CommandDistributionUtilityImpl
	 * @see mncModel.utility.impl.UtilityPackageImpl#getCommandDistributionUtility()
	 * @generated
	 */
	int COMMAND_DISTRIBUTION_UTILITY = 21;

	/**
	 * The feature id for the '<em><b>Command Distributions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_DISTRIBUTION_UTILITY__COMMAND_DISTRIBUTIONS = 0;

	/**
	 * The number of structural features of the '<em>Command Distribution Utility</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_DISTRIBUTION_UTILITY_FEATURE_COUNT = 1;


	/**
	 * Returns the meta object for class '{@link mncModel.utility.CommandValidationUtility <em>Command Validation Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Command Validation Utility</em>'.
	 * @see mncModel.utility.CommandValidationUtility
	 * @generated
	 */
	EClass getCommandValidationUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.CommandValidationUtility#getCommandValidations <em>Command Validations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Command Validations</em>'.
	 * @see mncModel.utility.CommandValidationUtility#getCommandValidations()
	 * @see #getCommandValidationUtility()
	 * @generated
	 */
	EReference getCommandValidationUtility_CommandValidations();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.AlarmHandlingUtility <em>Alarm Handling Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alarm Handling Utility</em>'.
	 * @see mncModel.utility.AlarmHandlingUtility
	 * @generated
	 */
	EClass getAlarmHandlingUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.AlarmHandlingUtility#getAlarmHandling <em>Alarm Handling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Alarm Handling</em>'.
	 * @see mncModel.utility.AlarmHandlingUtility#getAlarmHandling()
	 * @see #getAlarmHandlingUtility()
	 * @generated
	 */
	EReference getAlarmHandlingUtility_AlarmHandling();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.DataPointHandlingUtility <em>Data Point Handling Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Point Handling Utility</em>'.
	 * @see mncModel.utility.DataPointHandlingUtility
	 * @generated
	 */
	EClass getDataPointHandlingUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.DataPointHandlingUtility#getDataPointHandling <em>Data Point Handling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Point Handling</em>'.
	 * @see mncModel.utility.DataPointHandlingUtility#getDataPointHandling()
	 * @see #getDataPointHandlingUtility()
	 * @generated
	 */
	EReference getDataPointHandlingUtility_DataPointHandling();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.OperationUtility <em>Operation Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation Utility</em>'.
	 * @see mncModel.utility.OperationUtility
	 * @generated
	 */
	EClass getOperationUtility();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.utility.OperationUtility#getOperations <em>Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Operations</em>'.
	 * @see mncModel.utility.OperationUtility#getOperations()
	 * @see #getOperationUtility()
	 * @generated
	 */
	EReference getOperationUtility_Operations();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.CommandUtility <em>Command Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Command Utility</em>'.
	 * @see mncModel.utility.CommandUtility
	 * @generated
	 */
	EClass getCommandUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.CommandUtility#getCommands <em>Commands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Commands</em>'.
	 * @see mncModel.utility.CommandUtility#getCommands()
	 * @see #getCommandUtility()
	 * @generated
	 */
	EReference getCommandUtility_Commands();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.ResponseValidationUtility <em>Response Validation Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Response Validation Utility</em>'.
	 * @see mncModel.utility.ResponseValidationUtility
	 * @generated
	 */
	EClass getResponseValidationUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.ResponseValidationUtility#getResponseValidations <em>Response Validations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Response Validations</em>'.
	 * @see mncModel.utility.ResponseValidationUtility#getResponseValidations()
	 * @see #getResponseValidationUtility()
	 * @generated
	 */
	EReference getResponseValidationUtility_ResponseValidations();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.AlarmConditionUtility <em>Alarm Condition Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alarm Condition Utility</em>'.
	 * @see mncModel.utility.AlarmConditionUtility
	 * @generated
	 */
	EClass getAlarmConditionUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.AlarmConditionUtility#getAlarmConditions <em>Alarm Conditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Alarm Conditions</em>'.
	 * @see mncModel.utility.AlarmConditionUtility#getAlarmConditions()
	 * @see #getAlarmConditionUtility()
	 * @generated
	 */
	EReference getAlarmConditionUtility_AlarmConditions();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.DataPointUtility <em>Data Point Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Point Utility</em>'.
	 * @see mncModel.utility.DataPointUtility
	 * @generated
	 */
	EClass getDataPointUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.DataPointUtility#getDataPoints <em>Data Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Points</em>'.
	 * @see mncModel.utility.DataPointUtility#getDataPoints()
	 * @see #getDataPointUtility()
	 * @generated
	 */
	EReference getDataPointUtility_DataPoints();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.AlarmUtility <em>Alarm Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alarm Utility</em>'.
	 * @see mncModel.utility.AlarmUtility
	 * @generated
	 */
	EClass getAlarmUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.AlarmUtility#getAlarms <em>Alarms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Alarms</em>'.
	 * @see mncModel.utility.AlarmUtility#getAlarms()
	 * @see #getAlarmUtility()
	 * @generated
	 */
	EReference getAlarmUtility_Alarms();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.EventUtility <em>Event Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Utility</em>'.
	 * @see mncModel.utility.EventUtility
	 * @generated
	 */
	EClass getEventUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.EventUtility#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Events</em>'.
	 * @see mncModel.utility.EventUtility#getEvents()
	 * @see #getEventUtility()
	 * @generated
	 */
	EReference getEventUtility_Events();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.ResponseUtility <em>Response Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Response Utility</em>'.
	 * @see mncModel.utility.ResponseUtility
	 * @generated
	 */
	EClass getResponseUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.ResponseUtility#getResponses <em>Responses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Responses</em>'.
	 * @see mncModel.utility.ResponseUtility#getResponses()
	 * @see #getResponseUtility()
	 * @generated
	 */
	EReference getResponseUtility_Responses();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.StateUtility <em>State Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Utility</em>'.
	 * @see mncModel.utility.StateUtility
	 * @generated
	 */
	EClass getStateUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.StateUtility#getOperatingStates <em>Operating States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operating States</em>'.
	 * @see mncModel.utility.StateUtility#getOperatingStates()
	 * @see #getStateUtility()
	 * @generated
	 */
	EReference getStateUtility_OperatingStates();

	/**
	 * Returns the meta object for the reference '{@link mncModel.utility.StateUtility#getStartState <em>Start State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Start State</em>'.
	 * @see mncModel.utility.StateUtility#getStartState()
	 * @see #getStateUtility()
	 * @generated
	 */
	EReference getStateUtility_StartState();

	/**
	 * Returns the meta object for the reference '{@link mncModel.utility.StateUtility#getEndState <em>End State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>End State</em>'.
	 * @see mncModel.utility.StateUtility#getEndState()
	 * @see #getStateUtility()
	 * @generated
	 */
	EReference getStateUtility_EndState();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.AuthorisationUtility <em>Authorisation Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Authorisation Utility</em>'.
	 * @see mncModel.utility.AuthorisationUtility
	 * @generated
	 */
	EClass getAuthorisationUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.AuthorisationUtility#getAuthorisations <em>Authorisations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Authorisations</em>'.
	 * @see mncModel.utility.AuthorisationUtility#getAuthorisations()
	 * @see #getAuthorisationUtility()
	 * @generated
	 */
	EReference getAuthorisationUtility_Authorisations();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.RoleUtility <em>Role Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Utility</em>'.
	 * @see mncModel.utility.RoleUtility
	 * @generated
	 */
	EClass getRoleUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.RoleUtility#getRoles <em>Roles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Roles</em>'.
	 * @see mncModel.utility.RoleUtility#getRoles()
	 * @see #getRoleUtility()
	 * @generated
	 */
	EReference getRoleUtility_Roles();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.UserUtility <em>User Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Utility</em>'.
	 * @see mncModel.utility.UserUtility
	 * @generated
	 */
	EClass getUserUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.UserUtility#getUsers <em>Users</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Users</em>'.
	 * @see mncModel.utility.UserUtility#getUsers()
	 * @see #getUserUtility()
	 * @generated
	 */
	EReference getUserUtility_Users();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.TransitionUtility <em>Transition Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition Utility</em>'.
	 * @see mncModel.utility.TransitionUtility
	 * @generated
	 */
	EClass getTransitionUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.TransitionUtility#getTransitions <em>Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transitions</em>'.
	 * @see mncModel.utility.TransitionUtility#getTransitions()
	 * @see #getTransitionUtility()
	 * @generated
	 */
	EReference getTransitionUtility_Transitions();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.CommandResponseBlockUtility <em>Command Response Block Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Command Response Block Utility</em>'.
	 * @see mncModel.utility.CommandResponseBlockUtility
	 * @generated
	 */
	EClass getCommandResponseBlockUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.CommandResponseBlockUtility#getCommandResponseBlocks <em>Command Response Blocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Command Response Blocks</em>'.
	 * @see mncModel.utility.CommandResponseBlockUtility#getCommandResponseBlocks()
	 * @see #getCommandResponseBlockUtility()
	 * @generated
	 */
	EReference getCommandResponseBlockUtility_CommandResponseBlocks();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.ResponseBlockUtility <em>Response Block Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Response Block Utility</em>'.
	 * @see mncModel.utility.ResponseBlockUtility
	 * @generated
	 */
	EClass getResponseBlockUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.ResponseBlockUtility#getResponseBlocks <em>Response Blocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Response Blocks</em>'.
	 * @see mncModel.utility.ResponseBlockUtility#getResponseBlocks()
	 * @see #getResponseBlockUtility()
	 * @generated
	 */
	EReference getResponseBlockUtility_ResponseBlocks();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.EventBlockUtility <em>Event Block Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Block Utility</em>'.
	 * @see mncModel.utility.EventBlockUtility
	 * @generated
	 */
	EClass getEventBlockUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.EventBlockUtility#getEventBlocks <em>Event Blocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Event Blocks</em>'.
	 * @see mncModel.utility.EventBlockUtility#getEventBlocks()
	 * @see #getEventBlockUtility()
	 * @generated
	 */
	EReference getEventBlockUtility_EventBlocks();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.AlarmBlockUtility <em>Alarm Block Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alarm Block Utility</em>'.
	 * @see mncModel.utility.AlarmBlockUtility
	 * @generated
	 */
	EClass getAlarmBlockUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.AlarmBlockUtility#getAlarmBlocks <em>Alarm Blocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Alarm Blocks</em>'.
	 * @see mncModel.utility.AlarmBlockUtility#getAlarmBlocks()
	 * @see #getAlarmBlockUtility()
	 * @generated
	 */
	EReference getAlarmBlockUtility_AlarmBlocks();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.DataPointBlockUtility <em>Data Point Block Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Point Block Utility</em>'.
	 * @see mncModel.utility.DataPointBlockUtility
	 * @generated
	 */
	EClass getDataPointBlockUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.DataPointBlockUtility#getDataPointBlocks <em>Data Point Blocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Point Blocks</em>'.
	 * @see mncModel.utility.DataPointBlockUtility#getDataPointBlocks()
	 * @see #getDataPointBlockUtility()
	 * @generated
	 */
	EReference getDataPointBlockUtility_DataPointBlocks();

	/**
	 * Returns the meta object for class '{@link mncModel.utility.CommandDistributionUtility <em>Command Distribution Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Command Distribution Utility</em>'.
	 * @see mncModel.utility.CommandDistributionUtility
	 * @generated
	 */
	EClass getCommandDistributionUtility();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.utility.CommandDistributionUtility#getCommandDistributions <em>Command Distributions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Command Distributions</em>'.
	 * @see mncModel.utility.CommandDistributionUtility#getCommandDistributions()
	 * @see #getCommandDistributionUtility()
	 * @generated
	 */
	EReference getCommandDistributionUtility_CommandDistributions();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	UtilityFactory getUtilityFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.CommandValidationUtilityImpl <em>Command Validation Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.CommandValidationUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getCommandValidationUtility()
		 * @generated
		 */
		EClass COMMAND_VALIDATION_UTILITY = eINSTANCE.getCommandValidationUtility();

		/**
		 * The meta object literal for the '<em><b>Command Validations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_VALIDATION_UTILITY__COMMAND_VALIDATIONS = eINSTANCE.getCommandValidationUtility_CommandValidations();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.AlarmHandlingUtilityImpl <em>Alarm Handling Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.AlarmHandlingUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getAlarmHandlingUtility()
		 * @generated
		 */
		EClass ALARM_HANDLING_UTILITY = eINSTANCE.getAlarmHandlingUtility();

		/**
		 * The meta object literal for the '<em><b>Alarm Handling</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM_HANDLING_UTILITY__ALARM_HANDLING = eINSTANCE.getAlarmHandlingUtility_AlarmHandling();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.DataPointHandlingUtilityImpl <em>Data Point Handling Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.DataPointHandlingUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getDataPointHandlingUtility()
		 * @generated
		 */
		EClass DATA_POINT_HANDLING_UTILITY = eINSTANCE.getDataPointHandlingUtility();

		/**
		 * The meta object literal for the '<em><b>Data Point Handling</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_HANDLING_UTILITY__DATA_POINT_HANDLING = eINSTANCE.getDataPointHandlingUtility_DataPointHandling();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.OperationUtilityImpl <em>Operation Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.OperationUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getOperationUtility()
		 * @generated
		 */
		EClass OPERATION_UTILITY = eINSTANCE.getOperationUtility();

		/**
		 * The meta object literal for the '<em><b>Operations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_UTILITY__OPERATIONS = eINSTANCE.getOperationUtility_Operations();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.CommandUtilityImpl <em>Command Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.CommandUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getCommandUtility()
		 * @generated
		 */
		EClass COMMAND_UTILITY = eINSTANCE.getCommandUtility();

		/**
		 * The meta object literal for the '<em><b>Commands</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_UTILITY__COMMANDS = eINSTANCE.getCommandUtility_Commands();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.ResponseValidationUtilityImpl <em>Response Validation Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.ResponseValidationUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getResponseValidationUtility()
		 * @generated
		 */
		EClass RESPONSE_VALIDATION_UTILITY = eINSTANCE.getResponseValidationUtility();

		/**
		 * The meta object literal for the '<em><b>Response Validations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE_VALIDATION_UTILITY__RESPONSE_VALIDATIONS = eINSTANCE.getResponseValidationUtility_ResponseValidations();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.AlarmConditionUtilityImpl <em>Alarm Condition Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.AlarmConditionUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getAlarmConditionUtility()
		 * @generated
		 */
		EClass ALARM_CONDITION_UTILITY = eINSTANCE.getAlarmConditionUtility();

		/**
		 * The meta object literal for the '<em><b>Alarm Conditions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM_CONDITION_UTILITY__ALARM_CONDITIONS = eINSTANCE.getAlarmConditionUtility_AlarmConditions();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.DataPointUtilityImpl <em>Data Point Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.DataPointUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getDataPointUtility()
		 * @generated
		 */
		EClass DATA_POINT_UTILITY = eINSTANCE.getDataPointUtility();

		/**
		 * The meta object literal for the '<em><b>Data Points</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_UTILITY__DATA_POINTS = eINSTANCE.getDataPointUtility_DataPoints();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.AlarmUtilityImpl <em>Alarm Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.AlarmUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getAlarmUtility()
		 * @generated
		 */
		EClass ALARM_UTILITY = eINSTANCE.getAlarmUtility();

		/**
		 * The meta object literal for the '<em><b>Alarms</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM_UTILITY__ALARMS = eINSTANCE.getAlarmUtility_Alarms();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.EventUtilityImpl <em>Event Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.EventUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getEventUtility()
		 * @generated
		 */
		EClass EVENT_UTILITY = eINSTANCE.getEventUtility();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_UTILITY__EVENTS = eINSTANCE.getEventUtility_Events();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.ResponseUtilityImpl <em>Response Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.ResponseUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getResponseUtility()
		 * @generated
		 */
		EClass RESPONSE_UTILITY = eINSTANCE.getResponseUtility();

		/**
		 * The meta object literal for the '<em><b>Responses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE_UTILITY__RESPONSES = eINSTANCE.getResponseUtility_Responses();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.StateUtilityImpl <em>State Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.StateUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getStateUtility()
		 * @generated
		 */
		EClass STATE_UTILITY = eINSTANCE.getStateUtility();

		/**
		 * The meta object literal for the '<em><b>Operating States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_UTILITY__OPERATING_STATES = eINSTANCE.getStateUtility_OperatingStates();

		/**
		 * The meta object literal for the '<em><b>Start State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_UTILITY__START_STATE = eINSTANCE.getStateUtility_StartState();

		/**
		 * The meta object literal for the '<em><b>End State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_UTILITY__END_STATE = eINSTANCE.getStateUtility_EndState();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.AuthorisationUtilityImpl <em>Authorisation Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.AuthorisationUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getAuthorisationUtility()
		 * @generated
		 */
		EClass AUTHORISATION_UTILITY = eINSTANCE.getAuthorisationUtility();

		/**
		 * The meta object literal for the '<em><b>Authorisations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AUTHORISATION_UTILITY__AUTHORISATIONS = eINSTANCE.getAuthorisationUtility_Authorisations();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.RoleUtilityImpl <em>Role Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.RoleUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getRoleUtility()
		 * @generated
		 */
		EClass ROLE_UTILITY = eINSTANCE.getRoleUtility();

		/**
		 * The meta object literal for the '<em><b>Roles</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_UTILITY__ROLES = eINSTANCE.getRoleUtility_Roles();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.UserUtilityImpl <em>User Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.UserUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getUserUtility()
		 * @generated
		 */
		EClass USER_UTILITY = eINSTANCE.getUserUtility();

		/**
		 * The meta object literal for the '<em><b>Users</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER_UTILITY__USERS = eINSTANCE.getUserUtility_Users();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.TransitionUtilityImpl <em>Transition Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.TransitionUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getTransitionUtility()
		 * @generated
		 */
		EClass TRANSITION_UTILITY = eINSTANCE.getTransitionUtility();

		/**
		 * The meta object literal for the '<em><b>Transitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION_UTILITY__TRANSITIONS = eINSTANCE.getTransitionUtility_Transitions();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.CommandResponseBlockUtilityImpl <em>Command Response Block Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.CommandResponseBlockUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getCommandResponseBlockUtility()
		 * @generated
		 */
		EClass COMMAND_RESPONSE_BLOCK_UTILITY = eINSTANCE.getCommandResponseBlockUtility();

		/**
		 * The meta object literal for the '<em><b>Command Response Blocks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_RESPONSE_BLOCK_UTILITY__COMMAND_RESPONSE_BLOCKS = eINSTANCE.getCommandResponseBlockUtility_CommandResponseBlocks();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.ResponseBlockUtilityImpl <em>Response Block Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.ResponseBlockUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getResponseBlockUtility()
		 * @generated
		 */
		EClass RESPONSE_BLOCK_UTILITY = eINSTANCE.getResponseBlockUtility();

		/**
		 * The meta object literal for the '<em><b>Response Blocks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE_BLOCK_UTILITY__RESPONSE_BLOCKS = eINSTANCE.getResponseBlockUtility_ResponseBlocks();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.EventBlockUtilityImpl <em>Event Block Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.EventBlockUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getEventBlockUtility()
		 * @generated
		 */
		EClass EVENT_BLOCK_UTILITY = eINSTANCE.getEventBlockUtility();

		/**
		 * The meta object literal for the '<em><b>Event Blocks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_BLOCK_UTILITY__EVENT_BLOCKS = eINSTANCE.getEventBlockUtility_EventBlocks();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.AlarmBlockUtilityImpl <em>Alarm Block Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.AlarmBlockUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getAlarmBlockUtility()
		 * @generated
		 */
		EClass ALARM_BLOCK_UTILITY = eINSTANCE.getAlarmBlockUtility();

		/**
		 * The meta object literal for the '<em><b>Alarm Blocks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM_BLOCK_UTILITY__ALARM_BLOCKS = eINSTANCE.getAlarmBlockUtility_AlarmBlocks();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.DataPointBlockUtilityImpl <em>Data Point Block Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.DataPointBlockUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getDataPointBlockUtility()
		 * @generated
		 */
		EClass DATA_POINT_BLOCK_UTILITY = eINSTANCE.getDataPointBlockUtility();

		/**
		 * The meta object literal for the '<em><b>Data Point Blocks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_BLOCK_UTILITY__DATA_POINT_BLOCKS = eINSTANCE.getDataPointBlockUtility_DataPointBlocks();

		/**
		 * The meta object literal for the '{@link mncModel.utility.impl.CommandDistributionUtilityImpl <em>Command Distribution Utility</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.utility.impl.CommandDistributionUtilityImpl
		 * @see mncModel.utility.impl.UtilityPackageImpl#getCommandDistributionUtility()
		 * @generated
		 */
		EClass COMMAND_DISTRIBUTION_UTILITY = eINSTANCE.getCommandDistributionUtility();

		/**
		 * The meta object literal for the '<em><b>Command Distributions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_DISTRIBUTION_UTILITY__COMMAND_DISTRIBUTIONS = eINSTANCE.getCommandDistributionUtility_CommandDistributions();

	}

} //UtilityPackage
