/**
 */
package mncModel.utility;

import mncModel.AbstractOperationableItems;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.OperationUtility#getOperations <em>Operations</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getOperationUtility()
 * @model
 * @generated
 */
public interface OperationUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Operations</b></em>' reference list.
	 * The list contents are of type {@link mncModel.AbstractOperationableItems}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations</em>' reference list.
	 * @see mncModel.utility.UtilityPackage#getOperationUtility_Operations()
	 * @model
	 * @generated
	 */
	EList<AbstractOperationableItems> getOperations();

} // OperationUtility
