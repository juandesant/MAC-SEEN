/**
 */
package mncModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Translation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.ParameterTranslation#getInputParameters <em>Input Parameters</em>}</li>
 *   <li>{@link mncModel.ParameterTranslation#getTranslatedParameters <em>Translated Parameters</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getParameterTranslation()
 * @model
 * @generated
 */
public interface ParameterTranslation extends EObject {
	/**
	 * Returns the value of the '<em><b>Input Parameters</b></em>' reference list.
	 * The list contents are of type {@link mncModel.Parameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Parameters</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Parameters</em>' reference list.
	 * @see mncModel.MncModelPackage#getParameterTranslation_InputParameters()
	 * @model
	 * @generated
	 */
	EList<Parameter> getInputParameters();

	/**
	 * Returns the value of the '<em><b>Translated Parameters</b></em>' reference list.
	 * The list contents are of type {@link mncModel.Parameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Translated Parameters</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Translated Parameters</em>' reference list.
	 * @see mncModel.MncModelPackage#getParameterTranslation_TranslatedParameters()
	 * @model
	 * @generated
	 */
	EList<Parameter> getTranslatedParameters();

} // ParameterTranslation
