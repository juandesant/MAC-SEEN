/**
 */
package mncModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Point Valid Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.DataPointValidCondition#getDataPoint <em>Data Point</em>}</li>
 *   <li>{@link mncModel.DataPointValidCondition#getCheckValues <em>Check Values</em>}</li>
 *   <li>{@link mncModel.DataPointValidCondition#getCheckMaxValue <em>Check Max Value</em>}</li>
 *   <li>{@link mncModel.DataPointValidCondition#getCheckMinValue <em>Check Min Value</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getDataPointValidCondition()
 * @model
 * @generated
 */
public interface DataPointValidCondition extends EObject {
	/**
	 * Returns the value of the '<em><b>Data Point</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Point</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Point</em>' reference.
	 * @see #setDataPoint(DataPoint)
	 * @see mncModel.MncModelPackage#getDataPointValidCondition_DataPoint()
	 * @model
	 * @generated
	 */
	DataPoint getDataPoint();

	/**
	 * Sets the value of the '{@link mncModel.DataPointValidCondition#getDataPoint <em>Data Point</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Point</em>' reference.
	 * @see #getDataPoint()
	 * @generated
	 */
	void setDataPoint(DataPoint value);

	/**
	 * Returns the value of the '<em><b>Check Values</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.PrimitiveValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Check Values</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Check Values</em>' containment reference list.
	 * @see mncModel.MncModelPackage#getDataPointValidCondition_CheckValues()
	 * @model containment="true"
	 * @generated
	 */
	EList<PrimitiveValue> getCheckValues();

	/**
	 * Returns the value of the '<em><b>Check Max Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Check Max Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Check Max Value</em>' containment reference.
	 * @see #setCheckMaxValue(PrimitiveValue)
	 * @see mncModel.MncModelPackage#getDataPointValidCondition_CheckMaxValue()
	 * @model containment="true"
	 * @generated
	 */
	PrimitiveValue getCheckMaxValue();

	/**
	 * Sets the value of the '{@link mncModel.DataPointValidCondition#getCheckMaxValue <em>Check Max Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Check Max Value</em>' containment reference.
	 * @see #getCheckMaxValue()
	 * @generated
	 */
	void setCheckMaxValue(PrimitiveValue value);

	/**
	 * Returns the value of the '<em><b>Check Min Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Check Min Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Check Min Value</em>' containment reference.
	 * @see #setCheckMinValue(PrimitiveValue)
	 * @see mncModel.MncModelPackage#getDataPointValidCondition_CheckMinValue()
	 * @model containment="true"
	 * @generated
	 */
	PrimitiveValue getCheckMinValue();

	/**
	 * Sets the value of the '{@link mncModel.DataPointValidCondition#getCheckMinValue <em>Check Min Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Check Min Value</em>' containment reference.
	 * @see #getCheckMinValue()
	 * @generated
	 */
	void setCheckMinValue(PrimitiveValue value);

} // DataPointValidCondition
