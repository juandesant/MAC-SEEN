/**
 */
package mncModel.stakeholder;

import mncModel.Alarm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alarm Acknowledgement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.stakeholder.AlarmAcknowledgement#getAlarms <em>Alarms</em>}</li>
 * </ul>
 *
 * @see mncModel.stakeholder.StakeholderPackage#getAlarmAcknowledgement()
 * @model
 * @generated
 */
public interface AlarmAcknowledgement extends UserOperation {
	/**
	 * Returns the value of the '<em><b>Alarms</b></em>' reference list.
	 * The list contents are of type {@link mncModel.Alarm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarms</em>' reference list.
	 * @see mncModel.stakeholder.StakeholderPackage#getAlarmAcknowledgement_Alarms()
	 * @model
	 * @generated
	 */
	EList<Alarm> getAlarms();

} // AlarmAcknowledgement
