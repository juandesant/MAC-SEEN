/**
 */
package mncModel.stakeholder;

import mncModel.utility.RoleUtility;
import mncModel.utility.UserUtility;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stakeholder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.stakeholder.Stakeholder#getRoles <em>Roles</em>}</li>
 *   <li>{@link mncModel.stakeholder.Stakeholder#getUsers <em>Users</em>}</li>
 * </ul>
 *
 * @see mncModel.stakeholder.StakeholderPackage#getStakeholder()
 * @model
 * @generated
 */
public interface Stakeholder extends EObject {
	/**
	 * Returns the value of the '<em><b>Roles</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roles</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roles</em>' containment reference.
	 * @see #setRoles(RoleUtility)
	 * @see mncModel.stakeholder.StakeholderPackage#getStakeholder_Roles()
	 * @model containment="true"
	 * @generated
	 */
	RoleUtility getRoles();

	/**
	 * Sets the value of the '{@link mncModel.stakeholder.Stakeholder#getRoles <em>Roles</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Roles</em>' containment reference.
	 * @see #getRoles()
	 * @generated
	 */
	void setRoles(RoleUtility value);

	/**
	 * Returns the value of the '<em><b>Users</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Users</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Users</em>' containment reference.
	 * @see #setUsers(UserUtility)
	 * @see mncModel.stakeholder.StakeholderPackage#getStakeholder_Users()
	 * @model containment="true"
	 * @generated
	 */
	UserUtility getUsers();

	/**
	 * Sets the value of the '{@link mncModel.stakeholder.Stakeholder#getUsers <em>Users</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Users</em>' containment reference.
	 * @see #getUsers()
	 * @generated
	 */
	void setUsers(UserUtility value);

} // Stakeholder
