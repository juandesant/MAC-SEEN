/**
 */
package mncModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Response Distribution</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.ResponseDistribution#getResponse <em>Response</em>}</li>
 *   <li>{@link mncModel.ResponseDistribution#getDestinationNodes <em>Destination Nodes</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getResponseDistribution()
 * @model
 * @generated
 */
public interface ResponseDistribution extends EObject {
	/**
	 * Returns the value of the '<em><b>Response</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Response</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Response</em>' reference.
	 * @see #setResponse(Response)
	 * @see mncModel.MncModelPackage#getResponseDistribution_Response()
	 * @model
	 * @generated
	 */
	Response getResponse();

	/**
	 * Sets the value of the '{@link mncModel.ResponseDistribution#getResponse <em>Response</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Response</em>' reference.
	 * @see #getResponse()
	 * @generated
	 */
	void setResponse(Response value);

	/**
	 * Returns the value of the '<em><b>Destination Nodes</b></em>' reference list.
	 * The list contents are of type {@link mncModel.ControlNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Destination Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Destination Nodes</em>' reference list.
	 * @see mncModel.MncModelPackage#getResponseDistribution_DestinationNodes()
	 * @model
	 * @generated
	 */
	EList<ControlNode> getDestinationNodes();

} // ResponseDistribution
