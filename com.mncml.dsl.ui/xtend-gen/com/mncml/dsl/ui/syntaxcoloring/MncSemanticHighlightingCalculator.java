package com.mncml.dsl.ui.syntaxcoloring;

import com.google.common.base.Objects;
import com.google.common.collect.Iterators;
import com.mncml.dsl.ui.syntaxcoloring.MncHighlightingConfiguration;
import java.util.Iterator;
import java.util.List;
import mncModel.CommandResponseBlock;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.CrossReference;
import org.eclipse.xtext.nodemodel.BidiTreeIterable;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.parser.IParseResult;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultSemanticHighlightingCalculator;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightedPositionAcceptor;
import org.eclipse.xtext.ui.editor.syntaxcoloring.ISemanticHighlightingCalculator;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;

@SuppressWarnings("all")
public class MncSemanticHighlightingCalculator extends DefaultSemanticHighlightingCalculator implements ISemanticHighlightingCalculator {
  @Override
  public void provideHighlightingFor(final XtextResource resource, final IHighlightedPositionAcceptor acceptor) {
    boolean _or = false;
    boolean _equals = Objects.equal(resource, null);
    if (_equals) {
      _or = true;
    } else {
      IParseResult _parseResult = resource.getParseResult();
      boolean _equals_1 = Objects.equal(_parseResult, null);
      _or = _equals_1;
    }
    if (_or) {
      return;
    }
    TreeIterator<EObject> _allContents = resource.getAllContents();
    Iterator<CommandResponseBlock> _filter = Iterators.<CommandResponseBlock>filter(_allContents, CommandResponseBlock.class);
    List<CommandResponseBlock> listOfCommandResponseBlocks = IteratorExtensions.<CommandResponseBlock>toList(_filter);
    for (final CommandResponseBlock commandResponseBlock : listOfCommandResponseBlocks) {
      {
        ICompositeNode compositeNode = NodeModelUtils.getNode(commandResponseBlock);
        BidiTreeIterable<INode> _asTreeIterable = compositeNode.getAsTreeIterable();
        for (final INode node : _asTreeIterable) {
          boolean _and = false;
          EObject _grammarElement = node.getGrammarElement();
          if (!(_grammarElement instanceof CrossReference)) {
            _and = false;
          } else {
            EObject _semanticElement = node.getSemanticElement();
            _and = (_semanticElement instanceof CommandResponseBlock);
          }
          if (_and) {
            this.highlightNode(acceptor, node, MncHighlightingConfiguration.CRB_COMMAND);
          }
        }
      }
    }
  }
}
