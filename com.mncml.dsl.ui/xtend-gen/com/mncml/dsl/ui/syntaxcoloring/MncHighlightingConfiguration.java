package com.mncml.dsl.ui.syntaxcoloring;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultHighlightingConfiguration;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfiguration;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfigurationAcceptor;
import org.eclipse.xtext.ui.editor.utils.TextStyle;
import org.eclipse.xtext.xbase.lib.InputOutput;

@SuppressWarnings("all")
public class MncHighlightingConfiguration extends DefaultHighlightingConfiguration implements IHighlightingConfiguration {
  public final static String CRB_COMMAND = "CRB_command";
  
  @Override
  public void configure(final IHighlightingConfigurationAcceptor acceptor) {
    InputOutput.<String>println("MncHC");
    super.configure(acceptor);
    TextStyle _CRB_CommandTextStyle = this.CRB_CommandTextStyle();
    acceptor.acceptDefaultHighlighting(MncHighlightingConfiguration.CRB_COMMAND, "CRB_command", _CRB_CommandTextStyle);
  }
  
  public TextStyle CRB_CommandTextStyle() {
    TextStyle _xblockexpression = null;
    {
      TextStyle textStyle = new TextStyle();
      RGB _rGB = new RGB(255, 00, 00);
      textStyle.setColor(_rGB);
      textStyle.setStyle(SWT.ITALIC);
      _xblockexpression = textStyle;
    }
    return _xblockexpression;
  }
}
