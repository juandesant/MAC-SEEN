package com.mncml.dsl.ui.labeling;

import com.google.inject.Inject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.xtext.ui.label.DefaultEObjectLabelProvider;

@SuppressWarnings("all")
public class MncLabelProvider extends DefaultEObjectLabelProvider {
  @Inject
  public MncLabelProvider(final AdapterFactoryLabelProvider delegate) {
    super(delegate);
  }
}
