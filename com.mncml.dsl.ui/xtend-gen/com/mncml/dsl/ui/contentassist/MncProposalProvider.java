package com.mncml.dsl.ui.contentassist;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.inject.Inject;
import com.mncml.dsl.ui.contentassist.AbstractMncProposalProvider;
import java.util.List;
import java.util.function.Consumer;
import mncModel.AbstractInterfaceItems;
import mncModel.Action;
import mncModel.Alarm;
import mncModel.AlarmBlock;
import mncModel.AlarmTriggerCondition;
import mncModel.CheckParameterCondition;
import mncModel.Command;
import mncModel.CommandResponseBlock;
import mncModel.CommandTriggerCondition;
import mncModel.CommandValidation;
import mncModel.ControlNode;
import mncModel.DataPoint;
import mncModel.DataPointBlock;
import mncModel.DataPointTriggerCondition;
import mncModel.Event;
import mncModel.EventBlock;
import mncModel.EventTriggerCondition;
import mncModel.InterfaceDescription;
import mncModel.MncModelFactory;
import mncModel.OperatingState;
import mncModel.Operation;
import mncModel.Parameter;
import mncModel.PrimitiveValueType;
import mncModel.Response;
import mncModel.ResponseBlock;
import mncModel.ResponseTranslationRule;
import mncModel.ResponseValidation;
import mncModel.ResponsibleItemList;
import mncModel.SimpleType;
import mncModel.utility.AlarmBlockUtility;
import mncModel.utility.AlarmUtility;
import mncModel.utility.CommandResponseBlockUtility;
import mncModel.utility.CommandUtility;
import mncModel.utility.DataPointBlockUtility;
import mncModel.utility.DataPointUtility;
import mncModel.utility.EventBlockUtility;
import mncModel.utility.EventUtility;
import mncModel.utility.ResponseUtility;
import mncModel.utility.StateUtility;
import org.eclipse.emf.common.ui.dialogs.ResourceDialog;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

@SuppressWarnings("all")
public class MncProposalProvider extends AbstractMncProposalProvider {
  @Inject
  private IQualifiedNameProvider qualifiedNameProvider;
  
  public final static String INT = "int";
  
  public final static String STRING = "string";
  
  public final static String BOOLEAN = "boolean";
  
  public final static String FLOAT = "float";
  
  @Override
  public void completeModel_Name(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    String string = "Enter Model Name";
    StyledString styledString = new StyledString();
    StyledString.Styler styler = StyledString.QUALIFIER_STYLER;
    styledString.append(string, styler);
    ICompletionProposal completionProposal = this.createCompletionProposal("", styledString, null, context);
    acceptor.accept(completionProposal);
  }
  
  @Override
  public void completeCommand_Name(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    String string = "Enter Command Name";
    StyledString styledString = new StyledString();
    StyledString.Styler styler = StyledString.QUALIFIER_STYLER;
    styledString.append(string, styler);
    ICompletionProposal completionProposal = this.createCompletionProposal("", styledString, null, context);
    acceptor.accept(completionProposal);
  }
  
  @Override
  public void completeAlarm_Name(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    String string = "Enter Alarm Name";
    StyledString styledString = new StyledString();
    StyledString.Styler styler = StyledString.QUALIFIER_STYLER;
    styledString.append(string, styler);
    ICompletionProposal completionProposal = this.createCompletionProposal("", styledString, null, context);
    acceptor.accept(completionProposal);
  }
  
  @Override
  public void completeEvent_Name(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    String string = "Enter Event Name";
    StyledString styledString = new StyledString();
    StyledString.Styler styler = StyledString.QUALIFIER_STYLER;
    styledString.append(string, styler);
    ICompletionProposal completionProposal = this.createCompletionProposal("", styledString, null, context);
    acceptor.accept(completionProposal);
  }
  
  @Override
  public void completeDataPoint_Name(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    String string = "Enter DataPoint Name";
    StyledString styledString = new StyledString();
    StyledString.Styler styler = StyledString.QUALIFIER_STYLER;
    styledString.append(string, styler);
    ICompletionProposal completionProposal = this.createCompletionProposal("", styledString, null, context);
    acceptor.accept(completionProposal);
  }
  
  @Override
  public void completeInterfaceDescription_Name(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    String string = "Enter InterfaceDescription Name";
    StyledString styledString = new StyledString();
    StyledString.Styler styler = StyledString.QUALIFIER_STYLER;
    styledString.append(string, styler);
    ICompletionProposal completionProposal = this.createCompletionProposal("", styledString, null, context);
    acceptor.accept(completionProposal);
  }
  
  @Override
  public void completeSimpleType_Name(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    String string = "Enter Parameter Name";
    StyledString styledString = new StyledString();
    StyledString.Styler styler = StyledString.QUALIFIER_STYLER;
    styledString.append(string, styler);
    ICompletionProposal completionProposal = this.createCompletionProposal("", styledString, null, context);
    acceptor.accept(completionProposal);
  }
  
  @Override
  public void completeStateUtility_OperatingStates(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    String string = "Enter State Name";
    StyledString styledString = new StyledString();
    StyledString.Styler styler = StyledString.QUALIFIER_STYLER;
    styledString.append(string, styler);
    ICompletionProposal completionProposal = this.createCompletionProposal("", styledString, null, context);
    acceptor.accept(completionProposal);
  }
  
  @Override
  public void completeResponse_Name(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    String string = "Enter Response Name";
    StyledString styledString = new StyledString();
    StyledString.Styler styler = StyledString.QUALIFIER_STYLER;
    styledString.append(string, styler);
    ICompletionProposal completionProposal = this.createCompletionProposal("", styledString, null, context);
    acceptor.accept(completionProposal);
  }
  
  @Override
  public void completeControlNode_Name(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    String string = "Enter Control Node Name";
    StyledString styledString = new StyledString();
    StyledString.Styler styler = StyledString.QUALIFIER_STYLER;
    styledString.append(string, styler);
    ICompletionProposal completionProposal = this.createCompletionProposal("", styledString, null, context);
    acceptor.accept(completionProposal);
  }
  
  @Override
  public void completeSimpleType_Value(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    if ((!(model instanceof SimpleType))) {
      return;
    }
    final SimpleType parameter = ((SimpleType) model);
    PrimitiveValueType _type = parameter.getType();
    String type = _type.getName();
    switch (type) {
      case MncProposalProvider.INT:
        ICompletionProposal _createCompletionProposal = this.createCompletionProposal("", "Enter Integer Value", null, context);
        acceptor.accept(_createCompletionProposal);
        break;
      case MncProposalProvider.BOOLEAN:
        ICompletionProposal _createCompletionProposal_1 = this.createCompletionProposal("", "Enter \'true\' or \'false\'", null, context);
        acceptor.accept(_createCompletionProposal_1);
        break;
      case MncProposalProvider.FLOAT:
        ICompletionProposal _createCompletionProposal_2 = this.createCompletionProposal("", "Enter Float Value", null, context);
        acceptor.accept(_createCompletionProposal_2);
        break;
      case MncProposalProvider.STRING:
        ICompletionProposal _createCompletionProposal_3 = this.createCompletionProposal("", "Enter String", null, context);
        acceptor.accept(_createCompletionProposal_3);
        break;
    }
  }
  
  @Override
  public void completeDataPoint_Value(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    if ((!(model instanceof DataPoint))) {
      return;
    }
    final DataPoint dataPoint = ((DataPoint) model);
    PrimitiveValueType _type = dataPoint.getType();
    String type = _type.getName();
    switch (type) {
      case MncProposalProvider.INT:
        ICompletionProposal _createCompletionProposal = this.createCompletionProposal("", "Enter Integer Value", null, context);
        acceptor.accept(_createCompletionProposal);
        break;
      case MncProposalProvider.BOOLEAN:
        ICompletionProposal _createCompletionProposal_1 = this.createCompletionProposal("", "Enter \'true\' or \'false\'", null, context);
        acceptor.accept(_createCompletionProposal_1);
        break;
      case MncProposalProvider.FLOAT:
        ICompletionProposal _createCompletionProposal_2 = this.createCompletionProposal("", "Enter Float Value", null, context);
        acceptor.accept(_createCompletionProposal_2);
        break;
      case MncProposalProvider.STRING:
        ICompletionProposal _createCompletionProposal_3 = this.createCompletionProposal("", "Enter String", null, context);
        acceptor.accept(_createCompletionProposal_3);
        break;
    }
  }
  
  @Override
  public void completeKeyword(final Keyword keyword, final ContentAssistContext contentAssistContext, final ICompletionProposalAcceptor acceptor) {
    super.completeKeyword(keyword, contentAssistContext, acceptor);
  }
  
  public List<OperatingState> getCandidateOperatingStates(final InterfaceDescription interfaceDescription) {
    List<OperatingState> _xblockexpression = null;
    {
      List<OperatingState> listOfOperatingStates = CollectionLiterals.<OperatingState>newArrayList();
      StateUtility _operatingStates = interfaceDescription.getOperatingStates();
      boolean _notEquals = (!Objects.equal(_operatingStates, null));
      if (_notEquals) {
        StateUtility _operatingStates_1 = interfaceDescription.getOperatingStates();
        EList<OperatingState> _operatingStates_2 = _operatingStates_1.getOperatingStates();
        listOfOperatingStates.addAll(_operatingStates_2);
      }
      EList<InterfaceDescription> _uses = interfaceDescription.getUses();
      for (final InterfaceDescription interfaceDescriptionInUses : _uses) {
        StateUtility _operatingStates_3 = interfaceDescriptionInUses.getOperatingStates();
        EList<OperatingState> _operatingStates_4 = _operatingStates_3.getOperatingStates();
        listOfOperatingStates.addAll(_operatingStates_4);
      }
      _xblockexpression = listOfOperatingStates;
    }
    return _xblockexpression;
  }
  
  public List<Alarm> getCandidateAlarms(final InterfaceDescription interfaceDescription) {
    List<Alarm> _xblockexpression = null;
    {
      List<Alarm> listOfAlarms = CollectionLiterals.<Alarm>newArrayList();
      AlarmUtility _alarms = interfaceDescription.getAlarms();
      boolean _notEquals = (!Objects.equal(_alarms, null));
      if (_notEquals) {
        AlarmUtility _alarms_1 = interfaceDescription.getAlarms();
        EList<Alarm> _alarms_2 = _alarms_1.getAlarms();
        listOfAlarms.addAll(_alarms_2);
      }
      EList<InterfaceDescription> _uses = interfaceDescription.getUses();
      for (final InterfaceDescription interfaceDescriptionInUses : _uses) {
        AlarmUtility _alarms_3 = interfaceDescriptionInUses.getAlarms();
        boolean _notEquals_1 = (!Objects.equal(_alarms_3, null));
        if (_notEquals_1) {
          AlarmUtility _alarms_4 = interfaceDescriptionInUses.getAlarms();
          EList<Alarm> _alarms_5 = _alarms_4.getAlarms();
          listOfAlarms.addAll(_alarms_5);
        }
      }
      _xblockexpression = listOfAlarms;
    }
    return _xblockexpression;
  }
  
  @Override
  public void completeCommandResponseBlock_Command(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    ControlNode controlNode = EcoreUtil2.<ControlNode>getContainerOfType(model, ControlNode.class);
    InterfaceDescription interfaceDescription = controlNode.getInterfaceDescription();
    List<Command> listOfCommandsInCommandBlock = CollectionLiterals.<Command>newArrayList();
    CommandResponseBlockUtility _commandResponseBlocks = controlNode.getCommandResponseBlocks();
    EList<CommandResponseBlock> _commandResponseBlocks_1 = _commandResponseBlocks.getCommandResponseBlocks();
    for (final CommandResponseBlock commRespBlock : _commandResponseBlocks_1) {
      Command _command = commRespBlock.getCommand();
      listOfCommandsInCommandBlock.add(_command);
    }
    CommandUtility _commands = interfaceDescription.getCommands();
    boolean _notEquals = (!Objects.equal(_commands, null));
    if (_notEquals) {
      CommandUtility _commands_1 = interfaceDescription.getCommands();
      EList<Command> _commands_2 = _commands_1.getCommands();
      for (final Command command : _commands_2) {
        boolean _contains = listOfCommandsInCommandBlock.contains(command);
        boolean _not = (!_contains);
        if (_not) {
          QualifiedName _apply = new Function<Command, QualifiedName>() {
            @Override
            public QualifiedName apply(final Command input) {
              return MncProposalProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
            }
            
            @Override
            public boolean equals(final Object object) {
              throw new UnsupportedOperationException("TODO: auto-generated method stub");
            }
          }.apply(command);
          String proposal = _apply.toString();
          ICompletionProposal completionProposal = this.createCompletionProposal(proposal, context);
          acceptor.accept(completionProposal);
        }
      }
      EList<InterfaceDescription> _uses = interfaceDescription.getUses();
      for (final InterfaceDescription interfaceDescriptionInUses : _uses) {
        CommandUtility _commands_3 = interfaceDescriptionInUses.getCommands();
        EList<Command> _commands_4 = _commands_3.getCommands();
        for (final Command command_1 : _commands_4) {
          boolean _contains_1 = listOfCommandsInCommandBlock.contains(interfaceDescriptionInUses);
          boolean _not_1 = (!_contains_1);
          if (_not_1) {
            QualifiedName _apply_1 = new Function<Command, QualifiedName>() {
              @Override
              public QualifiedName apply(final Command input) {
                return MncProposalProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
              }
              
              @Override
              public boolean equals(final Object object) {
                throw new UnsupportedOperationException("TODO: auto-generated method stub");
              }
            }.apply(command_1);
            String proposal_1 = _apply_1.toString();
            ICompletionProposal completionProposal_1 = this.createCompletionProposal(proposal_1, context);
            acceptor.accept(completionProposal_1);
          }
        }
      }
    }
  }
  
  @Override
  public void completeAlarmBlock_Alarm(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    ControlNode controlNode = EcoreUtil2.<ControlNode>getContainerOfType(model, ControlNode.class);
    InterfaceDescription interfaceDescription = controlNode.getInterfaceDescription();
    List<Alarm> listOfAlarmsInAlarmBlock = CollectionLiterals.<Alarm>newArrayList();
    AlarmBlockUtility _alarmBlocks = controlNode.getAlarmBlocks();
    EList<AlarmBlock> _alarmBlocks_1 = _alarmBlocks.getAlarmBlocks();
    for (final AlarmBlock alarmBlock : _alarmBlocks_1) {
      Alarm _alarm = alarmBlock.getAlarm();
      listOfAlarmsInAlarmBlock.add(_alarm);
    }
    AlarmUtility _alarms = interfaceDescription.getAlarms();
    boolean _notEquals = (!Objects.equal(_alarms, null));
    if (_notEquals) {
      AlarmUtility _alarms_1 = interfaceDescription.getAlarms();
      EList<Alarm> _alarms_2 = _alarms_1.getAlarms();
      for (final Alarm alarm : _alarms_2) {
        boolean _contains = listOfAlarmsInAlarmBlock.contains(alarm);
        boolean _not = (!_contains);
        if (_not) {
          QualifiedName _apply = new Function<Alarm, QualifiedName>() {
            @Override
            public QualifiedName apply(final Alarm input) {
              return MncProposalProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
            }
            
            @Override
            public boolean equals(final Object object) {
              throw new UnsupportedOperationException("TODO: auto-generated method stub");
            }
          }.apply(alarm);
          String proposal = _apply.toString();
          ICompletionProposal completionProposal = this.createCompletionProposal(proposal, context);
          acceptor.accept(completionProposal);
        }
      }
      EList<InterfaceDescription> _uses = interfaceDescription.getUses();
      for (final InterfaceDescription interfaceDescriptionInUses : _uses) {
        AlarmUtility _alarms_3 = interfaceDescriptionInUses.getAlarms();
        EList<Alarm> _alarms_4 = _alarms_3.getAlarms();
        for (final Alarm alarm_1 : _alarms_4) {
          boolean _contains_1 = listOfAlarmsInAlarmBlock.contains(interfaceDescriptionInUses);
          boolean _not_1 = (!_contains_1);
          if (_not_1) {
            QualifiedName _apply_1 = new Function<Alarm, QualifiedName>() {
              @Override
              public QualifiedName apply(final Alarm input) {
                return MncProposalProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
              }
              
              @Override
              public boolean equals(final Object object) {
                throw new UnsupportedOperationException("TODO: auto-generated method stub");
              }
            }.apply(alarm_1);
            String proposal_1 = _apply_1.toString();
            ICompletionProposal completionProposal_1 = this.createCompletionProposal(proposal_1, context);
            acceptor.accept(completionProposal_1);
          }
        }
      }
    }
  }
  
  @Override
  public void completeDataPointBlock_DataPoint(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    ControlNode controlNode = EcoreUtil2.<ControlNode>getContainerOfType(model, ControlNode.class);
    InterfaceDescription interfaceDescription = controlNode.getInterfaceDescription();
    List<DataPoint> listOfDatapointsInDatapointBlock = CollectionLiterals.<DataPoint>newArrayList();
    DataPointBlockUtility _dataPointBlocks = controlNode.getDataPointBlocks();
    EList<DataPointBlock> _dataPointBlocks_1 = _dataPointBlocks.getDataPointBlocks();
    for (final DataPointBlock dataPointBlock : _dataPointBlocks_1) {
      DataPoint _dataPoint = dataPointBlock.getDataPoint();
      listOfDatapointsInDatapointBlock.add(_dataPoint);
    }
    DataPointUtility _dataPoints = interfaceDescription.getDataPoints();
    boolean _notEquals = (!Objects.equal(_dataPoints, null));
    if (_notEquals) {
      DataPointUtility _dataPoints_1 = interfaceDescription.getDataPoints();
      EList<DataPoint> _dataPoints_2 = _dataPoints_1.getDataPoints();
      for (final DataPoint dataPoint : _dataPoints_2) {
        boolean _contains = listOfDatapointsInDatapointBlock.contains(dataPoint);
        boolean _not = (!_contains);
        if (_not) {
          QualifiedName _apply = new Function<DataPoint, QualifiedName>() {
            @Override
            public QualifiedName apply(final DataPoint input) {
              return MncProposalProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
            }
            
            @Override
            public boolean equals(final Object object) {
              throw new UnsupportedOperationException("TODO: auto-generated method stub");
            }
          }.apply(dataPoint);
          String proposal = _apply.toString();
          ICompletionProposal completionProposal = this.createCompletionProposal(proposal, context);
          acceptor.accept(completionProposal);
        }
      }
      EList<InterfaceDescription> _uses = interfaceDescription.getUses();
      for (final InterfaceDescription interfaceDescriptionInUses : _uses) {
        DataPointUtility _dataPoints_3 = interfaceDescriptionInUses.getDataPoints();
        EList<DataPoint> _dataPoints_4 = _dataPoints_3.getDataPoints();
        for (final DataPoint dataPoint_1 : _dataPoints_4) {
          boolean _contains_1 = listOfDatapointsInDatapointBlock.contains(interfaceDescriptionInUses);
          boolean _not_1 = (!_contains_1);
          if (_not_1) {
            QualifiedName _apply_1 = new Function<DataPoint, QualifiedName>() {
              @Override
              public QualifiedName apply(final DataPoint input) {
                return MncProposalProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
              }
              
              @Override
              public boolean equals(final Object object) {
                throw new UnsupportedOperationException("TODO: auto-generated method stub");
              }
            }.apply(dataPoint_1);
            String proposal_1 = _apply_1.toString();
            ICompletionProposal completionProposal_1 = this.createCompletionProposal(proposal_1, context);
            acceptor.accept(completionProposal_1);
          }
        }
      }
    }
  }
  
  @Override
  public void completeEventBlock_Event(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    ControlNode controlNode = EcoreUtil2.<ControlNode>getContainerOfType(model, ControlNode.class);
    InterfaceDescription interfaceDescription = controlNode.getInterfaceDescription();
    List<Event> listOfEventsInEventBlock = CollectionLiterals.<Event>newArrayList();
    EventBlockUtility _eventBlocks = controlNode.getEventBlocks();
    EList<EventBlock> _eventBlocks_1 = _eventBlocks.getEventBlocks();
    for (final EventBlock eventBlock : _eventBlocks_1) {
      Event _event = eventBlock.getEvent();
      listOfEventsInEventBlock.add(_event);
    }
    EventUtility _events = interfaceDescription.getEvents();
    boolean _notEquals = (!Objects.equal(_events, null));
    if (_notEquals) {
      EventUtility _events_1 = interfaceDescription.getEvents();
      EList<Event> _events_2 = _events_1.getEvents();
      for (final Event event : _events_2) {
        boolean _contains = listOfEventsInEventBlock.contains(event);
        boolean _not = (!_contains);
        if (_not) {
          QualifiedName _apply = new Function<Event, QualifiedName>() {
            @Override
            public QualifiedName apply(final Event input) {
              return MncProposalProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
            }
            
            @Override
            public boolean equals(final Object object) {
              throw new UnsupportedOperationException("TODO: auto-generated method stub");
            }
          }.apply(event);
          String proposal = _apply.toString();
          ICompletionProposal completionProposal = this.createCompletionProposal(proposal, context);
          acceptor.accept(completionProposal);
        }
      }
      EList<InterfaceDescription> _uses = interfaceDescription.getUses();
      for (final InterfaceDescription interfaceDescriptionInUses : _uses) {
        EventUtility _events_3 = interfaceDescriptionInUses.getEvents();
        EList<Event> _events_4 = _events_3.getEvents();
        for (final Event event_1 : _events_4) {
          boolean _contains_1 = listOfEventsInEventBlock.contains(interfaceDescriptionInUses);
          boolean _not_1 = (!_contains_1);
          if (_not_1) {
            QualifiedName _apply_1 = new Function<Event, QualifiedName>() {
              @Override
              public QualifiedName apply(final Event input) {
                return MncProposalProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
              }
              
              @Override
              public boolean equals(final Object object) {
                throw new UnsupportedOperationException("TODO: auto-generated method stub");
              }
            }.apply(event_1);
            String proposal_1 = _apply_1.toString();
            ICompletionProposal completionProposal_1 = this.createCompletionProposal(proposal_1, context);
            acceptor.accept(completionProposal_1);
          }
        }
      }
    }
  }
  
  @Override
  public void completeAction_Alarm(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    Action action = ((Action) model);
    AlarmBlock alarmBlock = EcoreUtil2.<AlarmBlock>getContainerOfType(action, AlarmBlock.class);
    ControlNode _containerOfType = EcoreUtil2.<ControlNode>getContainerOfType(action, ControlNode.class);
    InterfaceDescription interfaceDescription = _containerOfType.getInterfaceDescription();
    EList<Alarm> actionAlarm = action.getAlarm();
    List<Alarm> listOfAlarms = CollectionLiterals.<Alarm>newArrayList();
    AlarmUtility _alarms = interfaceDescription.getAlarms();
    EList<Alarm> _alarms_1 = _alarms.getAlarms();
    listOfAlarms.addAll(_alarms_1);
    listOfAlarms.removeAll(actionAlarm);
    boolean _notEquals = (!Objects.equal(alarmBlock, null));
    if (_notEquals) {
      Alarm _alarm = alarmBlock.getAlarm();
      listOfAlarms.remove(_alarm);
    }
    for (final Alarm alarm : listOfAlarms) {
      {
        QualifiedName _apply = new Function<Alarm, QualifiedName>() {
          @Override
          public QualifiedName apply(final Alarm input) {
            return MncProposalProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
          }
          
          @Override
          public boolean equals(final Object object) {
            throw new UnsupportedOperationException("TODO: auto-generated method stub");
          }
        }.apply(alarm);
        String proposal = _apply.toString();
        ICompletionProposal completionProposal = this.createCompletionProposal(proposal, context);
        acceptor.accept(completionProposal);
      }
    }
  }
  
  @Override
  public void completeAction_Command(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    Action action = ((Action) model);
    CommandResponseBlock ifCommRespBlock = EcoreUtil2.<CommandResponseBlock>getContainerOfType(action, CommandResponseBlock.class);
    ControlNode controlNode = EcoreUtil2.<ControlNode>getContainerOfType(action, ControlNode.class);
    InterfaceDescription interfaceDescription = controlNode.getInterfaceDescription();
    EList<Command> actionCommand = action.getCommand();
    List<Command> listOfCommands = CollectionLiterals.<Command>newArrayList();
    CommandUtility _commands = interfaceDescription.getCommands();
    EList<Command> _commands_1 = _commands.getCommands();
    listOfCommands.addAll(_commands_1);
    EList<ControlNode> _childNodes = controlNode.getChildNodes();
    for (final ControlNode childNode : _childNodes) {
      InterfaceDescription _interfaceDescription = childNode.getInterfaceDescription();
      CommandUtility _commands_2 = _interfaceDescription.getCommands();
      EList<Command> _commands_3 = _commands_2.getCommands();
      listOfCommands.addAll(_commands_3);
    }
    listOfCommands.removeAll(actionCommand);
    boolean _notEquals = (!Objects.equal(ifCommRespBlock, null));
    if (_notEquals) {
      Command _command = ifCommRespBlock.getCommand();
      listOfCommands.remove(_command);
    }
    for (final Command command : listOfCommands) {
      {
        QualifiedName _apply = new Function<Command, QualifiedName>() {
          @Override
          public QualifiedName apply(final Command input) {
            return MncProposalProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
          }
          
          @Override
          public boolean equals(final Object object) {
            return false;
          }
        }.apply(command);
        String proposal = _apply.toString();
        ICompletionProposal completionProposal = this.createCompletionProposal(proposal, context);
        acceptor.accept(completionProposal);
      }
    }
  }
  
  @Override
  public void completeAction_Event(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    Action action = ((Action) model);
    EventBlock eventBlock = EcoreUtil2.<EventBlock>getContainerOfType(action, EventBlock.class);
    ControlNode _containerOfType = EcoreUtil2.<ControlNode>getContainerOfType(action, ControlNode.class);
    InterfaceDescription interfaceDescription = _containerOfType.getInterfaceDescription();
    EList<Event> actionEvent = action.getEvent();
    List<Event> listOfEvents = CollectionLiterals.<Event>newArrayList();
    EventUtility _events = interfaceDescription.getEvents();
    EList<Event> _events_1 = _events.getEvents();
    listOfEvents.addAll(_events_1);
    listOfEvents.removeAll(actionEvent);
    boolean _notEquals = (!Objects.equal(eventBlock, null));
    if (_notEquals) {
      Event _event = eventBlock.getEvent();
      listOfEvents.remove(_event);
    }
    for (final Event event : listOfEvents) {
      {
        QualifiedName _apply = new Function<Event, QualifiedName>() {
          @Override
          public QualifiedName apply(final Event input) {
            return MncProposalProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
          }
          
          @Override
          public boolean equals(final Object object) {
            throw new UnsupportedOperationException("TODO: auto-generated method stub");
          }
        }.apply(event);
        String proposal = _apply.toString();
        ICompletionProposal completionProposal = this.createCompletionProposal(proposal, context);
        acceptor.accept(completionProposal);
      }
    }
  }
  
  @Override
  public void completeStateUtility_EndState(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    if ((!(model instanceof StateUtility))) {
      return;
    }
    StateUtility stateUtility = ((StateUtility) model);
    InterfaceDescription _containerOfType = EcoreUtil2.<InterfaceDescription>getContainerOfType(stateUtility, InterfaceDescription.class);
    List<OperatingState> istOfOperatingStates = this.getCandidateOperatingStates(_containerOfType);
    OperatingState _startState = stateUtility.getStartState();
    boolean _notEquals = (!Objects.equal(_startState, null));
    if (_notEquals) {
      OperatingState _startState_1 = stateUtility.getStartState();
      istOfOperatingStates.remove(_startState_1);
    }
    final Consumer<OperatingState> _function = new Consumer<OperatingState>() {
      @Override
      public void accept(final OperatingState it) {
        String _name = it.getName();
        ICompletionProposal _createCompletionProposal = MncProposalProvider.this.createCompletionProposal(_name, context);
        acceptor.accept(_createCompletionProposal);
      }
    };
    istOfOperatingStates.forEach(_function);
  }
  
  @Override
  public void completeResponsibleItemList_ResponsibleAlarms(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    if ((!(model instanceof ResponsibleItemList))) {
      return;
    }
    ResponsibleItemList responsibleItemList = ((ResponsibleItemList) model);
    AlarmBlock _containerOfType = EcoreUtil2.<AlarmBlock>getContainerOfType(model, AlarmBlock.class);
    boolean _notEquals = (!Objects.equal(_containerOfType, null));
    if (_notEquals) {
      ControlNode _containerOfType_1 = EcoreUtil2.<ControlNode>getContainerOfType(responsibleItemList, ControlNode.class);
      InterfaceDescription _interfaceDescription = _containerOfType_1.getInterfaceDescription();
      List<Alarm> listOfAlarms = this.getCandidateAlarms(_interfaceDescription);
      ControlNode _containerOfType_2 = EcoreUtil2.<ControlNode>getContainerOfType(responsibleItemList, ControlNode.class);
      EList<ControlNode> _childNodes = _containerOfType_2.getChildNodes();
      for (final ControlNode childControlNode : _childNodes) {
        InterfaceDescription _interfaceDescription_1 = childControlNode.getInterfaceDescription();
        List<Alarm> _candidateAlarms = this.getCandidateAlarms(_interfaceDescription_1);
        listOfAlarms.addAll(_candidateAlarms);
      }
      AlarmBlock _containerOfType_3 = EcoreUtil2.<AlarmBlock>getContainerOfType(responsibleItemList, AlarmBlock.class);
      Alarm _alarm = _containerOfType_3.getAlarm();
      listOfAlarms.remove(_alarm);
      final Consumer<Alarm> _function = new Consumer<Alarm>() {
        @Override
        public void accept(final Alarm it) {
          QualifiedName _apply = new Function<Alarm, QualifiedName>() {
            @Override
            public QualifiedName apply(final Alarm input) {
              return MncProposalProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
            }
            
            @Override
            public boolean equals(final Object object) {
              throw new UnsupportedOperationException("TODO: auto-generated method stub");
            }
          }.apply(it);
          String proposal = _apply.toString();
          ICompletionProposal _createCompletionProposal = MncProposalProvider.this.createCompletionProposal(proposal, context);
          acceptor.accept(_createCompletionProposal);
        }
      };
      listOfAlarms.forEach(_function);
    }
  }
  
  @Override
  public void completeStateUtility_StartState(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    if ((!(model instanceof StateUtility))) {
      return;
    }
    StateUtility stateUtility = ((StateUtility) model);
    InterfaceDescription _containerOfType = EcoreUtil2.<InterfaceDescription>getContainerOfType(stateUtility, InterfaceDescription.class);
    List<OperatingState> listOfOperatingStates = this.getCandidateOperatingStates(_containerOfType);
    OperatingState _endState = stateUtility.getEndState();
    boolean _notEquals = (!Objects.equal(_endState, null));
    if (_notEquals) {
      OperatingState _endState_1 = stateUtility.getEndState();
      listOfOperatingStates.remove(_endState_1);
    }
    final Consumer<OperatingState> _function = new Consumer<OperatingState>() {
      @Override
      public void accept(final OperatingState it) {
        String _name = it.getName();
        ICompletionProposal _createCompletionProposal = MncProposalProvider.this.createCompletionProposal(_name, context);
        acceptor.accept(_createCompletionProposal);
      }
    };
    listOfOperatingStates.forEach(_function);
  }
  
  @Override
  public void completeOperation_Script(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    if ((!(model instanceof Operation))) {
      return;
    }
    IWorkbench _workbench = PlatformUI.getWorkbench();
    IWorkbenchWindow _activeWorkbenchWindow = _workbench.getActiveWorkbenchWindow();
    Shell _shell = _activeWorkbenchWindow.getShell();
    final ResourceDialog resourceDialog = new ResourceDialog(_shell, "Choose File", 
      SWT.NONE);
    resourceDialog.open();
    final Operation operation = ((Operation) model);
    XtextEditor _activeXtextEditor = EditorUtils.getActiveXtextEditor();
    final IXtextDocument xtextDocument = _activeXtextEditor.getDocument();
    Display currentUiThread = Display.getCurrent();
    currentUiThread.asyncExec(
      new Runnable() {
        @Override
        public void run() {
          xtextDocument.<Object>modify(new IUnitOfWork.Void<XtextResource>() {
            @Override
            public void process(final XtextResource state) throws Exception {
              Operation _createOperation = MncModelFactory.eINSTANCE.createOperation();
              final Procedure1<Operation> _function = new Procedure1<Operation>() {
                @Override
                public void apply(final Operation it) {
                  String _name = operation.getName();
                  it.setName(_name);
                  EList<AbstractInterfaceItems> _inputParameters = it.getInputParameters();
                  EList<AbstractInterfaceItems> _inputParameters_1 = operation.getInputParameters();
                  _inputParameters.addAll(_inputParameters_1);
                  EList<AbstractInterfaceItems> _outputParameters = it.getOutputParameters();
                  EList<AbstractInterfaceItems> _outputParameters_1 = operation.getOutputParameters();
                  _outputParameters.addAll(_outputParameters_1);
                  String _uRIText = resourceDialog.getURIText();
                  it.setScript(_uRIText);
                }
              };
              Operation newOperation = ObjectExtensions.<Operation>operator_doubleArrow(_createOperation, _function);
              EObject eParent = operation.eContainer();
              boolean _matched = false;
              if (!_matched) {
                if (eParent instanceof CommandValidation) {
                  _matched=true;
                  ((CommandValidation) eParent).setOperation(newOperation);
                }
              }
              if (!_matched) {
                if (eParent instanceof CommandTriggerCondition) {
                  _matched=true;
                  ((CommandTriggerCondition) eParent).setOperation(newOperation);
                }
              }
              if (!_matched) {
                if (eParent instanceof EventTriggerCondition) {
                  _matched=true;
                  ((EventTriggerCondition) eParent).setOperation(newOperation);
                }
              }
              if (!_matched) {
                if (eParent instanceof AlarmTriggerCondition) {
                  _matched=true;
                  ((AlarmTriggerCondition) eParent).setOperation(newOperation);
                }
              }
              if (!_matched) {
                if (eParent instanceof DataPointTriggerCondition) {
                  _matched=true;
                  ((DataPointTriggerCondition) eParent).setOperation(newOperation);
                }
              }
              if (!_matched) {
                if (eParent instanceof ResponseTranslationRule) {
                  _matched=true;
                  ((ResponseTranslationRule) eParent).setOperation(newOperation);
                }
              }
              if (!_matched) {
                if (eParent instanceof Action) {
                  _matched=true;
                  ((Action) eParent).setOperation(newOperation);
                }
              }
            }
          });
        }
      });
  }
  
  @Override
  public void completeCheckParameterCondition_Parameter(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    CheckParameterCondition validation = ((CheckParameterCondition) model);
    ResponseBlock responseBlock = EcoreUtil2.<ResponseBlock>getContainerOfType(validation, ResponseBlock.class);
    boolean _and = false;
    boolean _notEquals = (!Objects.equal(responseBlock, null));
    if (!_notEquals) {
      _and = false;
    } else {
      _and = (responseBlock instanceof ResponseBlock);
    }
    if (_and) {
      ControlNode _containerOfType = EcoreUtil2.<ControlNode>getContainerOfType(validation, ControlNode.class);
      InterfaceDescription interfaceDescription = _containerOfType.getInterfaceDescription();
      ResponseValidation _responseValidation = responseBlock.getResponseValidation();
      EList<CheckParameterCondition> responseValidationRules = _responseValidation.getValidationRules();
      List<Parameter> responseValidationParameters = CollectionLiterals.<Parameter>newArrayList();
      for (final CheckParameterCondition respValidRule : responseValidationRules) {
        Parameter _parameter = respValidRule.getParameter();
        responseValidationParameters.add(_parameter);
      }
      List<Parameter> listOfParameters = CollectionLiterals.<Parameter>newArrayList();
      ResponseUtility _responses = interfaceDescription.getResponses();
      EList<Response> _responses_1 = _responses.getResponses();
      for (final Response response : _responses_1) {
        Response _response = responseBlock.getResponse();
        boolean _equals = response.equals(_response);
        if (_equals) {
        }
      }
      listOfParameters.removeAll(responseValidationParameters);
      for (final Parameter parameter : listOfParameters) {
        {
          QualifiedName _apply = new Function<Parameter, QualifiedName>() {
            @Override
            public QualifiedName apply(final Parameter input) {
              return MncProposalProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
            }
            
            @Override
            public boolean equals(final Object object) {
              throw new UnsupportedOperationException("TODO: auto-generated method stub");
            }
          }.apply(parameter);
          String proposal = _apply.toString();
          ICompletionProposal completionProposal = this.createCompletionProposal(proposal, context);
          acceptor.accept(completionProposal);
        }
      }
    } else {
      CommandResponseBlock commandBlock = EcoreUtil2.<CommandResponseBlock>getContainerOfType(model, CommandResponseBlock.class);
      ControlNode _containerOfType_1 = EcoreUtil2.<ControlNode>getContainerOfType(validation, ControlNode.class);
      InterfaceDescription interfaceDescription_1 = _containerOfType_1.getInterfaceDescription();
      CommandValidation _commandValidation = commandBlock.getCommandValidation();
      EList<CheckParameterCondition> commandValidationRules = _commandValidation.getValidationRules();
      List<Parameter> commandValidationParameters = CollectionLiterals.<Parameter>newArrayList();
      for (final CheckParameterCondition commValidRule : commandValidationRules) {
        Parameter _parameter_1 = commValidRule.getParameter();
        commandValidationParameters.add(_parameter_1);
      }
      List<Parameter> listOfParameters_1 = CollectionLiterals.<Parameter>newArrayList();
      CommandUtility _commands = interfaceDescription_1.getCommands();
      EList<Command> _commands_1 = _commands.getCommands();
      for (final Command command : _commands_1) {
        Command _command = commandBlock.getCommand();
        boolean _equals_1 = command.equals(_command);
        if (_equals_1) {
          EList<Parameter> _parameters = command.getParameters();
          listOfParameters_1.addAll(_parameters);
        }
      }
      listOfParameters_1.removeAll(commandValidationParameters);
      for (final Parameter parameter_1 : listOfParameters_1) {
        {
          QualifiedName _apply = new Function<Parameter, QualifiedName>() {
            @Override
            public QualifiedName apply(final Parameter input) {
              return MncProposalProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
            }
            
            @Override
            public boolean equals(final Object object) {
              throw new UnsupportedOperationException("TODO: auto-generated method stub");
            }
          }.apply(parameter_1);
          String proposal = _apply.toString();
          ICompletionProposal completionProposal = this.createCompletionProposal(proposal, context);
          acceptor.accept(completionProposal);
        }
      }
    }
  }
}
