package com.mncml.dsl.ui.quickfix;

import com.mncml.dsl.validation.MncValidator;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.ui.editor.model.edit.IModification;
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext;
import org.eclipse.xtext.ui.editor.quickfix.DefaultQuickfixProvider;
import org.eclipse.xtext.ui.editor.quickfix.Fix;
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor;
import org.eclipse.xtext.validation.Issue;

@SuppressWarnings("all")
public class MncQuickfixProvider extends DefaultQuickfixProvider {
  @Fix(MncValidator.INVALID_NAME)
  public void capitalizeName(final Issue issue, final IssueResolutionAcceptor acceptor) {
    final IModification _function = new IModification() {
      @Override
      public void apply(final IModificationContext context) throws Exception {
        final IXtextDocument xtextDocument = context.getXtextDocument();
        Integer _offset = issue.getOffset();
        final String firstLetter = xtextDocument.get((_offset).intValue(), 1);
        Integer _offset_1 = issue.getOffset();
        String _upperCase = firstLetter.toUpperCase();
        xtextDocument.replace((_offset_1).intValue(), 1, _upperCase);
      }
    };
    acceptor.accept(issue, "Capitalize name", "Capitalize the name.", "upcase.png", _function);
  }
}
