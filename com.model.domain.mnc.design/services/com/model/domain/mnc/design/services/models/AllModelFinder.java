package com.model.domain.mnc.design.services.models;

import java.util.HashMap;

import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManager;
import mncModel.InterfaceDescription;
import mncModel.Model;

public class AllModelFinder {

	HashMap<InterfaceDescription,Model> modelData=null;
	public AllModelFinder() {		
		
	}
	
	//Getting all models from project
	public HashMap<InterfaceDescription,Model> getAllModels()
	{
		modelData=new HashMap<InterfaceDescription, Model>();
		Model tempModel=null;		
		try 
		{
			IWorkspace workspace = ResourcesPlugin.getWorkspace();
			URI sessionResourceURI = URI.createPlatformResourceURI(""+(workspace.getRoot().getProjects())[0].getLocation(), true);
			System.out.println("sessioURI="+sessionResourceURI);
			Session createdSession = SessionManager.INSTANCE.getExistingSession(sessionResourceURI);
			for (Resource element : createdSession.getSemanticResources()) {
				TreeIterator<EObject> eAllContents = element.getAllContents();
				while (eAllContents.hasNext()) {
				      EObject next = eAllContents.next();
				      if(next instanceof Model)
				      {
				    	  tempModel=(Model) next;
				      }
				    	  	
				      if(next instanceof InterfaceDescription)
				      {
				    	 modelData.put((InterfaceDescription)next,tempModel);
				      }				    	  
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
					
		return modelData;
	}
	
	public HashMap<InterfaceDescription,Model> getAllModels(Model model)
	{
		modelData=new HashMap<InterfaceDescription, Model>();
		Model tempModel=null;		
		try 
		{
			Session createdSession = SessionManager.INSTANCE.getSession(model);
			for (Resource element : createdSession.getSemanticResources()) {
				TreeIterator<EObject> eAllContents = element.getAllContents();
				while (eAllContents.hasNext()) {
				      EObject next = eAllContents.next();
				      if(next instanceof Model)
				      {
				    	  tempModel=(Model) next;
				      }
				    	  	
				      if(next instanceof InterfaceDescription)
				      {
				    	 modelData.put((InterfaceDescription)next,tempModel);
				      }				    	  
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
					
		return modelData;
	}

}
