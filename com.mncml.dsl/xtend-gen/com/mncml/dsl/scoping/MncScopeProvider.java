package com.mncml.dsl.scoping;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.List;
import mncModel.Action;
import mncModel.Alarm;
import mncModel.AlarmBlock;
import mncModel.CheckParameterCondition;
import mncModel.Command;
import mncModel.CommandDistribution;
import mncModel.CommandResponseBlock;
import mncModel.CommandTranslation;
import mncModel.CommandValidation;
import mncModel.ControlNode;
import mncModel.DataPoint;
import mncModel.DataPointBlock;
import mncModel.Event;
import mncModel.EventBlock;
import mncModel.InterfaceDescription;
import mncModel.Model;
import mncModel.OperatingState;
import mncModel.Parameter;
import mncModel.ParameterTranslation;
import mncModel.Response;
import mncModel.ResponseBlock;
import mncModel.ResponseTranslationRule;
import mncModel.ResponseValidation;
import mncModel.ResponsibleItemList;
import mncModel.Transition;
import mncModel.utility.AlarmUtility;
import mncModel.utility.CommandUtility;
import mncModel.utility.DataPointUtility;
import mncModel.utility.EventUtility;
import mncModel.utility.ResponseUtility;
import mncModel.utility.StateUtility;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.Scopes;
import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@SuppressWarnings("all")
public class MncScopeProvider extends AbstractDeclarativeScopeProvider {
  @Inject
  private IQualifiedNameProvider qualifiedNameProvider;
  
  public List<OperatingState> getCandidateOperatingStatesForTransitions(final Transition transition) {
    List<OperatingState> _xblockexpression = null;
    {
      List<OperatingState> listOfOperatingStates = CollectionLiterals.<OperatingState>newArrayList();
      ControlNode _containerOfType = EcoreUtil2.<ControlNode>getContainerOfType(transition, ControlNode.class);
      InterfaceDescription interfaceDescription = _containerOfType.getInterfaceDescription();
      StateUtility _operatingStates = interfaceDescription.getOperatingStates();
      boolean _notEquals = (!Objects.equal(_operatingStates, null));
      if (_notEquals) {
        StateUtility _operatingStates_1 = interfaceDescription.getOperatingStates();
        EList<OperatingState> _operatingStates_2 = _operatingStates_1.getOperatingStates();
        listOfOperatingStates.addAll(_operatingStates_2);
      }
      EList<InterfaceDescription> _uses = interfaceDescription.getUses();
      for (final InterfaceDescription interfaceDescriptionInUses : _uses) {
        StateUtility _operatingStates_3 = interfaceDescriptionInUses.getOperatingStates();
        boolean _notEquals_1 = (!Objects.equal(_operatingStates_3, null));
        if (_notEquals_1) {
          StateUtility _operatingStates_4 = interfaceDescriptionInUses.getOperatingStates();
          EList<OperatingState> _operatingStates_5 = _operatingStates_4.getOperatingStates();
          listOfOperatingStates.addAll(_operatingStates_5);
        }
      }
      _xblockexpression = listOfOperatingStates;
    }
    return _xblockexpression;
  }
  
  public List<Command> getCandidateCommands(final ControlNode controlNode) {
    List<Command> _xblockexpression = null;
    {
      InterfaceDescription interfaceDescription = controlNode.getInterfaceDescription();
      List<Command> listOfCommands = CollectionLiterals.<Command>newArrayList();
      CommandUtility _commands = interfaceDescription.getCommands();
      boolean _notEquals = (!Objects.equal(_commands, null));
      if (_notEquals) {
        CommandUtility _commands_1 = interfaceDescription.getCommands();
        EList<Command> _commands_2 = _commands_1.getCommands();
        listOfCommands.addAll(_commands_2);
      }
      EList<InterfaceDescription> _uses = interfaceDescription.getUses();
      for (final InterfaceDescription interfaceDescriptionInUses : _uses) {
        CommandUtility _commands_3 = interfaceDescriptionInUses.getCommands();
        boolean _notEquals_1 = (!Objects.equal(_commands_3, null));
        if (_notEquals_1) {
          CommandUtility _commands_4 = interfaceDescriptionInUses.getCommands();
          EList<Command> _commands_5 = _commands_4.getCommands();
          listOfCommands.addAll(_commands_5);
        }
      }
      _xblockexpression = listOfCommands;
    }
    return _xblockexpression;
  }
  
  public List<Alarm> getCandidateAlarms(final InterfaceDescription interfaceDescription) {
    List<Alarm> _xblockexpression = null;
    {
      List<Alarm> listOfAlarms = CollectionLiterals.<Alarm>newArrayList();
      AlarmUtility _alarms = interfaceDescription.getAlarms();
      boolean _notEquals = (!Objects.equal(_alarms, null));
      if (_notEquals) {
        AlarmUtility _alarms_1 = interfaceDescription.getAlarms();
        EList<Alarm> _alarms_2 = _alarms_1.getAlarms();
        listOfAlarms.addAll(_alarms_2);
      }
      EList<InterfaceDescription> _uses = interfaceDescription.getUses();
      for (final InterfaceDescription interfaceDescriptionInUses : _uses) {
        AlarmUtility _alarms_3 = interfaceDescriptionInUses.getAlarms();
        boolean _notEquals_1 = (!Objects.equal(_alarms_3, null));
        if (_notEquals_1) {
          AlarmUtility _alarms_4 = interfaceDescriptionInUses.getAlarms();
          EList<Alarm> _alarms_5 = _alarms_4.getAlarms();
          listOfAlarms.addAll(_alarms_5);
        }
      }
      _xblockexpression = listOfAlarms;
    }
    return _xblockexpression;
  }
  
  public List<Event> getCandidateEvents(final InterfaceDescription interfaceDescription) {
    List<Event> _xblockexpression = null;
    {
      List<Event> listOfEvents = CollectionLiterals.<Event>newArrayList();
      EventUtility _events = interfaceDescription.getEvents();
      boolean _notEquals = (!Objects.equal(_events, null));
      if (_notEquals) {
        EventUtility _events_1 = interfaceDescription.getEvents();
        EList<Event> _events_2 = _events_1.getEvents();
        listOfEvents.addAll(_events_2);
      }
      EList<InterfaceDescription> _uses = interfaceDescription.getUses();
      for (final InterfaceDescription interfaceDescriptionInUses : _uses) {
        EventUtility _events_3 = interfaceDescriptionInUses.getEvents();
        boolean _notEquals_1 = (!Objects.equal(_events_3, null));
        if (_notEquals_1) {
          EventUtility _events_4 = interfaceDescriptionInUses.getEvents();
          EList<Event> _events_5 = _events_4.getEvents();
          listOfEvents.addAll(_events_5);
        }
      }
      _xblockexpression = listOfEvents;
    }
    return _xblockexpression;
  }
  
  public List<Response> getCandidateResponses(final InterfaceDescription interfaceDescription) {
    List<Response> _xblockexpression = null;
    {
      List<Response> listOfResponses = CollectionLiterals.<Response>newArrayList();
      ResponseUtility _responses = interfaceDescription.getResponses();
      boolean _notEquals = (!Objects.equal(_responses, null));
      if (_notEquals) {
        ResponseUtility _responses_1 = interfaceDescription.getResponses();
        EList<Response> _responses_2 = _responses_1.getResponses();
        listOfResponses.addAll(_responses_2);
      }
      EList<InterfaceDescription> _uses = interfaceDescription.getUses();
      for (final InterfaceDescription interfaceDescriptionInUses : _uses) {
        ResponseUtility _responses_3 = interfaceDescriptionInUses.getResponses();
        boolean _notEquals_1 = (!Objects.equal(_responses_3, null));
        if (_notEquals_1) {
          ResponseUtility _responses_4 = interfaceDescriptionInUses.getResponses();
          EList<Response> _responses_5 = _responses_4.getResponses();
          listOfResponses.addAll(_responses_5);
        }
      }
      _xblockexpression = listOfResponses;
    }
    return _xblockexpression;
  }
  
  public List<DataPoint> getCandidateDataPoints(final InterfaceDescription interfaceDescription) {
    List<DataPoint> _xblockexpression = null;
    {
      List<DataPoint> listOfDataPoints = CollectionLiterals.<DataPoint>newArrayList();
      DataPointUtility _dataPoints = interfaceDescription.getDataPoints();
      boolean _notEquals = (!Objects.equal(_dataPoints, null));
      if (_notEquals) {
        DataPointUtility _dataPoints_1 = interfaceDescription.getDataPoints();
        EList<DataPoint> _dataPoints_2 = _dataPoints_1.getDataPoints();
        listOfDataPoints.addAll(_dataPoints_2);
      }
      EList<InterfaceDescription> _uses = interfaceDescription.getUses();
      for (final InterfaceDescription interfaceDescriptionInUses : _uses) {
        DataPointUtility _dataPoints_3 = interfaceDescriptionInUses.getDataPoints();
        boolean _notEquals_1 = (!Objects.equal(_dataPoints_3, null));
        if (_notEquals_1) {
          DataPointUtility _dataPoints_4 = interfaceDescriptionInUses.getDataPoints();
          EList<DataPoint> _dataPoints_5 = _dataPoints_4.getDataPoints();
          listOfDataPoints.addAll(_dataPoints_5);
        }
      }
      _xblockexpression = listOfDataPoints;
    }
    return _xblockexpression;
  }
  
  public List<ControlNode> getCandidateChildNodes(final ControlNode controlNode) {
    List<ControlNode> _xblockexpression = null;
    {
      List<ControlNode> listOfControlNodes = CollectionLiterals.<ControlNode>newArrayList();
      Model model = EcoreUtil2.<Model>getContainerOfType(controlNode, Model.class);
      EList<mncModel.System> _systems = model.getSystems();
      for (final mncModel.System system : _systems) {
        boolean _and = false;
        if (!(system instanceof ControlNode)) {
          _and = false;
        } else {
          boolean _notEquals = (!Objects.equal(system, controlNode));
          _and = _notEquals;
        }
        if (_and) {
          listOfControlNodes.add(((ControlNode) system));
        }
      }
      _xblockexpression = listOfControlNodes;
    }
    return _xblockexpression;
  }
  
  public List<Response> getCandidateResponsesFromChildNode(final ControlNode controlNode) {
    List<Response> _xblockexpression = null;
    {
      List<Response> listOfResponses = CollectionLiterals.<Response>newArrayList();
      EList<ControlNode> _childNodes = controlNode.getChildNodes();
      for (final ControlNode ccn : _childNodes) {
        {
          InterfaceDescription _interfaceDescription = ccn.getInterfaceDescription();
          ResponseUtility _responses = _interfaceDescription.getResponses();
          boolean _notEquals = (!Objects.equal(_responses, null));
          if (_notEquals) {
            InterfaceDescription _interfaceDescription_1 = ccn.getInterfaceDescription();
            ResponseUtility _responses_1 = _interfaceDescription_1.getResponses();
            EList<Response> _responses_2 = _responses_1.getResponses();
            listOfResponses.addAll(_responses_2);
          }
          InterfaceDescription _interfaceDescription_2 = ccn.getInterfaceDescription();
          EList<InterfaceDescription> _uses = _interfaceDescription_2.getUses();
          for (final InterfaceDescription interfaceDescriptionInUses : _uses) {
            ResponseUtility _responses_3 = interfaceDescriptionInUses.getResponses();
            EList<Response> _responses_4 = _responses_3.getResponses();
            listOfResponses.addAll(_responses_4);
          }
        }
      }
      _xblockexpression = listOfResponses;
    }
    return _xblockexpression;
  }
  
  public List<Command> getCandidateCommandsFromChildNode(final ControlNode controlNode) {
    List<Command> _xblockexpression = null;
    {
      List<Command> listOfCommands = CollectionLiterals.<Command>newArrayList();
      EList<ControlNode> _childNodes = controlNode.getChildNodes();
      for (final ControlNode childControlNode : _childNodes) {
        {
          InterfaceDescription _interfaceDescription = childControlNode.getInterfaceDescription();
          CommandUtility _commands = _interfaceDescription.getCommands();
          boolean _notEquals = (!Objects.equal(_commands, null));
          if (_notEquals) {
            InterfaceDescription _interfaceDescription_1 = childControlNode.getInterfaceDescription();
            CommandUtility _commands_1 = _interfaceDescription_1.getCommands();
            EList<Command> _commands_2 = _commands_1.getCommands();
            listOfCommands.addAll(_commands_2);
          }
          InterfaceDescription _interfaceDescription_2 = childControlNode.getInterfaceDescription();
          EList<InterfaceDescription> _uses = _interfaceDescription_2.getUses();
          for (final InterfaceDescription interfaceDescriptionInUses : _uses) {
            CommandUtility _commands_3 = interfaceDescriptionInUses.getCommands();
            EList<Command> _commands_4 = _commands_3.getCommands();
            listOfCommands.addAll(_commands_4);
          }
        }
      }
      _xblockexpression = listOfCommands;
    }
    return _xblockexpression;
  }
  
  public IScope scope_Transition_currentState(final Transition transition, final EReference ref) {
    List<OperatingState> _candidateOperatingStatesForTransitions = this.getCandidateOperatingStatesForTransitions(transition);
    return Scopes.<OperatingState>scopeFor(_candidateOperatingStatesForTransitions, 
      new Function<OperatingState, QualifiedName>() {
        @Override
        public QualifiedName apply(final OperatingState input) {
          return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
        }
        
        @Override
        public boolean equals(final Object object) {
          throw new UnsupportedOperationException("TODO: auto-generated method stub");
        }
      }, 
      IScope.NULLSCOPE);
  }
  
  public IScope scope_Transition_nextState(final Transition transition, final EReference ref) {
    List<OperatingState> _candidateOperatingStatesForTransitions = this.getCandidateOperatingStatesForTransitions(transition);
    return Scopes.<OperatingState>scopeFor(_candidateOperatingStatesForTransitions, 
      new Function<OperatingState, QualifiedName>() {
        @Override
        public QualifiedName apply(final OperatingState input) {
          return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
        }
        
        @Override
        public boolean equals(final Object object) {
          throw new UnsupportedOperationException("TODO: auto-generated method stub");
        }
      }, 
      IScope.NULLSCOPE);
  }
  
  public IScope scope_Action_command(final Action action, final EReference ref) {
    ControlNode _containerOfType = EcoreUtil2.<ControlNode>getContainerOfType(action, ControlNode.class);
    List<Command> _candidateCommands = this.getCandidateCommands(_containerOfType);
    return Scopes.<Command>scopeFor(_candidateCommands, 
      new Function<Command, QualifiedName>() {
        @Override
        public QualifiedName apply(final Command input) {
          return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
        }
        
        @Override
        public boolean equals(final Object object) {
          throw new UnsupportedOperationException("TODO: auto-generated method stub");
        }
      }, 
      IScope.NULLSCOPE);
  }
  
  public IScope scope_ResponseBlock_response(final ResponseBlock responseBlock, final EReference ref) {
    IScope _xblockexpression = null;
    {
      ControlNode controlNode = EcoreUtil2.<ControlNode>getContainerOfType(responseBlock, ControlNode.class);
      InterfaceDescription _interfaceDescription = controlNode.getInterfaceDescription();
      List<Response> _candidateResponses = this.getCandidateResponses(_interfaceDescription);
      _xblockexpression = Scopes.<Response>scopeFor(_candidateResponses, 
        new Function<Response, QualifiedName>() {
          @Override
          public QualifiedName apply(final Response input) {
            return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
          }
          
          @Override
          public boolean equals(final Object object) {
            throw new UnsupportedOperationException("TODO: auto-generated method stub");
          }
        }, 
        IScope.NULLSCOPE);
    }
    return _xblockexpression;
  }
  
  public IScope scope_CheckParameterCondition_parameter(final CheckParameterCondition checkParameterCondition, final EReference ref) {
    IScope _xifexpression = null;
    CommandValidation _containerOfType = EcoreUtil2.<CommandValidation>getContainerOfType(checkParameterCondition, CommandValidation.class);
    boolean _notEquals = (!Objects.equal(_containerOfType, null));
    if (_notEquals) {
      CommandResponseBlock _containerOfType_1 = EcoreUtil2.<CommandResponseBlock>getContainerOfType(checkParameterCondition, CommandResponseBlock.class);
      Command _command = _containerOfType_1.getCommand();
      EList<Parameter> _parameters = _command.getParameters();
      _xifexpression = Scopes.<Parameter>scopeFor(_parameters, 
        new Function<Parameter, QualifiedName>() {
          @Override
          public QualifiedName apply(final Parameter input) {
            return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
          }
          
          @Override
          public boolean equals(final Object object) {
            throw new UnsupportedOperationException("TODO: auto-generated method stub");
          }
        }, 
        IScope.NULLSCOPE);
    } else {
      ResponseValidation _containerOfType_2 = EcoreUtil2.<ResponseValidation>getContainerOfType(checkParameterCondition, ResponseValidation.class);
      Response _response = _containerOfType_2.getResponse();
      EList<Parameter> _parameters_1 = _response.getParameters();
      _xifexpression = Scopes.<Parameter>scopeFor(_parameters_1, 
        new Function<Parameter, QualifiedName>() {
          @Override
          public QualifiedName apply(final Parameter input) {
            return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
          }
          
          @Override
          public boolean equals(final Object object) {
            throw new UnsupportedOperationException("TODO: auto-generated method stub");
          }
        }, 
        IScope.NULLSCOPE);
    }
    return _xifexpression;
  }
  
  public IScope scope_CommandTranslation_translatedCommands(final CommandTranslation commandTranslation, final EReference ref) {
    ControlNode _containerOfType = EcoreUtil2.<ControlNode>getContainerOfType(commandTranslation, ControlNode.class);
    List<Command> _candidateCommandsFromChildNode = this.getCandidateCommandsFromChildNode(_containerOfType);
    return Scopes.<Command>scopeFor(_candidateCommandsFromChildNode, 
      new Function<Command, QualifiedName>() {
        @Override
        public QualifiedName apply(final Command input) {
          return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
        }
        
        @Override
        public boolean equals(final Object object) {
          throw new UnsupportedOperationException("TODO: auto-generated method stub");
        }
      }, 
      IScope.NULLSCOPE);
  }
  
  public IScope scope_CommandResponseBlock_command(final CommandResponseBlock commandResponseBlock, final EReference ref) {
    ControlNode _containerOfType = EcoreUtil2.<ControlNode>getContainerOfType(commandResponseBlock, ControlNode.class);
    List<Command> _candidateCommands = this.getCandidateCommands(_containerOfType);
    return Scopes.<Command>scopeFor(_candidateCommands, 
      new Function<Command, QualifiedName>() {
        @Override
        public QualifiedName apply(final Command input) {
          return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
        }
        
        @Override
        public boolean equals(final Object object) {
          throw new UnsupportedOperationException("TODO: auto-generated method stub");
        }
      }, 
      IScope.NULLSCOPE);
  }
  
  public IScope scope_CommandResponseBlock_triggeredEvents(final CommandResponseBlock commandResponseBlock, final EReference ref) {
    ControlNode _containerOfType = EcoreUtil2.<ControlNode>getContainerOfType(commandResponseBlock, ControlNode.class);
    InterfaceDescription _interfaceDescription = _containerOfType.getInterfaceDescription();
    List<Event> _candidateEvents = this.getCandidateEvents(_interfaceDescription);
    return Scopes.<Event>scopeFor(_candidateEvents, 
      new Function<Event, QualifiedName>() {
        @Override
        public QualifiedName apply(final Event input) {
          return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
        }
        
        @Override
        public boolean equals(final Object object) {
          throw new UnsupportedOperationException("TODO: auto-generated method stub");
        }
      }, 
      IScope.NULLSCOPE);
  }
  
  public IScope scope_ParameterTranslation_inputParameters(final ParameterTranslation parameterTranslation, final EReference ref) {
    IScope _xifexpression = null;
    CommandTranslation _containerOfType = EcoreUtil2.<CommandTranslation>getContainerOfType(parameterTranslation, CommandTranslation.class);
    boolean _notEquals = (!Objects.equal(_containerOfType, null));
    if (_notEquals) {
      CommandResponseBlock _containerOfType_1 = EcoreUtil2.<CommandResponseBlock>getContainerOfType(parameterTranslation, CommandResponseBlock.class);
      Command _command = _containerOfType_1.getCommand();
      EList<Parameter> _parameters = _command.getParameters();
      _xifexpression = Scopes.scopeFor(_parameters);
    } else {
      IScope _xblockexpression = null;
      {
        List<Parameter> listOfParameters = CollectionLiterals.<Parameter>newArrayList();
        ResponseTranslationRule _containerOfType_2 = EcoreUtil2.<ResponseTranslationRule>getContainerOfType(parameterTranslation, ResponseTranslationRule.class);
        EList<Response> _inputResponses = _containerOfType_2.getInputResponses();
        for (final Response response : _inputResponses) {
          EList<Parameter> _parameters_1 = response.getParameters();
          listOfParameters.addAll(_parameters_1);
        }
        _xblockexpression = Scopes.<Parameter>scopeFor(listOfParameters, 
          new Function<Parameter, QualifiedName>() {
            @Override
            public QualifiedName apply(final Parameter input) {
              return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
            }
            
            @Override
            public boolean equals(final Object object) {
              throw new UnsupportedOperationException("TODO: auto-generated method stub");
            }
          }, 
          IScope.NULLSCOPE);
      }
      _xifexpression = _xblockexpression;
    }
    return _xifexpression;
  }
  
  public IScope scope_ParameterTranslation_translatedParameters(final ParameterTranslation parameterTranslation, final EReference ref) {
    IScope _xblockexpression = null;
    {
      List<Parameter> listOfParameters = CollectionLiterals.<Parameter>newArrayList();
      IScope _xifexpression = null;
      CommandTranslation _containerOfType = EcoreUtil2.<CommandTranslation>getContainerOfType(parameterTranslation, CommandTranslation.class);
      boolean _notEquals = (!Objects.equal(_containerOfType, null));
      if (_notEquals) {
        IScope _xblockexpression_1 = null;
        {
          CommandTranslation _containerOfType_1 = EcoreUtil2.<CommandTranslation>getContainerOfType(parameterTranslation, CommandTranslation.class);
          EList<Command> _translatedCommands = _containerOfType_1.getTranslatedCommands();
          for (final Command command : _translatedCommands) {
            EList<Parameter> _parameters = command.getParameters();
            listOfParameters.addAll(_parameters);
          }
          _xblockexpression_1 = Scopes.<Parameter>scopeFor(listOfParameters, 
            new Function<Parameter, QualifiedName>() {
              @Override
              public QualifiedName apply(final Parameter input) {
                return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
              }
              
              @Override
              public boolean equals(final Object object) {
                throw new UnsupportedOperationException("TODO: auto-generated method stub");
              }
            }, 
            IScope.NULLSCOPE);
        }
        _xifexpression = _xblockexpression_1;
      } else {
        _xifexpression = Scopes.<Parameter>scopeFor(listOfParameters, 
          new Function<Parameter, QualifiedName>() {
            @Override
            public QualifiedName apply(final Parameter input) {
              return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
            }
            
            @Override
            public boolean equals(final Object object) {
              throw new UnsupportedOperationException("TODO: auto-generated method stub");
            }
          }, 
          IScope.NULLSCOPE);
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public IScope scope_CommandDistribution_command(final CommandDistribution oommandDistribution, final EReference ref) {
    CommandResponseBlock _containerOfType = EcoreUtil2.<CommandResponseBlock>getContainerOfType(oommandDistribution, CommandResponseBlock.class);
    CommandTranslation _commandTranslation = _containerOfType.getCommandTranslation();
    EList<Command> _translatedCommands = _commandTranslation.getTranslatedCommands();
    return Scopes.<Command>scopeFor(_translatedCommands, 
      new Function<Command, QualifiedName>() {
        @Override
        public QualifiedName apply(final Command input) {
          return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
        }
        
        @Override
        public boolean equals(final Object object) {
          throw new UnsupportedOperationException("TODO: auto-generated method stub");
        }
      }, 
      IScope.NULLSCOPE);
  }
  
  public IScope scope_CommandDistribution_destinationNodes(final CommandDistribution commandDistribution, final EReference ref) {
    ControlNode _containerOfType = EcoreUtil2.<ControlNode>getContainerOfType(commandDistribution, ControlNode.class);
    EList<ControlNode> _childNodes = _containerOfType.getChildNodes();
    return Scopes.scopeFor(_childNodes);
  }
  
  public IScope scope_ResponseTranslationRule_inputResponses(final ResponseTranslationRule responseTranslationRule, final EReference ref) {
    ControlNode _containerOfType = EcoreUtil2.<ControlNode>getContainerOfType(responseTranslationRule, ControlNode.class);
    List<Response> _candidateResponsesFromChildNode = this.getCandidateResponsesFromChildNode(_containerOfType);
    return Scopes.<Response>scopeFor(_candidateResponsesFromChildNode, 
      new Function<Response, QualifiedName>() {
        @Override
        public QualifiedName apply(final Response input) {
          return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
        }
        
        @Override
        public boolean equals(final Object object) {
          throw new UnsupportedOperationException("TODO: auto-generated method stub");
        }
      }, 
      IScope.NULLSCOPE);
  }
  
  public IScope scope_ResponsibleItemList_responsibleCommands(final ResponsibleItemList responsibleItemList, final EReference ref) {
    ControlNode _containerOfType = EcoreUtil2.<ControlNode>getContainerOfType(responsibleItemList, ControlNode.class);
    List<Command> _candidateCommands = this.getCandidateCommands(_containerOfType);
    return Scopes.<Command>scopeFor(_candidateCommands, 
      new Function<Command, QualifiedName>() {
        @Override
        public QualifiedName apply(final Command input) {
          return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
        }
        
        @Override
        public boolean equals(final Object object) {
          throw new UnsupportedOperationException("TODO: auto-generated method stub");
        }
      }, 
      IScope.NULLSCOPE);
  }
  
  public IScope scope_ResponsibleItemList_responsibleAlarms(final ResponsibleItemList responsibleItemList, final EReference ref) {
    IScope _xblockexpression = null;
    {
      ControlNode controlNode = EcoreUtil2.<ControlNode>getContainerOfType(responsibleItemList, ControlNode.class);
      InterfaceDescription _interfaceDescription = controlNode.getInterfaceDescription();
      List<Alarm> listOfAlarms = this.getCandidateAlarms(_interfaceDescription);
      AlarmBlock _containerOfType = EcoreUtil2.<AlarmBlock>getContainerOfType(responsibleItemList, AlarmBlock.class);
      boolean _notEquals = (!Objects.equal(_containerOfType, null));
      if (_notEquals) {
        ControlNode _containerOfType_1 = EcoreUtil2.<ControlNode>getContainerOfType(responsibleItemList, ControlNode.class);
        EList<ControlNode> _childNodes = _containerOfType_1.getChildNodes();
        for (final ControlNode childControlNode : _childNodes) {
          InterfaceDescription _interfaceDescription_1 = childControlNode.getInterfaceDescription();
          List<Alarm> _candidateAlarms = this.getCandidateAlarms(_interfaceDescription_1);
          listOfAlarms.addAll(_candidateAlarms);
        }
      }
      _xblockexpression = Scopes.<Alarm>scopeFor(listOfAlarms, 
        new Function<Alarm, QualifiedName>() {
          @Override
          public QualifiedName apply(final Alarm input) {
            return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
          }
          
          @Override
          public boolean equals(final Object object) {
            throw new UnsupportedOperationException("TODO: auto-generated method stub");
          }
        }, 
        IScope.NULLSCOPE);
    }
    return _xblockexpression;
  }
  
  public IScope scope_ResponsibleItemList_responsibleEvents(final ResponsibleItemList responsibleItemList, final EReference ref) {
    IScope _xblockexpression = null;
    {
      ControlNode _containerOfType = EcoreUtil2.<ControlNode>getContainerOfType(responsibleItemList, ControlNode.class);
      InterfaceDescription _interfaceDescription = _containerOfType.getInterfaceDescription();
      List<Event> listOfEvents = this.getCandidateEvents(_interfaceDescription);
      EventBlock _containerOfType_1 = EcoreUtil2.<EventBlock>getContainerOfType(responsibleItemList, EventBlock.class);
      boolean _notEquals = (!Objects.equal(_containerOfType_1, null));
      if (_notEquals) {
        ControlNode _containerOfType_2 = EcoreUtil2.<ControlNode>getContainerOfType(responsibleItemList, ControlNode.class);
        EList<ControlNode> _childNodes = _containerOfType_2.getChildNodes();
        for (final ControlNode childControlNode : _childNodes) {
          InterfaceDescription _interfaceDescription_1 = childControlNode.getInterfaceDescription();
          List<Event> _candidateEvents = this.getCandidateEvents(_interfaceDescription_1);
          listOfEvents.addAll(_candidateEvents);
        }
      }
      _xblockexpression = Scopes.<Event>scopeFor(listOfEvents, 
        new Function<Event, QualifiedName>() {
          @Override
          public QualifiedName apply(final Event input) {
            return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
          }
          
          @Override
          public boolean equals(final Object object) {
            throw new UnsupportedOperationException("TODO: auto-generated method stub");
          }
        }, 
        IScope.NULLSCOPE);
    }
    return _xblockexpression;
  }
  
  public IScope scope_ResponsibleItemList_responsibleDataPoints(final ResponsibleItemList responsibleItemList, final EReference ref) {
    IScope _xblockexpression = null;
    {
      ControlNode _containerOfType = EcoreUtil2.<ControlNode>getContainerOfType(responsibleItemList, ControlNode.class);
      InterfaceDescription _interfaceDescription = _containerOfType.getInterfaceDescription();
      List<DataPoint> listOfDatapoints = this.getCandidateDataPoints(_interfaceDescription);
      DataPointBlock _containerOfType_1 = EcoreUtil2.<DataPointBlock>getContainerOfType(responsibleItemList, DataPointBlock.class);
      boolean _notEquals = (!Objects.equal(_containerOfType_1, null));
      if (_notEquals) {
        ControlNode _containerOfType_2 = EcoreUtil2.<ControlNode>getContainerOfType(responsibleItemList, ControlNode.class);
        EList<ControlNode> _childNodes = _containerOfType_2.getChildNodes();
        for (final ControlNode ccn : _childNodes) {
          InterfaceDescription _interfaceDescription_1 = ccn.getInterfaceDescription();
          List<DataPoint> _candidateDataPoints = this.getCandidateDataPoints(_interfaceDescription_1);
          listOfDatapoints.addAll(_candidateDataPoints);
        }
      }
      _xblockexpression = Scopes.<DataPoint>scopeFor(listOfDatapoints, 
        new Function<DataPoint, QualifiedName>() {
          @Override
          public QualifiedName apply(final DataPoint input) {
            return MncScopeProvider.this.qualifiedNameProvider.getFullyQualifiedName(input);
          }
          
          @Override
          public boolean equals(final Object object) {
            throw new UnsupportedOperationException("TODO: auto-generated method stub");
          }
        }, 
        IScope.NULLSCOPE);
    }
    return _xblockexpression;
  }
}
