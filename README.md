# MAC-SEEN

This repository presents an environment to generate design for Monitoring and Control (M&C) solutions.

## Requirements
* Java8 - http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
* Eclipse DSL Package (Mars) - http://www.eclipse.org/downloads/packages/eclipse-ide-java-and-dsl-developers/mars2 
* JTango-9.0.5.jar - https://sourceforge.net/projects/tango-cs/files/JTango/JTango-9.0.5/
* json-simple-1.1.1.jar - https://mvnrepository.com/artifact/com.googlecode.json-simple/json-simple/1.1.1
* Eclipse Sirius Plugins - Install Eclipse sirius plugins from **Eclipse Marketplace**. (**OPTIONAL :** Only while using `com.model.domain.mnc.design` project.)

## Guide
* Clone the Git repository on your eclipse application.  A thorough guide to use Git on Eclipse (EGit) can be found at https://wiki.eclipse.org/EGit/User_Guide
* After downloading the JTango-9.0.5.jar and json-simple-1.1.1.jar place them into the 'libs' folder of the project 'com.mncml.dsl.libs'.
* Use the 'mncml.product' file in 'com.mncml.product' project to launch an eclipse instance.
* Use `Run as Eclipse Application` for launching an Eclipse instance while using the Sirius Design project.
 
## Walkthrough
After Launching an eclipse application the user is recommended to follow these steps
* Create a MNC project in the workbench by clicking `File → New → Project`
* Enter the name of the project in the text box.
* Create a file by clicking on `src → New → File` 
* Enter the name of the file and very importantly give the extension of the file as *“.mncspec”*.
* Start creating the **Interface Description** for a control node.
* Specify the control functionalities and behaviour of the controller in the **Control Node** block. 
* The code generation to TANGO Java can be performed by `Right Click(on editor) → Generate Java Code`

## Note
* ‘JVM cannot be created. Fatal error’ is caused by less available memory on RAM.
* `com.model.domain.mnc.design` is an experimental project for showing visual elements for the DSL.

